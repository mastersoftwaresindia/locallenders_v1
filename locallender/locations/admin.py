from django.contrib import admin

# Register your models here.
from .models import Location, LenderAccountTransaction

#admin.site.register(Location)
admin.site.register(Location)
admin.site.register(LenderAccountTransaction)
