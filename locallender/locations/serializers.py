from rest_framework import serializers

from .models import Location


class LocationSerializer(serializers.ModelSerializer):
    
    class Meta:
        model   = Location
        fields  = ('id', 'lender_id', 'location_name', 'added_on', 'landmark',
                  'products_offered','location_tagline','lender_store_id','name',
                  'email','address','city','state','zipcode','phone','logo',
                  'longitude','latitude','max_hourly','max_daily','max_weekly','max_monthly',
                  'count_hourly','count_daily','count_weekly','count_monthly','is_active',
                  'max_distance_allowed_in_miles','time_before_willing_to_accept_same_lead_in_days',
                  'last_reset_on','lead_price'
                  )