import json
from .models import *
from helpdesk.models import *
from rest_framework import generics, permissions
from django.http import HttpResponse, StreamingHttpResponse
from django.views.generic.base import View
from django.core.validators import validate_email
#from .models import Location
from .serializers import *
from django.views.generic.base import TemplateView
from django.conf import settings
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, Http404
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache
from django.conf import settings
from django.utils.encoding import iri_to_uri
from django.utils.http import urlquote
from django.core.exceptions import ImproperlyConfigured
from django.contrib.auth import authenticate
import hashlib
import hmac
import time
from django.core.mail import send_mail
from django.contrib.auth import login, logout
from django.views.generic.base import RedirectView

from django.core.urlresolvers import reverse

import sys
from geopy.geocoders import Nominatim
from geopy.geocoders import GoogleV3
from geopy.distance import great_circle

from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from django.utils import timezone
import datetime
import stripe
from freshdesk_api.tickets import *
from itertools import chain
import monitio
from django.contrib import messages
from django.contrib.auth.models import User
from itertools import chain
from StringIO import StringIO
import pycurl
from django.core.mail import send_mail
import ast
from collections import Counter
import datetime
import decimal
from django.db.models.base import ModelState
from hellosign_sdk import HSClient
from datetime import  timedelta
from django.utils import timezone

from django.core.mail import send_mail
from django.shortcuts import render

from geopy.geocoders import Nominatim
from geopy.geocoders import GoogleV3
from geopy.distance import great_circle
from django.views.decorators.csrf import csrf_exempt

try:
    from django.contrib.auth import get_user_model
    User = get_user_model()
except ImportError:
    from django.contrib.auth.models import User

# Create your views here.
@login_required
@csrf_exempt
def process_request(request, *args, **kwargs):
    ''' Exception handler sterted '''
    try:
        myDict = dict(request.POST.iterlists())
        content =ast.literal_eval(json.dumps(myDict))
        print content
        ''' Take operation type in operation variable and call the function '''
        operation = content['oper'][0]
        print operation
        ''' add new location '''
        if operation=='add':
            #if check_location(content['address'][0]):
                if create_location(request, content):
                    return HttpResponse("New Location added successfully !")

        ''' update location '''
        if operation=='edit':
            #if check_location(content['address'][0]):
                update_location(request,content)
                return HttpResponse("Working here for location operations update")

        if operation=='del':
            #if check_location(content['address'][0]):
                ''' add new location '''
                if delete_location(request, content):
                    return HttpResponse("New Location added successfully !")
    except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
     
''' Calculate longitude and lattitude '''
def calculate_longi_latti_by_zipcode(zipcode):
    geolocator = GoogleV3()
    cordinates = geolocator.geocode(zipcode)
    
    ''' collect longitude and lattitude of current lead data '''
    if cordinates:
        loc =(cordinates.longitude,cordinates.latitude)
        return loc
    else:
        return (None,None)
    
''' Calculate longitude and lattitude '''
def calculate_longi_latti_by_address(address):
    geolocator = GoogleV3()
    cordinates = geolocator.geocode(str(address))
    
    ''' collect longitude and lattitude of current lead data '''
    if cordinates:
        loc =(cordinates.longitude,cordinates.latitude)
        return loc
    else:
        return (None,None)

''' check if location exists '''
def check_location(id):
    ''' Check if location already exists '''
    location = Location.objects.filter(id=id)    
    ''' If location already exists update info '''
    return location

''' function call to create lender location '''
def create_location(request, content):
    print content['zipcode'][0]
    ''' Cretae model object and save data in location entity '''
    loc = calculate_longi_latti_by_zipcode(content['zipcode'][0]) # weak
    #loc = calculate_longi_latti_by_zipcode(content['address'][0])
    print "create", loc
    
    ''' Create store manager account '''
    manager    = create_location_manager(content)
    location   = Location.objects.create(
                                        lender_id       =request.user,
                                        location_name   =content['location_name'][0],
                                        address         =content['address'][0],
                                        longitude       =loc[0],
                                        latitude        =loc[1],
                                        zipcode         =content['zipcode'][0],
                                        name            =manager,
                                        email           =content['email'][0],
                                        phone           =content['phone'][0],
                                        max_hourly      =content['max_hourly'][0],
                                        max_daily       =content['max_daily'][0],
                                        max_weekly      =content['max_weekly'][0],
                                        max_monthly     =content['max_monthly'][0],
                                        added_on        =datetime.datetime.now(),
                                        last_reset_on   =datetime.datetime.now()
                                    ).save()
    return True

''' function call to update lender location '''
def update_location(request,content):
    ''' Create model object and update data in location entity '''
    loc = calculate_longi_latti_by_zipcode(content['zipcode'][0]) # weak
    #loc = calculate_longi_latti_by_zipcode(content['address'][0])
    location = check_location(content['id'][0])
    for loca in location:
        location_id = loca.id
        
    manager    = create_location_manager(content)
    Location.objects.filter(id=location_id).update(
                                        lender_id       =request.user,
                                        location_name   =content['location_name'][0],
                                        address         =content['address'][0],
                                        longitude       =loc[0],
                                        latitude        =loc[1],
                                        zipcode         =content['zipcode'][0],
                                        name            =manager,
                                        email           =content['email'][0],
                                        phone           =content['phone'][0],
                                        max_hourly      =content['max_hourly'][0],
                                        max_daily       =content['max_daily'][0],
                                        max_weekly      =content['max_weekly'][0],
                                        max_monthly     =content['max_monthly'][0],
                                        last_reset_on   =datetime.datetime.now()
                                    )
    return True

''' Create location manager '''
def create_location_manager(content):
    Email   =content['email'][0]  
    user    =User.objects.filter(email=Email)
    if user:
        for userdata in user:
            Send_email(userdata.email,userdata.username,userdata.password) # send email notification 
            userid=userdata.username
            return userid
    else:
        print"here in create"
        empty=None
        User.objects.create(
                username=Email.split("@")[0],
                email=Email,
                password=hashlib.md5(Email).hexdigest(),
                group='Manager',
                is_active=0
            ).save()
        new_user=User.objects.filter(email=Email)
        for user in new_user:
            ''' Send manager account info at manager's email ID '''
            Send_email(user.email,user.username,user.password) # send email notification
            userid=user.username           
    return userid

''' Delete Location '''
def delete_location(request, content):
    Location.objects.filter(id=content['id'][0]).delete()
    return HttpResponse(" Location successfully deleted !!! ");

''' Dispatch email '''
def Send_email(email,username,password):
    try:
        ''' Change manager's account activation info to carried out in a template '''
        login=settings.SITE_NAME+"/accounts/login"
        plaintext = get_template('registration/activation_email.txt')
        htmly     = get_template('email-templates/location_manager_account_info.htm')        
        d = Context({ 'username': username, 'password': password, 'email': email, 'login':login })        
        subject, from_email, to = 'Location Manager Account | Store Info Created Or Updated', 'info@locallenders.net', email
        text_content = plaintext.render(d)
        html_content = htmly.render(d)
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
    except Exception,e:
        res = str(e)
        data = {'error': res}
        return HttpResponse(json.dumps(data), content_type='application/json')

'''Get locations of current Lender using lender Id'''
class getLocationsById(generics.ListAPIView):
    model = Location
    serializer_class = LocationSerializer
    def get_queryset(self):
        queryset = super(getLocationsById, self).get_queryset()
        return queryset.filter(lender_id__id=self.request.user.id)