from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required

from locations.views import *

urlpatterns = patterns('',
    url(r'v1/', include([
        url(r'^get/', getLocationsById.as_view(), name='getLocationsBy-Id'),
        url(r'^add/', process_request),
        url(r'^update/(\d{1,4})/$', process_request),
        url(r'^delete/(\d{1,4})/$', process_request),
    ])),
)