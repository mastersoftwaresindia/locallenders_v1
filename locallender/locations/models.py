from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes import generic

''' Check and fetch if auth_user model has been swipped with custom user model, if yes call custom user model '''
try:
    from django.contrib.auth import get_user_model
    User = get_user_model()
except ImportError:
    from django.contrib.auth.models import User
    
# TODO: Need to test this for the logo field
# From: http://stackoverflow.com/questions/8189800/django-store-user-image-in-model
def get_image_path(instance, filename):
    return os.path.join('logos', str(instance.id), filename)
    
# All models belongs to lender's location application will go here in this module.
''' Lender locations Model Class, set blank and null parameters to avoid exceptional error during database table migrations, @jeevan '''
class Location(models.Model):
    lender_id       = models.ForeignKey(User,blank=True,null=True,related_name="lender_ids")
    location_name   = models.TextField(max_length = 255,blank = False,null = True,db_column = 'location_name')
    added_on        = models.DateField(blank = False,null = True)

    # Freeform text which may be used on goodnews.html page later
    landmark            = models.CharField(max_length=255,blank=True,null=True)
    products_offered    = models.CharField(max_length=255,blank=True,null=True)
    location_tagline    = models.CharField(max_length=255,blank=True,null=True)

    # TODO: Is this how we can set up permissions for the store managers to see multiple locations?
    managers = models.ManyToManyField(User,related_name="managers")

    # This is so they can merge data generated here with their own system data
    lender_store_id = models.CharField(max_length=255,blank=True,null=True)
   
    # These are used for the goodnews.html page
    name    = models.CharField(max_length=255,blank=True,null=True)
    email   = models.CharField(max_length=255,blank=True,null=True)
    phone   = models.CharField(max_length=255,blank=True,null=True)
    address = models.TextField(max_length = 255,blank = False,null = True,db_column = 'address')
    city    = models.CharField(max_length=255,blank=True,null=True)
    state   = models.CharField(max_length=255,blank=True,null=True)
    zipcode = models.CharField(max_length=255,blank=True,null=True)
    phone   = models.CharField(max_length = 255,blank = True,null = True)

    # This is the image for the goodnews.html page
    logo        = models.ImageField(upload_to=get_image_path, blank=True, null=True)

    # Calculated from address
    longitude   = models.FloatField(blank = True,null = True, db_column = 'longitude')
    latitude    = models.FloatField(blank = True,null = True, db_column = 'latitude')

    # Used in services/views.py to determine who to send a lead to
    max_hourly      = models.PositiveIntegerField(_("max_hourly"),blank=True,null=True)
    max_daily       = models.PositiveIntegerField(_("max_daily"),blank=True,null=True)
    max_weekly      = models.PositiveIntegerField(_("max_weekly"),blank=True,null=True)
    max_monthly     = models.PositiveIntegerField(_("max_monthly"),blank=True,null=True)
    count_hourly    = models.PositiveIntegerField(_("count_hourly"),blank=True,null=True)
    count_daily     = models.PositiveIntegerField(_("count_daily"),blank=True,null=True)
    count_weekly    = models.PositiveIntegerField(_("count_weekly"),blank=True,null=True)
    count_monthly   = models.PositiveIntegerField(_("count_monthly"),blank=True,null=True)
    is_active       = models.BooleanField(default=True)
    max_distance_allowed_in_miles = models.FloatField(default = 5)
    time_before_willing_to_accept_same_lead_in_days = models.FloatField(default=100)

    # Used to determine when to reset the count_* variables
    last_reset_on   = models.DateTimeField(blank=False,null=True)
  
    # Amount we are charing this particular location for leads
    lead_price      = models.FloatField(default = 20)

class GoodNewsToken(models.Model):
    location_id = models.ForeignKey('locations.Location')
    lead_id = models.ForeignKey('api.Lead')
    token = models.CharField(max_length=12,blank=False,null=False)
    # TODO: Create a way to track whether or not the customer is redirected to this page.

class LenderAccountTransaction(models.Model):
    transaction_type = models.CharField(max_length=255,blank=True,null=True)
    lead_id = models.ForeignKey('api.Lead')
    location_id = models.ForeignKey('locations.Location')
    transaction_datetime = models.DateTimeField(blank=False,null=True)
    notes = models.CharField(max_length=255,blank=True,null=True)
    transaction_amount = models.DecimalField(max_digits=8, decimal_places=2)
