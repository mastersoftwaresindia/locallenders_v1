import json
from django.contrib.auth.models import User
from django.core.mail import send_mail

from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from freshdesk_api.tickets import *

# NOTE: This was used in a test.
def send_verification_email(new_user):
    ''' Sends verification email via djrill'''
    try:
        subject = 'Welcome to LocalLenders.net!\n\nPlease confirm your email address by clicking on the link below.\n\n'
        from_email = 'info@locallenders.net'
        to = new_user.email
        token = 1
        text_content = 'Hello!'
        #               'Please confirm your new email address by clicking the link below: ' +
        #               settings.SITE_NAME + "/verify?x=" + token 
                       
        html_content = "<p>Hello!</p>"
        #               "<p>Please confirm your new email address by clicking the link below:</p>" +
        #               "<p>" + settings.SITE_NAME+ "/verify?x=" + token + "</p>"
        
        ''' sending mail for account registration confirmation '''
        plaintext   = get_template('registration/activation_email.txt')
        htmly       = get_template('email-templates/account-registered.htm')
        d           = Context({'username': username_tail[0],'email':data['email']})
        subject, from_email, to = 'Locallenders.net | New Lender Account Registered', 'info@locallenders.net', str(data['email'])
        text_content = plaintext.render(d)
        html_content = htmly.render(d)
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        
        
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        if msg:
            return email_sent, "Verification email sucessfully sent."
        else:
            return email_sent, "Error in sending verification email."
    except Exception, e:
        return False, str(e)