from django.template import Context,loader
from django.http import HttpResponse, StreamingHttpResponse
from django.core.context_processors import csrf
from django.conf import settings
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.middleware.csrf import get_token

from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth import authenticate

from app.api.models import *
from locations.models import *
from django.contrib.auth.models import User    
#from django.contrib.auth.models import Location
#from django.contrib.auth.models import LenderAccountTransactions
    
from django.views.generic.base import TemplateView
import json
#
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.db import *
import re
from django.core.mail import send_mail, BadHeaderError
from django.core.mail import EmailMessage
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
import httplib2
import requests
http = httplib2.Http()
from django.utils.encoding import iri_to_uri
from django.utils.http import urlquote
import hashlib
import hmac
import time
import datetime
from pprint import pprint # to remove later
from freshdesk_api import contacts, tickets
import ast
from freshdesk_api.tickets import *
from django.core.mail import send_mail
methods_allowed = ['GET','POST']
requests_allowed_only_from = ['GET','POST']

from geopy.geocoders import Nominatim
from geopy.geocoders import GoogleV3
from geopy.distance import great_circle

from djstripe.models import *
from decimal import *
# TODO:v1.1:Protect passwords so they are not stored in cleartext in the database
# TODO:v1.1:Encrypt SSN and bank account information so they are not stored as cleartext on disk.
# TODO:v2.0:Save the raw data coming from the lead gens to a text file somewhere without any processing, preferably in something like a mongo or google document storage database
from django.db.models import Q

import phonenumbers
from app.api.emails import *

try:
    from django.contrib.auth import get_user_model
    User = get_user_model()
except ImportError:
    from django.contrib.auth.models import User


''' use core functionality to validate an email address '''
def validateEmail( email ):
    try:
        validate_email( email )
        return True
    except ValidationError:
        return False

# TODO:v2.0:Convert table and function names to approved naming conventions.
# TODO:v1.1:Jeevan, please verify that I did this correctly.  I added to allow me to hit the decision service without being logged in.
from django.views.decorators.csrf import csrf_exempt

def respond_to_lead_bearer(response):
    return HttpResponse(json.dumps(response), content_type='application/json')

''' lead listening service '''
# TODO:v1.1:Verify that this approach is correct. I added csrf_exempt decorator to allow it to accept post from outside. After conversation with Jeevan, this appears to be unncessary.
@csrf_exempt
def process_request(request, method, **args):   
    path = request.path
    client_ip =request.META['REMOTE_ADDR']
    content =str(request.GET["data"]).encode('utf8')
    content =content.replace('%25253a', ':')
    content =content.replace('%25252b', ' ')
    content =content.replace('%252b', ' ')
    content =content.replace('%253a', ':')
    content =content.replace('%3a', ':')
    content =content.replace('%2b', ' ')
    response = {}
    # validate request method only post allowed
    if request.META['REQUEST_METHOD'] not in methods_allowed:
        response = {'status':'error','error':'request method not allowed'}
    else:
        slashparts = path.split('/')
        basename = '/'.join(slashparts[:3]) + '/'
        dirname = '/'.join(slashparts[:-1]) + '/'
        # TODO:v1.1:This needs better error handling.
        if slashparts[5] == 'listen':
            content = ast.literal_eval(str(content).encode('utf8'))
            lead_data, parse_mesg = parse_lead_data(content)
            if not lead_data:
                print 'PARSE ERROR: ' + str(parse_mesg)


            # Save all leads to the database EVEN IF WE DO NOT ACCEPTED IT.
            new_lead, lead_mesg = save_lead_data(lead_data)


            print "LEAD DATA: " + str(lead_data)
            
            # Decide whether or not we want to buy the lead
            decision, decision_mesg = decision_engine(lead_data)
            print decision_mesg

            if not decision:
                # TODO:v2.0:Need to set up logging for these events.
                print "Unexpected response from decision engine: " + str(decision_mesg) + " : " + str(lead_data)
                response = {'status':'ERROR','error':'Unable to process account information.'}

            elif not new_lead:
                print "Unexpected error saving lead data: " + str(lead_mesg) + " : " + str(lead_data)
                response = {'status':'ERROR','error':'Unable to process account information.'}

            elif decision == 'y':

                # Message for accepted leads is the reference to the closest location
                location = decision_mesg
                print "LOCATION: " + str(location)
                print "OFFERED_TO: " + str(location.id)
                new_lead.offered_to = location.id  # TODO: This should be a foreign key
                new_lead.is_accepted = 'y'
                new_lead.offered='y'
                new_lead.save()

                # Assign the lead to a location, enter a transaction, and deduct the lead price from the acount balance
                new_assignment, assignment_mesg = assign_lead_to_location(location, new_lead)
                status, new_user, user_mesg = create_user(new_lead)
                if not new_user:
                    print 'ERROR: User not created. ' + user_mesg

                if not new_user:
                    print 'ERROR: User not created after lead purchase: ' + user_mesg

                # Create a ticket at freshdesk to call the customer    
                ticket_id, ticket_mesg = post_lead_to_freshdesk(new_lead,admin_email='williamjwest@gmail.com')
                
                # save the newly created freshdesk ticket_id to the lead table
                new_lead.ticket_id = ticket_id
                new_lead.save()
                    

                # Send tokenized redirect link to the lead bearer who will redirect the customer
                lead_lender_location, redirect_url, redirect_mesg = goodnews_redirect_url(new_lead)
                
                if status=='new':
                    # Add the newly created user_id to the lead table
                    new_lead.user_id = new_user.id # TODO: This should be a foreign key
                    new_lead.save()
                    # Use mandrill to send a verification email to the user
                    send_verification_email_to_customer(new_user, redirect_url)
                    
                if status=='existing':
                    pass
                
                ''' Save lead, lender and location token in database '''
                lead_lender_location_token.objects.create(**lead_lender_location)
                response = {'status':'ACCEPTED','redirect_url': redirect_url }

            elif decision == 'n':
                print "Lead Declined: "
                # For declined leads, the decision_mesg serves as the rejection reason
                new_lead.decision_reason = decision_mesg
                new_lead.is_accepted='n'
                new_lead.save()
                response = { 'status':'REJECTED'}
            else:
                print "Unknown error."
                response = {'status':'REJECTED'}
        else:
            print "I'm not sure what causes this error."
            response = {'status':'REJECTED'}
    return respond_to_lead_bearer(response)


    # TODO:v1.1:Improve email look and feel
    # TODO:v2.0:Setup email templates in mandrill
    # TODO:v1.1:Change default admin email address and suppress notification in freshdesk. 

def goodnews_redirect_url_old(lead):
    ''' creates hash out of email for url redirection '''
    try:
        import string
        import random
        def id_generator(size=12, chars=string.ascii_uppercase + string.digits):
            return ''.join(random.choice(chars) for _ in range(size))
     
    
        # Get reference to assigned location
        location = Location.objects.get(id = lead.offered_to)
    
        # TODO: There must be a cleaner way to do this.
        token = id_generator()
        print token
        try:
            obj = GoodNewsToken.objects.get(lead_id = lead)
            GoodNewsToken.objects.filter(location_id = location).update(token = token)
        except GoodNewsToken.DoesNotExist:
            GoodNewsToken.objects.create(location_id = location,lead_id = lead, token = token)
            
        return settings.SITE_URL + "/goodnews/?token=" + token, "GoodNewsToken created."
    except Exception, e:
        return False, "GoodNewsToken creation error."
    
def goodnews_redirect_url(lead):
    # TODO:v1.1:I need someway for the link to be portable, even if we change who the lead is offered to later.  My vresion above does this.  This version does not, as far as I can tell.
    ''' create hash from lead's email address '''
    try:
        lead_lender_location = {}
        ''' Create unique hash token string '''
        token = hashlib.md5(lead.email+lead.ssn).hexdigest()
        
        ''' Create location object '''
        location = Location.objects.get(id=lead.offered_to)
        
        ''' find lender account associated with the location '''
        user = User.objects.get(id=location.lender_id_id)
        
        ''' Create lead lender location dictionary object '''
        lead_lender_location['lender']     =user
        lead_lender_location['lead']       =lead
        lead_lender_location['location']   =location
        lead_lender_location['token']      =token
        lead_lender_location['added_on']   =datetime.datetime.now()
        lead_lender_location['updated_on'] =datetime.datetime.now()
        
        return lead_lender_location, settings.SITE_URL + "/goodnews/?token=" + token, "GoodNewsToken created."        
    except Exception, e:
        return False, "GoodNewsToken creation error."    

# parse incoming data
# TODO: Choices should all be lowercase for consistency.
def parse_lead_data(content):
    try:
        # Prepare request data
        #req = content.lead_request
        req = content['lead_request']
        # Parse important information
        re_non_numbers = re.compile(r'[^\d]+')
        re_currency = re.compile(r'[^\d.]+')
    
        # Build object of cleaned lead data

        # lead fields are the same as Lead model
        # content.lead_request fields are what we will ask leadflash to provide
        # I created these names to fit with our end state model
        # TODO:v2.0:we should adjust our lead tables to match the content.lead_request wherever we can do so without too much trouble
        # TODO:v2.0:Save lead data in its entirety to a json field in the database
        lead = {}

        # Parse information coming in top level of request
        lead['ref_id']                  = req.get('ref_id')
        lead['client_ip_address']       = req.get('client_ip_address')

        lead['lead_provider']           = req.get('lead_provider')

        # Careful, lead provider is passing a different name
        lead['cost']                    = req.get('lead_price')
        lead['campaign_id']             = req.get('campaign_id')
        lead['campaign_name']           = req.get('campaign_name')
        lead['source_id']               = req.get('source_id')

        lead['first_name']              = req.get('first_name')
        lead['last_name']               = req.get('last_name')

        # Until we increase the length of middle_name, we need to trim it.
        if req.get('middle_name'):
            lead['middle_name']         = req.get('middle_name')[0] 

        # Customer addres information
        lead['street_addr1']            = req.get('home_address')
        lead['city']                    = req.get('home_city')
        lead['state']                   = req.get('home_state')

        # TODO:v2.0:update the database to make this lowercase (zip)
        lead['Zip']                     = req.get('home_zip')

        ## Leadflash does not pass these fields.  We will handle them later.
        #lead['mail_address']            = req.get('mail_address')
        #lead['mail_city']               = req.get('mail_city')
        #lead['mail_state']              = req.get('mail_state')
        #lead['mail_zip']                = req.get('mail_zip') # re_non_numbers
        lead['months_at_address']       = req.get('months_at_address')
        lead['ssn']                     = req.get('ssn')  # re_non_numbers
        lead['dob']                     = req.get('dob')
        lead['drivers_license']         = req.get('drivers_license')
        lead['drivers_license_state']   = req.get('drivers_license_state')
        #lead['own_home']                = req.get('own_home') # Need to change to character(1)
        lead['language']                = req.get('language')
        #lead['is_military']             = req.get('is_military') # Need to change to character
        lead['comments']                = req.get('comments') # Add later
        lead['home_phone']              = req.get('primary_phone')  #re_non_numbers
        lead['cell_phone']              = req.get('alternate_phone')  #re_non_numbers
        #lead['best_time_to_call']       = req.get('best_time_to_call')
        lead['email']                   = req.get('email')
        #lead['fax']                     = req.get('fax')

        # Parse employment information
        if 'employment_info' in req.keys():
            lead['employer']               = req['employment_info'].get('employer') 
            #lead['occupation']             = req['employment_info'].get('occupation') 
            lead['pay_frequency']          = req['employment_info'].get('pay_frequency') 
            #lead['emp_address']            = req['employment_info'].get('address')
            #lead['emp_city']               = req['employment_info'].get('city')
            #lead['emp_state']              = req['employment_info'].get('state') 
            #lead['emp_zip']                = req['employment_info'].get('zip') # Add re_non_numbers val
            #lead['emp_Supervisor']         = req['employment_info'].get('supervisor') # TODO: change to emp_supervisor
            lead['pay_day1']               = req['employment_info'].get('next_payday') # change to a date mm/dd/yyyy format
            lead['pay_day2']               = req['employment_info'].get('second_next_payday') # change to a date mm/dd/yyyy format
            lead['monthly_income']         = req['employment_info'].get('monthly_income') 
            lead['income_type']            = req['employment_info'].get('income_type') 
            lead['months_at_employer']       = req['employment_info'].get('months_at_employer') # change to months_at_employer change to positive integer field
            lead['payroll_type']            = req['employment_info'].get('payroll_type')  # Change to payroll_type
            lead['work_phone']             = req['employment_info'].get('phone') # re_non_numbers
            #lead['work_phone_ext']        = req['employment_info'].get('phone_ext') # re_non_numbers
        
        if 'bank_info' in req.keys():
            lead['bank_account_type']      = req['bank_info'].get('account_type')
            lead['bank_aba']               = req['bank_info'].get('aba_number')
            lead['bank_account']           = req['bank_info'].get('account_number')
            lead['bank_name']              = req['bank_info'].get('bank_name')
            lead['months_at_bank']         = req['bank_info'].get('months_at_bank')
        
        lead['created_on']              = datetime.datetime.now()

        if 'loan_info' in req.keys():
            lead['amount_requested']       = req['loan_info'].get('amount_requested')
    
        return lead, "Lead data parsed."
    except Exception, e:
        return False, str(e)

''' Check lende's account balance '''
def getLenderAccountBalanceById(lender_id):    
    return HttpResponse("Working here for a check if lender has enough cash in his account")

# lead decisioning service
def decision_engine(lead_data,
             max_times_unsold_allowed = 3,
             exclusivity_period_in_days = -2):
    try:
        #print lead_data['ssn']
        # Check to see if SSN is the correct length
        if len(str(lead_data['ssn'])) != 9:
            return 'n', "SSN does not have 9 digits"
      
        # Check for duplicates
        recent_duplicate = Lead.objects.filter(
            Q(ssn = lead_data['ssn']) | 
            Q(email = lead_data['email']) | 
            Q(cell_phone = lead_data['cell_phone']) | 
            Q(home_phone = lead_data['home_phone']),
            offered_to__isnull=False,
            created_on__gt = datetime.datetime.now() - datetime.timedelta(days=exclusivity_period_in_days))

        
        if recent_duplicate:
            return 'n', "SSN, email or phone already sold within exclusivity period."
        
        # Check for fraud where people are cancelling
        num_times_unsold = Lead.objects.filter( 
            ssn = lead_data['ssn'],
            offered_to__isnull = True,
            cost__gt = 0,
            is_accepted = True ).count()
        
        if num_times_unsold > max_times_unsold_allowed:
            return 'n', "SSN went unsold " + str(num_times_unsold) + " time(s)."

        # Check for valid email address
        if not validateEmail(lead_data['email']):
            return 'n', "Email invalid."
        
        # Check for valid phone number
        try:
            lead_phone_number = phonenumbers.parse(lead_data['home_phone'],"US")
        except Exception: 
            return 'n', "Phone number cannot be parsed."
        if not phonenumbers.is_valid_number(lead_phone_number):
            return 'n', "Phone invalid."
        
        # Check to see if last name exists
        if not lead_data['last_name']:
            return 'n', "Lastname is blank: '" + str(lead_data['last_name']) + "'."

        # Make sure google knows where the zip code is
        geo_address = make_geo_address(
                           lead_data['street_addr1'], lead_data['city'],
                           lead_data['state'],lead_data['Zip'])
        print "ADDRESS" ,geo_address
        lead_geoloc = coordinates_from_zip(lead_data['Zip'], geo_address)
        
        if not lead_geoloc:
            return 'n', "Google could not geolocate the zip code."
        
        # Create a pointer to the list of eligible locations.
        locations = Location.objects.filter(
                 max_hourly__gt = 0,
                 max_daily__gt = 0,
                 max_monthly__gt = 0,
                 max_weekly__gt = 0,
                 is_active = True)
        if not locations:
            return 'n', "No lender locations found in database"
        print locations
        # Initialize reference to location table object
        closest_location = None

        # Create a large distance as the initial benchmark
        distance_to_closest_location = 100000.00000 #11007.9407804

        # Loop through all locations to find the closest eligible location
        for location in locations:
            if not (location.longitude and location.latitude):
                location_geoloc = coordinates_from_zip(location.zipcode)
            else:
                location_geoloc = (location.longitude, location.latitude)
            
            # Calculate the distance between lead and location for this location
            distance = great_circle(lead_geoloc, location_geoloc).miles
            print distance
            print "LOCATION GEO : " + str(location.zipcode) + " : " + str(location_geoloc)
            print "BORROWER GEO : " + str(location.zipcode) + " : " + str(lead_geoloc)
            current_datetime = datetime.datetime.now()
            
            # TODO: Verify that the count_hourly variables are reset appropriately by this logic.
            # Reset the counters if this lenders counters have not been reset this hour.
            if location.last_reset_on.hour < current_datetime.hour:
                location.count_hourly = 0
            if location.last_reset_on.date < current_datetime.date:
                location.count_daily = 0
            if location.last_reset_on.strftime("%U") < current_datetime.strftime("%U"):
                location.count_hourly = 0
            if location.last_reset_on.month < current_datetime.month:
                location.count_monthly = 0

            # Print some helpful diagnostics of why a location was rejected.
            print "EVALUATING: " + location.location_name  + "..."
            if (distance <= location.max_distance_allowed_in_miles): print "Distance ok." 
            if (location.count_hourly < location.max_hourly): print "Hourly count ok."
            if (location.count_daily < location.max_daily):   print "Daily count ok."
            if (location.count_weekly < location.max_weekly): print "Weekly count ok."
            if (location.count_monthly < location.max_monthly ): print "Monthly count ok."
            if (location.is_active == True): print "Location is active."
            
            if (location.lender_id.account_balance >= location.lender_id.min_balance_required_to_receive_leads):
                print "Balance is enough." 
            else: 
                print "OUT!  Not enough money: " 
            
            # Qualify this lender for the lead
            if (distance <= location.max_distance_allowed_in_miles and
                location.count_hourly < location.max_hourly and
                location.count_daily < location.max_daily and
                location.count_weekly < location.max_weekly and
                location.count_monthly < location.max_monthly and
                location.is_active == True and
                location.lender_id.account_balance >= location.lender_id.min_balance_required_to_receive_leads):

                #If this is closer than all previous locations, set this as the benchmark
                if distance < distance_to_closest_location:
                    closest_location = location
                    distance_to_closest_location = distance

            # TODO:v1.1:Need a function which can be called when lead assignment changes.  This will look to see if it was recently sold and if  it were sold, credit the previous owner of the lead.
     
            if not closest_location:
                return 'n', "No eligible lenders within the maximum distance in mile(s)."
            else:
                return 'y', closest_location 
    except Exception, e:
        return False,str(e)


def assign_lead_to_location(location, lead):           
    try:
        # Get the lender information from the user table
        lender = User.objects.get(username = location.lender_id)

        # TODO: change type of lead_price to decimal
        # Reduce the balance by the lead price
        lead_price = location.lead_price
        lender.account_balance = lender.account_balance - Decimal(lead_price) 
        lender.save()

        # Create record in a transaction table
        transaction = LenderAccountTransaction.objects.create(
                transaction_type = 'Lead Sold to Lender',
                # TODO: Need to save lead_data first for all leads.
                lead_id = lead,
                location_id = location,
                transaction_datetime = datetime.datetime.now(),
                notes = '',
                transaction_amount = -lead_price
        )
        return transaction, "Transaction entered successfully."
    except Exception, e:
        return False,str(e)

def create_user(new_lead):
    #'''add lead in user tabel'''
    try:
        new_user=User.objects.filter(email=new_lead.email)
        # TODO:v2.0:When the email is verified for an existing user, update the account information with info from the new lead. 
        # Only create a new user when the email not in the database.
        if not new_user:
            if new_lead.cell_phone:
                # TODO:v2.0 rename home phone -> primary, cell_phone -> alternate
                # Home phone is primary, cell phone is alternate
                phone_number = new_lead.home_phone
            else:
                phone_number = new_lead.cell_phone

            # TODO:v1.1:Verify that the process for verifying an email address exists. Do we have something in user table to indicate that the email has been verified?
            geo_address = make_geo_address(
                           new_lead.street_addr1, new_lead.city,
                           new_lead.state,new_lead.Zip)
            
            geo_coordinates = coordinates_from_zip(new_lead.Zip,geo_address)
            new_user = User.objects.create(entityname=None,
                    first_name = new_lead.first_name,
                    last_name = new_lead.last_name,
                    password =  User.objects.make_random_password(),
                    email = new_lead.email,
                    group = 'Customers',
                    state = new_lead.state,
                    phone_number = new_lead.cell_phone, # need to align the user fields with the lead fields
                    zip_code = new_lead.Zip,
                    longitude = geo_coordinates[1],
                    latitude = geo_coordinates[0],
                    is_active = 0,
                    city = new_lead.city,
                    username=new_lead.email.split('@')[0],
                    )
            return "new", new_user, "New user created: " + str(new_user)
        else:
            print "new_user exists already:"
            return  "existing", new_user, "Existing user id returned: " + str(new_user)
    except Exception, e:
        return "error", False, str(e)

# TODO: We will need some way to filter for Lenders in the django admin.

def save_lead_data(lead):
    ''' lead add in lead tabel '''
    print lead['email']
    try:
        lead['created_on'] = datetime.datetime.now().replace(microsecond=0)
        new_lead = Lead.objects.filter(email=lead['email'])
        if not new_lead:
            new_lead    = Lead.objects.create(**lead)
        else:
            new_lead = Lead.objects.get(email=lead['email'])
        return new_lead, "New lead created or same lead exists"
    except Exception, e:
        print "Unable to add", e
        return False, str(e)

# TODO: Why is this not using mandrill.
# TODO: See my html version below.
def send_verification_email_old(new_user):
    ''' Dispatch email '''
    # TODO: Update with Mandrill and pull information through user_id instead of being passed.
    try:
        message1    = "Welcome to LocalLenders.net!\n\nPlease confirm your email address by clicking on the link below.\n\n"
        token       = 1; 
        message2    = settings.SITE_NAME+"/accounts/register/?token="+token
        message3    ="\n\nLocalLenders.net"
        email_sent  =send_mail("Welcome to LocalLenders.net",
                        message1+message2+message3, email,
                        new_user.email,
                        fail_silently=False)
        if email_sent:
            return email_sent, "Verification email sucessfully sent."
        else:
            return email_sent, "Error in sending verification email."
    except Exception, e:
        return False, str(e)

# NOTE: This was used in a test.
def send_verification_email2(new_user):
    ''' Sends verification email via djrill'''
    try:
        from django.core.mail import EmailMultiAlternatives
        subject = 'Welcome to LocalLenders.net!\n\nPlease confirm your email address by clicking on the link below.\n\n'
        from_email = 'info@locallenders.net'
        to = new_user.email
        token = 1
        text_content = 'Hello!'
        #               'Please confirm your new email address by clicking the link below: ' +
        #               settings.SITE_NAME + "/verify?x=" + token 
                       
        html_content = "<p>Hello!</p>"
        #               "<p>Please confirm your new email address by clicking the link below:</p>" +
        #               "<p>" + settings.SITE_NAME+ "/verify?x=" + token + "</p>" 
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        plaintext   = get_template('registration/activation_email.txt')
        htmly       = get_template('email-templates/account-registered.htm')
        d           = Context({'username': new_user.username,'email' : new_user.email})
        subject, from_email, to = 'Locallenders.net | New Lender Account Registered', 'info@locallenders.net', str(new_user.email)
        text_content = plaintext.render(d)
        html_content = htmly.render(d)
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        if msg:
            return email_sent, "Verification email sucessfully sent."
        else:
            return email_sent, "Error in sending verification email."
    except Exception, e:
        return False, str(e)
    
# TODO: v2 task only.  Not for v1.1.
# TODO:v2.0:Create function to return the number of active agents and the number of unworked tickets.  Setup decision logic so that we only buy leads when we have a relatively small queue.  This is going to be awesome, by the way, and is possibly our best competitive advantage.

''' Generate freshdesk ticket on api hit '''
def post_lead_to_freshdesk(new_lead,
                           admin_email="info@locallenders.net"):
    try:
        if new_lead.phone_cell:
            phone = lead.phone_cell
        else:
            if new_lead.phone_home:
                phone = lead.phone_home
            else:
                phone = 'No phone number.'
    
        title="New Lead: " + phone
        des  ="\u003Cdiv\u003E"
        des +="Name: "+new_lead.first_name + ' ' + new_lead.last_name + "\n"
        des +="\nEmail:"+new_lead.Email + "\n"
        des +="\nHome Phone: " + new_lead.phone_home + "\n"
        des +="\nCell Phone: " + new_lead.phone_cell + "\n"
        des  ="\u003C/div\u003E"
        ticket_data = { 'des': des, 'sub':title , 'email': admin_email}
        ticket_id=ticket_create(ticket_data);
        return ticket_id, "Fresh desk ticket created for lead_id: " + new_lead.id
    except Exception, e:
        return False, e

# LOOK FOR A VALID LENDER
# Validate the zip code
def make_geo_address(address,city,state,zip):
    full_address = address + ', ' + city + ', ' + state + ', ' + zip
    return full_address

def coordinates_from_zip(zip,address=None):
    
    geolocator = GoogleV3()
    if address:
        geo = geolocator.geocode(address,exactly_one=True)
    
    if not geo:
        geo = geolocator.geocode(zip,exactly_one=True)
    
    if geo:
        return (geo.longitude,geo.latitude)
    else:
        print "In geo code, failed to get codes"
        return False, "Google unable to geolocate zip code: " + zip
