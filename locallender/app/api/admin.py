from django.contrib import admin

from .models import User, Lead, lender_onetime_payment, Lead_Limit, lead_lender_location_token

admin.site.register(User)
admin.site.register(Lead)
admin.site.register(lender_onetime_payment)
admin.site.register(Lead_Limit)
admin.site.register(lead_lender_location_token)
