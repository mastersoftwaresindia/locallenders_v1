from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes import generic

# TODO:v2.0:what are followers, service?
# TODO:v2.0:we should change the names to be lowecases and underscores
# TODO:v2.0:We really need to split out Users and Companies, but only after we are live
# TODO:v2.0:We should change the name of social_security to ssn
# TODO:v2.0:We should create fixed choices for the groups.
class User(AbstractUser):
    followers = models.ManyToManyField('self', related_name='followees', symmetrical=False)
    phone_number = models.CharField(max_length=255,blank=True,null=True,default=None)
    zip_code = models.PositiveIntegerField(_("zip_code"),blank=True,null=True,default=None)
    group = models.CharField(max_length=255,blank=True,null=True,default=None)
    entityname = models.CharField(max_length=255,blank=True,null=True,default=None)
    state = models.CharField(max_length=255,blank=True,null=True,default=None)
    ownerName = models.CharField(max_length=255,blank=True,null=True,default=None)
    ownerEmail = models.CharField(max_length=255,blank=True,null=True,default=None)
    ownerPhone = models.PositiveIntegerField(_("ownerPhone"),blank=True,null=True,default=None)
    product_at_store = models.CharField(max_length=255,blank=True,null=True,default=None)
    storeName = models.CharField(max_length=255,blank=True,null=True,default=None)
    storeAddress = models.CharField(max_length=255,blank=True,null=True,default=None)
    EinNumber = models.CharField(max_length=255,blank=True,null=True,default=None)
    managerName = models.CharField(max_length=255,blank=True,null=True,default=None)
    managerEmail = models.CharField(max_length=255,blank=True,null=True,default=None)
    managerPhone = models.CharField(max_length=255,blank=True,null=True,default=None)
    longitude = models.FloatField(blank=True,null=True,default=None)
    latitude = models.FloatField(blank=True,null=True,default=None)
    loanLimit =  models.FloatField(blank=True,null=True,default=None)
    city = models.CharField(max_length=255,blank=True,null=True,default=None)
    service = models.CharField(max_length=255,blank=True,null=True,default=None)
    lead_conversion = models.PositiveIntegerField(_("lead_conversion"),blank=True,null=True,default=None)
    agreement = models.BooleanField(default=0)
    min_balance_required_to_receive_leads = models.FloatField(default = 20)
    account_balance = models.DecimalField(max_digits=8, decimal_places=2,default=0)
    test = models.CharField(max_length=10,blank=True,null=True,default=None)
    # TODO: Does this work?
    #@property
    #def account_balance(self):
    #    return "$%s" % self.price

class UserProfile(models.Model):
    user = models.ForeignKey('api.User')
    avatar = generic.GenericRelation(
        'user_media.UserMediaImage',
    )

# TODO: What is this? Can we delete it?
class Post(models.Model):
    author = models.ForeignKey(User, related_name='posts')
    title = models.CharField(max_length=255)
    body = models.TextField(blank=True, null=True)

# TODO: What is this?  Can we delete it?
class Photo(models.Model):
    post = models.ForeignKey(Post, related_name='photos')
    image = models.ImageField(upload_to="%Y/%m/%d")

'''Leads model to save lead data'''
class Lead(models.Model):
    Status = (
    ('y', 'yes'),
    ('n', 'no'),
    (None,'none')
    )

    Language = (
    ( 'e','English'),
    ( 's','Spanish'),
    ( 'o','Other'),
    (None,'None'))

    PayFrequency = (
    ('w', 'Weekly'),
    ('b', 'Biweekly'),
    ('s', 'Semimonthly'),
    ('m', 'Monthly'),
    ('o', 'Other'),
    (None,'None'))

    IncomeType = (
    ('e','Employment'),
    ('s','Social Security'),
    ('d','Disability'),
    ('u','Unemployment'),
    ('i','Self Employed'),
    ('r','Retirement'),
    ('o','Other'),
    (None,'None'))
    
    AccountType = (
    ('c','Checking'),
    ('s','Savings'),
    ('d','Debit Card'),
    ('o','Other'),
    (None,'None'))
    
    PayrollType = (
    ('cash','Cash'),
    ('check','Check'),
    ('direct','Direct Deposit'),
    ('other','Other'),
    (None,'None'))
    
    States = (
    ('AL','ALABAMA'),
    ('AK','ALASKA'),
    ('AZ','ARIZONA'),
    ('AR','ARKANSAS'),
    ('CA','CALIFORNIA'),
    ('CO','COLORADO'),
    ('CT','CONNECTICUT'),
    ('DE','DELAWARE'),
    ('FL','FLORIDA'),
    ('GA','GEORGIA'),
    ('HI','HAWAII'),
    ('ID','IDAHO'),
    ('IL','ILLINOIS'),
    ('IN','INDIANA'),
    ('IA','IOWA'),
    ('KS','KANSAS'),
    ('KY','KENTUCKY'),
    ('LA','LOUISIANA'),
    ('ME','MAINE'),
    ('MD','MARYLAND'),
    ('MA','MASSACHUSETTS'),
    ('MI','MICHIGAN'),
    ('MN','MINNESOTA'),
    ('MS','MISSISSIPPI'),
    ('MO','MISSOURI'),
    ('MT','MONTANA'),
    ('NE','NEBRASKA'),
    ('NV','NEVADA'),
    ('NH','NEW HAMPSHIRE'),
    ('NJ','NEW JERSEY'),
    ('NM','NEW MEXICO'),
    ('NY','NEW YORK'),
    ('NC','NORTH CAROLINA'),
    ('ND','NORTH DAKOTA'),
    ('OH','OHIO'),
    ('OK','OKLAHOMA'),
    ('OR','OREGON'),
    ('PA','PENNSYLVANIA'),
    ('RI','RHODE ISLAND'),
    ('SC','SOUTH CAROLINA'),
    ('SD','SOUTH DAKOTA'),
    ('TN','TENNESSEE'),
    ('TX','TEXAS'),
    ('UT','UTAH'),
    ('VT','VERMONT'),
    ('VA','VIRGINIA'),
    ('WA','WASHINGTON'),
    ('WV','WEST VIRGINIA'),
    ('WI','WISCONSIN'),
    ('WY','WYOMING'),
        )
    
    first_name      = models.CharField(max_length=50,blank=False,null=True,default=None)    # checked
    last_name       = models.CharField(max_length=50,blank=False,null=True,default=None)    # checked
    middle_name         = models.CharField(max_length=1,blank=True,null=True,default=None)  # checked
    street_addr1        = models.CharField(max_length=50,blank=False,null=True,default=None)    # not changed
    street_addr2        = models.CharField(max_length=50,blank=True,null=True,default=None)
    city            = models.CharField(max_length=100,blank=False,null=True,default=None)   # not changed
    state           = models.CharField(max_length=2,blank=False,null=True,default=None) # not changed
    Zip             = models.PositiveIntegerField(_("Zip"),blank=False,null=True,default=None)
    ssn             = models.PositiveIntegerField(_("social_security"),blank=False,null=True,default=None) #checked
    home_phone      = models.CharField(max_length=35,blank=False,null=True,default=None)    # checked
    cell_phone      = models.CharField(max_length=35,blank=True,null=True,default=None)     # chekced
    work_phone      = models.CharField(max_length=35,blank=False,null=True,default=None)    # checked
    work_phone_ext      = models.CharField(max_length=35,blank=True,null=True,default=None) # checked
    email           = models.EmailField(max_length=50,blank=False,null=False)       # checked
    dob             = models.CharField(max_length=35,blank=False,null=True,default=None)
    #employer_name       = models.CharField(max_length=35,blank=False,null=True,default=None)    # checked
    pay_frequency       = models.CharField(max_length=12,blank=False,null=True,choices=PayFrequency,default=None)  # checked
    #direct_deposit      = models.PositiveIntegerField(_("direct_deposite"),blank=False,null=True,default=None)
    pay_day1        = models.CharField(max_length=20,blank=False,null=True,default=None)    # checked
    pay_day2        = models.CharField(max_length=20,blank=False,null=True,default=None)    # checked
    bank_aba        = models.PositiveIntegerField(_("bank_aba"),blank=False,null=True,default=None)   # checked
    bank_account        = models.PositiveIntegerField(_("bank_account"),blank=False,null=True,default=None) # checked
    bank_name       = models.CharField(max_length=30,blank=False,null=True,default=None)    # checked
    monthly_income      = models.CharField(max_length=30,blank=False,null=True,default=None)    # checked
    #TODO:v2.0:Change this to a 1 character string
    own_home        = models.PositiveIntegerField(blank=False,null=True,choices=Status,default=None)  # checked
    drivers_license     = models.CharField(max_length=25,blank=False,null=True,default=None) #checked
    drivers_license_state   = models.CharField(max_length=2,blank=False,null=True,default=None)  #checked
    client_url_root     = models.URLField(max_length=100,blank=False,null=True,default=None)
    client_ip_address   = models.IPAddressField(null=True, blank=False, default=None)       # checked
    email_alternate     = models.EmailField(max_length=100,blank=True,null=True,default=None)
    
    income_type         = models.CharField(max_length=30,blank=False,null=True,choices=IncomeType,default=None)   # checked
    #TODO:v2.0:Change this to a 1 character string
    is_military         = models.PositiveIntegerField(blank=False,null=True,choices=Status,default=None)  #checked
    bank_account_type   = models.CharField(max_length=10,blank=False,null=True,choices=AccountType,default=None)  # checked
    amount_requested    = models.PositiveIntegerField(_("requested_amount"),blank=False,null=True,default=None)   # checked 
    months_at_address   = models.PositiveIntegerField(_("months_at_address"),blank=False,null=True,default=None) #checked
    months_at_bank      = models.PositiveIntegerField(_("months_at_bank"),blank=False,null=True,default=None)    #checked
    #source          = models.CharField(max_length=10,blank=False,null=True,default=None)
    payroll_type         = models.CharField(max_length=10,blank=False,null=True,choices=PayrollType,default=None)
    #gender          = models.CharField(max_length=10,blank=False,null=True,default=None)
    language        = models.CharField(max_length=10,blank=False,null=True,default=None,choices=Language)    # checked
    #homeaddress         = models.CharField(max_length=255,blank=False,null=True,default=None)
    mail_address        = models.CharField(max_length=255,blank=False,null=True,default=None)   # checked 
    mail_city       = models.CharField(max_length=255,blank=False,null=True,default=None)   # checked
    mail_state      = models.CharField(max_length=10,blank=False,null=True,default=None)    # checked
    mail_zip        = models.CharField(max_length=10,blank=False,null=True,default=None)
    
    best_time_to_call   = models.CharField(max_length=10,blank=False,null=True,default=None)    # checked
    #checkingaccount     = models.CharField(max_length=10,blank=False,null=True,default=None)
    fax             = models.CharField(max_length=20,blank=False,null=True,default=None)    # checked
    #accounttouse        = models.CharField(max_length=10,blank=False,null=True,default=None)
    #accountlength       = models.CharField(max_length=10,blank=False,null=True,default=None)
    occupation      = models.CharField(max_length=50,blank=False,null=True,default=None)    # chekced
    Shift           = models.CharField(max_length=10,blank=False,null=True,default=None)
    months_at_employer    = models.CharField(max_length=10,blank=False,null=True,default=None)
    employer        = models.CharField(max_length=255,blank=False,null=True,choices=AccountType,default=None)
    emp_address         = models.CharField(max_length=50,blank=False,null=True,default=None)    # checked
    emp_city        = models.CharField(max_length=50,blank=False,null=True,default=None)    # checked
    emp_state       = models.CharField(max_length=10,blank=False,null=True,default=None)    # checked
    emp_zip         = models.CharField(max_length=10,blank=False,null=True,default=None)    # checked
    emp_Supervisor      = models.CharField(max_length=10,blank=False,null=True,default=None)    # checked
    #emp_phone       = models.CharField(max_length=25,blank=False,null=True,default=None)
    #emp_phoneext        = models.CharField(max_length=10,blank=False,null=True,default=None)
    #references_firstname    = models.CharField(max_length=50,blank=False,null=True,default=None)
    #references_lastname     = models.CharField(max_length=50,blank=False,null=True,default=None)
    #references_phone    = models.CharField(max_length=25,blank=False,null=True,default=None)
    #references_relation     = models.CharField(max_length=100,blank=False,null=True,default=None)
    user_id         = models.PositiveIntegerField(blank=True,null=True,default=None) # TODO: Change to foreign key, null=True
    offered_to      = models.PositiveIntegerField(blank=True,null=True,default=None)
    offered         = models.CharField(max_length=1,blank=True,null=True,default=None,choices=Status)
    created_on      = models.DateTimeField(blank=False,null=True,default=None)
    updated_on      = models.DateTimeField(blank=True,null=True,default=None)
    ticket_id       = models.PositiveIntegerField(blank=False,null=True,default=None)
    cost            = models.DecimalField(decimal_places=2, max_digits=7,default=0.00)
    #source          = models.CharField(max_length=25,blank=True,null=True,default=None)
    is_accepted         = models.NullBooleanField(blank=True,null=True,default=None)
    decision_reason     = models.CharField(max_length=255,blank=False,null=True,default=None)
    ref_id          = models.CharField(max_length=25,blank=True,null=True,default=None)     # checked
    comments        = models.CharField(max_length=255,blank=True,null=True,default=None)    # checked
    lead_provider = models.CharField(max_length=50,blank=True,null=True,default=None) 
    campaign_id = models.CharField(max_length=50,blank=True,null=True,default=None) 
    campaign_name = models.CharField(max_length=50,blank=True,null=True,default=None) 
    source_id = models.CharField(max_length=50,blank=True,null=True,default=None) 
'''Status Codes Model'''
class StatusCodes(models.Model):
    Lead_Status = (
    ('101','Have not gone yet'),
    ('102','On my way'),
    ('103','Got my Loan')
    )
    Lead_Status_Codes = (
    ('101','101'),
    ('102','102'),
    ('103','103')
    )
    name = models.CharField(max_length=100,blank=False,null=False,choices=Lead_Status,db_column='name')
    value = models.PositiveIntegerField(_("value"),blank=False,null=False,choices=Lead_Status_Codes,db_column='value')




'''Lead-Status Model'''
class LeadStatus(models.Model):
    lead_id = models.PositiveIntegerField(_("value"),blank=False,null=True,db_column='lead_id')
    status_id = models.PositiveIntegerField(_("value"),blank=False,null=True,db_column='status_id')
    added_on = models.DateField(blank=False,null=True)
    updated_on = models.DateField(blank=True,null=True)

'''user registration registration'''    
class Leadsstatuscode(models.Model):
    user_id = models.PositiveIntegerField(_("value"),blank=False,null=True,db_column='user_id')
    name = models.CharField(max_length=50,blank=False,null=True)
    status_code = models.CharField(max_length=200,blank=False,null=True,db_column='status_code')
    
'''Appointments modal'''
class Appointments(models.Model):
    customer_id=models.IntegerField(_("value"),blank=False,null=True,db_column='customer_id')
    lender_id=models.IntegerField(_("value"),blank=False,null=True,db_column='lender_id')
    scheduled_by=models.CharField(max_length=20,blank=False,null=True,db_column='scheduled_by')
    description=models.CharField(max_length=1000,blank=False,null=True,db_column='description')
    scheduled_at=models.DateField(blank=False,null=True)
    added_on=models.DateField(blank=False,null=True)
    updated_on=models.DateField(blank=False,null=True)
    status_code_id=models.IntegerField(_("value"),blank=False,null=True,db_column='status_code_id') 
    location=models.CharField(max_length=100,blank=False,null=True,db_column='location')
    note=models.CharField(max_length=1000,blank=False,null=True,db_column='note')

    
'''Status model with types'''
# TODO: Should be StatusTypes, right?
class Statustypes(models.Model):
    relation_to = models.CharField(max_length=20,blank=False,null=True,db_column='relation_to')
    status_code = models.IntegerField(_("value"),blank=False,null=True,db_column='status_code')
    name = models.CharField(max_length=50,blank=False,null=True)
    

class LeadDecisioning(models.Model):
    ip_address=models.CharField(max_length=50,blank=False,null=True,db_column='ip_address')
    social_security=models.CharField(max_length=20,blank=False,null=True,db_column='social_security_num')
    Decision_status=models.CharField(max_length=20,blank=False,null=True,db_column='Decision_status')
    decisioning_data=models.TextField(max_length=200,blank=False,null=True,db_column='decisioning_data')
    added_on = models.DateField(blank=False,null=True)  
    
'''Lender Leads Request model'''
class Lenderleadsrequest(models.Model):
    lender_id=models.IntegerField(_("value"),blank=False,null=True,db_column='lender_id')
    leads_requested=models.IntegerField(_("value"),blank=False,null=True,db_column='leads_requested')
    request_status_code=models.CharField(max_length=20,blank=False,null=True,db_column='request_status_code')
    added_on = models.DateField(blank=False,null=True)
    updated_on = models.DateField(blank=True,null=True)

'''Lender Leads new Request model'''
class Lead_Request(models.Model):
    daily_set=models.IntegerField(_("value"),blank=False,null=True,db_column='daily_set')
    hourly_set=models.IntegerField(_("value"),blank=False,null=True,db_column='hourly_set')
    weekly_set=models.IntegerField(_("value"),blank=False,null=True,db_column='weekly_set')
    monthly_set=models.IntegerField(_("value"),blank=False,null=True,db_column='monthly_set')
    status=models.CharField(max_length=20,blank=False,null=True,db_column='status')
    lead_limit_id=models.IntegerField(_("value"),blank=False,null=True,db_column='lead_limit_id')
    added_on = models.DateField(blank=False,null=True)
    updated_on = models.DateField(blank=True,null=True)
    limit_diff=models.IntegerField(_("value"),blank=False,null=True,db_column='limit_diff')
    
    
    
'''Lead-Limit model for the leads limit set by staff'''
class Lead_Limit(models.Model):
    lender_account_id=models.IntegerField(_("value"),blank=False,null=True,db_column='lender_account_id')
    daily_set=models.IntegerField(_("value"),blank=False,null=True,db_column='daily_set')
    hourly_set=models.IntegerField(_("value"),blank=False,null=True,db_column='hourly_set')
    weekly_set=models.IntegerField(_("value"),blank=False,null=True,db_column='weekly_set')
    monthly_set=models.IntegerField(_("value"),blank=False,null=True,db_column='monthly_set')
    lead_limit_status=models.CharField(max_length=20,blank=False,null=True,db_column='lead_limit_status')
    added_on = models.DateField(blank=False,null=True)
    updated_on = models.DateField(blank=True,null=True)
    limit_diff=models.IntegerField(_("value"),blank=False,null=True,db_column='limit_diff')
    
'''Lead Assignment Model'''
class Lead_Assignment(models.Model):
    staff_id=models.IntegerField(_("value"),blank=False,null=True,db_column='staff_id')
    lender_id=models.IntegerField(_("value"),blank=False,null=True,db_column='lender_id')
    lead_id=models.IntegerField(_("value"),blank=False,null=True,db_column='lead_id')
    stripe_customer_account_id = models.IntegerField(_("value"),blank=False,null=True,db_column='stripe_customer_account_id')
    status=models.CharField(max_length=20,blank=False,null=True,db_column='status')
    added_on = models.DateField(blank=False,null=True)
    accepting = models.BooleanField()
    
'''Lender One time payment'''
class lender_onetime_payment(models.Model):
    #lender_id=models.IntegerField(_("value"),blank=False,null=True,db_column='lender_id')
    lender      =models.ForeignKey(User,blank=True,null=True,related_name="lender_id")
    stripe_id   =models.TextField(max_length=200,blank=True,null=True,db_column='stripe_id')
    amount      =models.DecimalField(decimal_places=2, max_digits=7,db_column='amount')
    paid_status     =models.BooleanField()
    customer_card   =models.TextField(max_length=200,blank=True,null=True,db_column='customer_card')
    added_on    =models.DateField(blank=False,null=True)
    data        =models.TextField(max_length=200,blank=True,null=True,db_column='data')
    
''' Lead, Lender and location connection store '''
class lead_lender_location_token(models.Model):
    lender      =models.ForeignKey(User,blank=True,null=True,related_name="lender")
    lead      	=models.ForeignKey(Lead,blank=True,null=True,related_name="lead")
    location    =models.ForeignKey('locations.Location',blank=True,null=True,related_name="location")
    token       =models.TextField(max_length=200,blank=True,null=True,db_column='token')
    added_on    =models.DateField(blank=False,null=True)
    updated_on  =models.DateField(blank=False,null=True)