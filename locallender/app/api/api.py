import json
from .models import *
from helpdesk.models import *
from rest_framework import serializers
from rest_framework import generics, permissions
from django.http import HttpResponse, StreamingHttpResponse
from django.views.generic.base import View
from django.core.validators import validate_email
from .serializers import UserSerializer, PostSerializer, PhotoSerializer
from .models import User, Post, Photo, lender_onetime_payment
from .permissions import PostAuthorCanEditPermission
from django.views.generic.base import TemplateView
from django.conf import settings
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, Http404
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache
from django.utils.encoding import iri_to_uri
from django.utils.http import urlquote
from django.core.exceptions import ImproperlyConfigured
from django.contrib.auth import authenticate
import hashlib
import hmac
import time
from django.core.mail import send_mail
from django.contrib.auth import login, logout
from django.views.generic.base import RedirectView

from django.core.urlresolvers import reverse

import sys
from geopy.geocoders import Nominatim
from geopy.geocoders import GoogleV3
from geopy.distance import great_circle

from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from django.utils import timezone
import datetime
import stripe
from freshdesk_api.tickets import *
from itertools import chain
import monitio
from django.contrib import messages
from notify import *
from django.contrib.auth.models import User
#from connections import define_relationship
#from calendar_sms.sms import sendSMS
from itertools import chain
#from notifier.shortcuts import create_notification
#from notifier.shortcuts import send_notification
from StringIO import StringIO
import pycurl
from django.core.mail import send_mail
import ast
from collections import Counter
import datetime
import decimal
from django.db.models.base import ModelState
from hellosign_sdk import HSClient
from datetime import  timedelta
from django.utils import timezone

from django.core.mail import send_mail
import itertools
import urlparse
from locations.models import *
from emails import *

try:
    from django.contrib.auth import get_user_model
    User = get_user_model()
except ImportError:
    from django.contrib.auth.models import User



'''class to pass date object as json'''
class DateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
       if hasattr(obj, 'isoformat'):
           return obj.isoformat()
       elif isinstance(obj, decimal.Decimal):
           return float(obj)
       elif isinstance(obj, ModelState):
           return None
       else:
           return json.JSONEncoder.default(self, obj)

'''Function to get weekly reports generated and seng to admin'''
class GenerateWeeklyReports(View):
    def get(self, request):
        try:
            N = 7
            week_ago = timezone.now().date() - timedelta(days=N)
            print week_ago
            keys = ['ip_address','ssn','dec_status','decisioning_data','added_on']
            dictList = []
            lead_decisioning_data=LeadDecisioning.objects.filter(added_on__gt=week_ago)
            if lead_decisioning_data:
                for data in lead_decisioning_data:
                    ip_address=data.ip_address
                    ssn=data.social_security_num
                    dec_status=data.Decision_status
                    decisioning_data=data.decisioning_data
                    added_on=data.added_on
                    lenderinfo = [ip_address,ssn,dec_status,decisioning_data,added_on]
                    dictList.append(dict(zip(keys, lenderinfo)))
                admins=settings.ADMINS
                print "admins",admins
                for admin in admins:
                    print "admin",admin[1]
                    staff_email=admin[1]
                    print "member_email",staff_email
                    plaintext = get_template('registration/activation_email.txt')
                    htmly     = get_template('email-templates/week_report.twig')
                    d = Context({'dictList':dictList})
                    subject, from_email, to = 'Lead Bearer Payments Weekly Report', 'Locallender@Locallenders.net', staff_email
                    text_content = plaintext.render(d)
                    html_content = htmly.render(d)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                data = {'result':'success','details': " Success"}
                return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                data = {'result':'error','details': " No data in leads_decision"}
                return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json') 

'''Get Lender agreement info'''
class GetAgreementInfo(View):
    def get(self, request):
        try:
            print "inside get agreement"
            current_user=request.user
            lender_id=current_user.id
            print "lender_id",lender_id
            user=User.objects.filter(id=lender_id)
            for u in user:
                agreement=u.agreement
                print "agreement",agreement
            data = {'result':'success','agreement_status': agreement}
            return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')    


'''Save Lender info edited by staff'''
class AcceptAgreement(View):
    def get(self, request):
        # <view logic>
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            print "inside accept Agreement",data
            current_user=request.user
            lender_id=current_user.id
            print "lender_id",lender_id
            User.objects.filter(id=lender_id).update(agreement='1')
            data = {'result':'success','details': "Lender Information updated successfully"}
            return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json') 


'''Save Lender info edited by staff'''
class SaveLenderChanges(View):
    def get(self, request):
        # <view logic>
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            print "inside save lender changes",data
            lender_id=data['lender_id']
            lender_email=data['lender_email']
            store_phone=data['store_phone']
            store_address=data['store_address']
            manager_name=data['manager_name']
            owner_email=data['owner_email']
            store_name=data['store_name']
            manager_email=data['manager_email']
            owner_phone=data['owner_phone']
            manager_phone=data['manager_phone']
            zip_code=data['zip_code']
            User.objects.filter(id=lender_id).update(email=lender_email,phone_number=store_phone,
            storeAddress=store_address,managerName=manager_name,ownerEmail=owner_email,storeName=store_name,
            managerEmail=manager_email,ownerPhone=owner_phone,managerPhone=manager_phone,zip_code=zip_code)
            data = {'result':'success','details': "Lender Information updated successfully"}
            return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')

'''Get customer email from ssn for accounts/register'''
class GetCustomerEmail(View):
    def get(self, request):
        # <view logic>
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            print "inside GetCustomerEmail",data
            ssn=data['ssn']
            customer_info = Lead.objects.filter(social_security=ssn)
            for customer in customer_info:
                customer_email = customer.Email
            print "customer_email",customer_email
            data = {'result':'success','info': customer_email}
            return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')    

'''Leads Limit set by staff member for a lender'''
class SetLeadsLimit(View):
    def get(self, request):
        # <view logic>
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            print "inside SetLeadsLimit",data
            lender_id=data['lender_id']
            hourly_set=data['hourly_set']
            daily_set=data['daily_set']
            weekly_set=data['weekly_set']
            monthly_set=data['monthly_set']
            added_on=datetime.datetime.now()
            updated_on=datetime.datetime.now()
            lender=User.objects.filter(id = lender_id)
            for data in lender:
                email=data.email
            already_set=Lead_Limit.objects.filter(lender_account_id=lender_id)
            if already_set:
                n=None
                Lead_Limit.objects.filter(lender_account_id=lender_id).update(hourly_set=hourly_set,
                daily_set=daily_set,weekly_set=weekly_set,monthly_set=monthly_set,updated_on=updated_on,
                lead_limit_status=400,limit_diff=n)
                plaintext = get_template('registration/activation_email.txt')
                htmly     = get_template('email-templates/leadlimit_updated.htm')
                d = Context({'hourly_set':hourly_set,'daily_set':daily_set,'weekly_set':weekly_set,'monthly_set':monthly_set,'lender_email':email})
                subject, from_email, to = 'Lead Limits Set Notification', 'info@locallenders.net', email
                text_content = plaintext.render(d)
                html_content = htmly.render(d)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                data = {'result':'success','details': "Leads Limit has been updated successfully"}
                return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                n=None
                limit=Lead_Limit.objects.create(lender_account_id=lender_id,hourly_set=hourly_set,daily_set=daily_set,
                weekly_set=weekly_set,monthly_set=monthly_set,added_on=added_on,updated_on=updated_on,
                lead_limit_status=400,limit_diff=n)
                limit.save()
                plaintext = get_template('registration/activation_email.txt')
                htmly     = get_template('email-templates/leadlimit_updated.htm')
                d = Context({'hourly_set':hourly_set,'daily_set':daily_set,'weekly_set':weekly_set,'monthly_set':monthly_set,'lender_email':email})
                subject, from_email, to = 'Lead Limits Set Notification', 'info@locallenders.net', email
                text_content = plaintext.render(d)
                html_content = htmly.render(d)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                data = {'result':'success','details': "Leads Limit has been set successfully"}
                return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')


'''Get Single Lender info for updation from staff account'''
class GetLenderData(View):
    def get(self, request):
        # <view logic>
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            print "inside lender data",data
            lender_id=data['lender_id']
            lenderdata=User.objects.filter(id=lender_id)
            for data in lenderdata:
                pass
            keys = ['username','email','phone_number','storeName','storeAddress','id','zip_code',
                    'ownerName','ownerPhone','ownerEmail','managerName','managerPhone','managerEmail','EinNumber',
                    'loanLimit']
            dictList = []
            lenderinfo = [data.username,data.email,data.phone_number,data.storeName,data.storeAddress,data.id,
            data.zip_code,data.ownerName,data.ownerPhone,data.ownerEmail,data.managerName,
            data.managerPhone,data.managerEmail,data.EinNumber,data.loanLimit]
            dictList.append(dict(zip(keys, lenderinfo)))
            return HttpResponse(json.dumps(dictList), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')   


'''code to use hello sign api'''
class GetSignUrl(View):
     def get(self, request):
        print "inside get sign url"
        client = HSClient(api_key='0babcb384682960ab3074f907b46633f9335a569b7da1406965f0801ef321cd2')
        signatureRequest= client.send_signature_request_embedded(
            test_mode=True,
            client_id='07cb906f6a297e6d9b1c6d2731fe76b0',
            subject="My First embedded signature request",
            message="Awesome, right?",
            signers=[{ 'email_address': 'mss.sachinsharma@gmail.com', 'name': 'Sachin Sharma' }],
            files=['/var/www/LocalLendersApp/locallenders/locallender/app/assets/My_First_embedded_signature_request.pdf']
        )
        print"here sachin",signatureRequest.signature_request_id
        client_id='07cb906f6a297e6d9b1c6d2731fe76b0'
        SIGNATURE_ID = signatureRequest.signature_request_id
        for signature in signatureRequest.signatures:
            embedded_obj = client.get_embedded_object(signature.signature_id)
            sign_url = embedded_obj.sign_url
            print "url",sign_url
        data = {'result': 'success','sign_url':sign_url,'client_id':client_id}
        return HttpResponse(json.dumps(data,cls = DateTimeEncoder), content_type='application/json')


'''Get Single Appointment for staff account'''
class GetSingleAppointment(View):
    def get(self, request):
        # <view logic>
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            appointment_id=data['appointment_id']
            print "data",appointment_id
            user_appointment = Appointments.objects.filter(id=appointment_id)
            print "user_appointment",user_appointment
            for u in user_appointment:
                status_code = u.status_code_id
            status = Statustypes.objects.filter(status_code = status_code)
            for stat in status:
                status_name = stat.name
            appointment_keys = ['appointment_id','scheduled_at','added_on','status_code_id']
            Appointment_data=[]
            for user in user_appointment:
                lender_id = user.lender_id
                customer_id = user.customer_id
                appointment_info=[user.id,user.scheduled_at,user.added_on,user.status_code_id]
                Appointment_data.append(dict(zip(appointment_keys, appointment_info)))
            lender_keys = ['id','Name','Email','Phone','Store','Address']
            Lender_data=[]
            lender=User.objects.filter(id = lender_id)
            for user in lender:
                lender_info = [user.id,user.username,user.email,user.phone_number,user.storeName,user.storeAddress]
                Lender_data.append(dict(zip(lender_keys, lender_info)))
            print "Lender_data",Lender_data
            customer_keys = ['id','Name','Email','Phone']
            Customer_data=[]
            customer=User.objects.filter(id = customer_id)
            for user in customer:
                customer_info = [user.id,user.username,user.email,user.phone_number]
                Customer_data.append(dict(zip(customer_keys, customer_info)))
            print "Customer_data",Customer_data
            data = {'result': 'success','Customer_data':Customer_data,'Lender_data':Lender_data,'Appointment_data':Appointment_data,'status':status_name}
            return HttpResponse(json.dumps(data,cls = DateTimeEncoder), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')   
            
       
'''Appointment approval by staff'''
class ApproveAppointment(View):
    def get(self, request):
        # <view logic>
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            lender_email = data['lender_email']
            customer_email = data['customer_email']
            emails = []
            emails.append(lender_email)
            emails.append(customer_email)
            appointment_id=data['appointment_id']
            location = data['loc']
            requested_on=data['requested_on']
            note=data['note']
            updated_on=datetime.datetime.now()
            if note:
                note_text = note
            else:
                n=None
                note_text = n
            Appointments.objects.filter(id=appointment_id).update(
            status_code_id = 200,updated_on = updated_on,scheduled_at=requested_on,location=location,note=note)
            for email in emails:           
                plaintext = get_template('registration/activation_email.txt')
                htmly     = get_template('email-templates/appointment-scheduled.htm')
                d = Context({'scheduled_at':requested_on,'location':location,'lender_email':lender_email,'customer_email':customer_email})
                subject, from_email, to = 'Appointment Schedule Confirmation', 'info@locallenders.net', email
                text_content = plaintext.render(d)
                html_content = htmly.render(d)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
            data = {'result': 'success','details' :'Successfully approved'}
            return HttpResponse(json.dumps(data,cls = DateTimeEncoder), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')   


def testscheduled():
    send_mail('Subject here', 'Here is the message.', 'from@example.com',
    ['mss.jeevanverma@gmail.com'], fail_silently=False)
 
'''helpdesk ticket push'''
class helpdeskticket(View):
    def get(self,request):
        try:
            ticket=Ticket.objects.filter(ticket_status='0')
            adminuser = User.objects.filter(is_staff=1)
            emailid=[]
            for admin in adminuser:
                emailid.append(admin.email)
            ccemail=emailid
            for tick in ticket:
                title=tick.title
                des=tick.description
                useremail=tick.submitter_email
                ticketsytem={'cc_email': ccemail, 'des':des , 'sub':title , 'email': useremail}
                statusapi=ticket_create(ticketsytem);
                Ticket.objects.filter(id=tick.id).update(ticket_status='1')
            data='sucess add tickets'
            return HttpResponse(data, content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')


'''Get appointmentslist for staff account'''
class GetAppointments(View):
    def get(self, request):
        try:
            appointments=Appointments.objects.all()
            print "appointments",appointments
            #Get appointments data
            keys = ['id','Lender_id','Customer_id','Added_on','Scheduled_on','description']
            Appointment_list=[]
            for appointment in appointments:
                lender_id = appointment.lender_id
                customer_id = appointment.customer_id
                appointments_info = [(appointment.id,lender_id,customer_id,appointment.added_on,appointment.scheduled_at,appointment.description)]
                Appointment_list.append(dict(zip(keys, appointments_info)))
            data = {'result': 'success','info' :Appointment_list}
            return HttpResponse(json.dumps(data,cls = DateTimeEncoder), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
            
        
'''Appointment request by a customer'''
class AppointmentRequest(View):
    def get(self, request):
        # <view logic>
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            customer_id = request.user.id
            email = data['lender_email']
            description = data['message']
            scheduled_at=data['date']
            added_on=datetime.datetime.now()
            updated_on=datetime.datetime.now()
            lender = User.objects.filter(email=email)
            for user in lender:
                lender_id = user.id
            user_exist= Appointments.objects.filter(lender_id=lender_id,customer_id=customer_id);
            if user_exist:
                data = {'result':'error','details': "You have already made a request for appointment"}
                return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                appointment=Appointments.objects.create(customer_id=customer_id,lender_id=lender_id,
                description=description,scheduled_at=scheduled_at,added_on=added_on,updated_on=updated_on,
                status_code_id=205)
                appointment.save()
                data = {'result':'success','details': "Appointment request has been made successfully"}
                return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        


'''Add agents to freshdesk portal'''
class AddAgents(View):
    def get(self, request):
        # <view logic>
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            send_mail('Subject here', 'Here is the message.', 'from@example.com',['mss.jeevanverma@gmail.com'], fail_silently=False)
            group = 'Customers'
            customers = User.objects.filter(group=group)
            for customer in customers:
                customer_email = str(customer.email)
                customer_name= str(customer.username)
                content={'name':customer_name,'email':customer_email}
                print "type",content
                fresh_data = create(content)
                print "fresh_data",fresh_data
            data = {'result':'success','details': " agents added successfully"}
            return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        
                
'''Add Lenders to Freshbook'''
class InsertInvoiceClients(View):
    def post(self, request):
        try:
            group = 'Lenders'
            lenders = User.objects.filter(group=group)
            fb=auth_freshbooks()
            c = api.TokenClient(FRESHBOOKS_URL,FRESHBOOKS_TOKEN)
            client_list = c.client.list()
            Clients_List=[]
            for client in client_list.clients.client:
                client_email=client.email
                Clients_List.append(client_email)
            print "Clients_List",Clients_List
            Lenders_List=[]
            keys = ['first_name','last_name','email','organization']
            for lender in lenders:
                if not lender.email in Clients_List:
                    fb_kwargs = {str('client'): remove_blank({'last_name': lender.last_name, 'first_name': lender.first_name, 'email':lender.email, 'organization': lender.storeName})}
                    func_type = getattr(fb, 'client')
                    func_type.create(**fb_kwargs)   
            print "Lenders_List",Lenders_List
            data = {'result': 'success','detail':'Successfully Saved'}
            return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
         
'''Lender's request for more lead'''
class LeadsRequest(View):
    def get(self, request):
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            print "data here",data
            current_user=request.user
            user_id=current_user.id
            email=current_user.email
            phoneNumber=current_user.phone_number
            limits=Lead_Limit.objects.filter(lender_account_id=user_id)
            for limit in limits:
                lead_limit_id=limit.id
            hourly_set=data['hourly_set']
            daily_set=data['daily_set']
            weekly_set=data['weekly_set']
            monthly_set=data['monthly_set']
            limit_diff=data['limit_diff']
            added_on=datetime.datetime.now()
            updated_on=datetime.datetime.now()
            request_exists = Lead_Request.objects.filter(lead_limit_id = lead_limit_id)
            print "request_exists",request_exists
            if request_exists:
                data = {'result': 'error','details':'You have already submitted a request'}
                return HttpResponse(json.dumps(data), content_type='application/json')
            members = User.objects.filter(is_staff=1)
            #Get admin emails
            adminuser = User.objects.filter(is_staff=1)
            emailid=[]
            for admin in adminuser:
                emailid.append(admin.email)
            ccemail=emailid
            #add data to lenderleadsrequest table
            request=Lead_Request.objects.create(limit_diff=limit_diff,lead_limit_id=lead_limit_id,hourly_set=hourly_set,daily_set=daily_set,
            weekly_set=weekly_set,monthly_set=monthly_set,added_on=added_on,updated_on=updated_on,
            status=400)
            request.save()
            #Send mails to admin users
            for staff_members in members:
                member_email=staff_members.email
                print "member_email",member_email
                plaintext = get_template('registration/activation_email.txt')
                htmly     = get_template('email-templates/leads-request.htm')
                d = Context({'count':hourly_set,'email':email})
                subject, from_email, to = 'hello', 'from@example.com', member_email
                text_content = plaintext.render(d)
                html_content = htmly.render(d)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
            #Generate ticket
            title="New Leads request"
            des1="Hi \n\n"
            des2= "lender having email "+ email+ " has requested for "
            des3=str(hourly_set) +" more leads\n"
            des4="\nEmail:"+email
            des5="\nPhone:"+phoneNumber
            des6="\n\nThanks"
            ticketsytem={'cc_email': ccemail, 'des':des1+des2+des3+des4+des5+des6, 'sub':title , 'email': email}
            statusapi=ticket_create(ticketsytem);
            data = {'result': 'success','details':'Your request has been submitted successfully'}
            return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')

'''Get Specific Leads for a Lender'''
class GetMyLeads(View):
    def get(self, request):
        try:
            dictList = []
            current_user = request.user
            userId=current_user.id
            limits=Lead_Limit.objects.filter(lender_account_id=userId)
            limits_list=[]
            limit_keys = ['hourly_set','daily_set','weekly_set','monthly_set']
            if limits:
                for limit in limits:
                    limit_diff=limit.limit_diff
                    print limit.hourly_set,limit.daily_set,limit.weekly_set,limit.monthly_set
                    Limitsinfo = [limit.hourly_set,limit.daily_set,limit.weekly_set,limit.monthly_set]
                    limits_list.append(dict(zip(limit_keys, Limitsinfo)))
                    
            ''' calculate lender's appointments '''
            user_appointments = Appointments.objects.filter(lender_id=userId)
            
            if user_appointments:
                print"user_appointments",user_appointments
                for user in  user_appointments:
                    print user.customer_id,user.scheduled_at
                    customer_id = user.customer_id
                    appointment_time = user.scheduled_at
                    
            ''' Calculate offered and asigned leads to lender '''
            leads = list(itertools.chain(Lead.objects.filter(offered_to = userId),Lead.objects.filter(offered_to__isnull=True)[:15]))
            
            ''' Calculate lender's cash with locallenders.net '''
            cashAmount = User.objects.filter(id=userId)
            if cashAmount:
                for cash in cashAmount:
                    amount = cash.account_balance
            else:
                amount = 0
            
            if leads:
                print "in leads loop"
                for lead in leads:
                    keys = ['first_name','last_name','street_addr1','street_addr2','city','state','home_phone',
                            'email','id','Zip','created_on','appointment_time']
                    #print "id test","lead_id",lead.id,"customer_id",customer_id
                    if user_appointments:
                        if (lead.offered_to == userId):
                            appointment_time = appointment_time
                        else:
                            appointment_time = 'No Scheduled Appointment'
                    else:
                        appointment_time = 'No Scheduled Appointment'
                    Leadinfo = [lead.first_name,lead.last_name,lead.street_addr1,lead.street_addr2,
                                    lead.city,lead.state,lead.home_phone,lead.email,lead.id,
                                    lead.Zip,lead.created_on,appointment_time]
                    dictList.append(dict(zip(keys, Leadinfo)))
                data = {'result': 'success','info' :dictList,'limits':limits_list,'limits_difference':limit_diff,'deposite':amount}
                return HttpResponse(json.dumps(data,cls=DateTimeEncoder), content_type='application/json')                
            else:
                data = {'result':'error','detail':'No Leads has been assigned','deposite':str(amount)}
                return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res, 'limits':'','limits_difference':''}
            return HttpResponse(json.dumps(data), content_type='application/json')
        
'''signle time payment'''
class Singletimepayment(View):
    def post(self,request):
        try:
            data = request.body
            print "data shintu",data
            word=data.split('&')
            token=word[0]
            Amount=word[1]
            token_split = token.split('=')
            stripe_token=token_split[1]
            Amount_split = Amount.split('=')
            stripe_Amount=Amount_split[1]
            stripe.api_key = settings.STRIPE_SECRET_KEY
    
            # Get the credit card details submitted by the form
            token = stripe_token
        
            # Create the charge on Stripe's servers - this will charge the user's card        
            current_user = request.user
            userId=current_user.id
            charge = stripe.Charge.create(
                amount=stripe_Amount, # amount in cents, again
                currency="usd",
                card=token,
                description=current_user.email
            )
            #print 'charge',charge  
            currentdate = datetime.datetime.now() 
            amount=charge['amount']  
            stripe_id=charge['id']
            paid_status=charge['paid']
            
            ''' Calculate total lender's account balance add update '''
            depposites = User.objects.filter(id=userId)
            print depposites
            if depposites:
                for depposite in depposites:
                    print depposite.account_balance
                    acount_balance = depposite.account_balance+amount
                    User.objects.filter(id=userId).update(account_balance=amount)
            
            lenders = lender_onetime_payment.objects.filter(lender=current_user)
            ''' Check if lender has already paid us'''
            if lenders:
                for lend in lenders:
                    amount = lend.amount+amount 
                    lender_onetime_payment.objects.filter(lender=current_user).update(lender=current_user,stripe_id=stripe_id,amount=amount,paid_status=paid_status,customer_card='4242',added_on=currentdate,data=str(charge))
                data = {'result': 'success'}
            else:
                lender_onetime_payment.objects.create(lender=current_user,stripe_id=stripe_id,amount=amount,paid_status=paid_status,customer_card='4242',added_on=currentdate,data=str(charge))
                data = {'result': 'success'}                    
            return HttpResponse(json.dumps('Your payment is successful, kindly check your leads page to see your cash with us.'), content_type='application/json')
        except stripe.CardError, e:
            # The card has been declined
            pass

''' get all leads belong to a lender logged in '''
class getLeadsByLenderId(View):
    def get(self, request):
        try:
            leads = chain(Lead.objects.filter(offered_to = request.user.id),Lead.objects.filter(offered_to__isnull=True)[:15])
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')        

'''Change Lead status'''
class ChangeLeadStatus(View):
    def get(self, request):
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            print "data",data
            lead_status = data['lead_status']
            lead_id = data['lead_id']
            status_code = StatusCodes.objects.filter(name = lead_status)
            for status in status_code:
               code = status.value
            currentdate = datetime.datetime.now()
            userExist = LeadStatus.objects.filter(lead_id = lead_id )
            if userExist:
                LeadStatus.objects.filter(lead_id=lead_id).update(
                status_id = code,updated_on = currentdate)
            else:
                leadstatus=LeadStatus.objects.create(lead_id = lead_id,status_id = code,added_on = currentdate,updated_on = currentdate)
                leadstatus.save()
            data = {'result': 'success','detail':'Status changed'}
            return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')


'''Lender status'''
class GetActiveLenderstatus(View):
    def get(self, request):
        try:
            current_user = request.user
            userId = current_user.id
            customer= current_user.group
            if(current_user.group=='Customers'):
                    user = Lead.objects.filter(user_id = userId)
                    adminuser = User.objects.filter(is_staff=1)
                    emailid=[]
                    for admin in adminuser:
                        emailid.append(admin.email)
                    ccemail=emailid
                    if user:
                        for u in user:
                            offered=u.offered_to
                            if offered:
                                pass
                            else:
                                current_user = request.user
                    else:
                        Msg='Outbound call nedded Email id'+current_user.email
                        ticketsytem={'cc_email': ccemail, 'des': msgtext, 'sub': 'hello free', 'email': current_user.email}
                        statusapi=ticket_create(ticketsytem);
                        #print statusapi
                        data = {'result':'success','details': res}
                        return HttpResponse(data, content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')

        
'''Get lender information for lender profile page '''
class GetLenderInfo(View):
    def get(self, request):
        try:
            current_user = request.user
            userId=current_user.id
            keys = ['username','email','phone_number','storeName','storeAddress','id','zip_code',
                    'ownerName','ownerPhone','ownerEmail','managerName','managerPhone','managerEmail','EinNumber',
                    'loanLimit']
            dictList = []
            Customerinfo = [current_user.username,current_user.email,current_user.phone_number,
            current_user.storeName,current_user.storeAddress,current_user.id,current_user.zip_code,
            current_user.ownerName,current_user.ownerPhone,current_user.ownerEmail,current_user.managerName,
            current_user.managerPhone,current_user.managerEmail,current_user.EinNumber,current_user.loanLimit]
            dictList.append(dict(zip(keys, Customerinfo)))
            return HttpResponse(json.dumps(dictList), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')

'''Save Lender  info from lender profile page'''
class SaveLenderInfo(View):
    def get(self, request):
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            print "data here",data
            current_user = request.user
            userId=current_user.id
            print "userId",userId
            User.objects.filter(id=userId).update(
            managerPhone=data['managerPhone'],loanLimit=data['loanLimit'],ownerEmail=data['ownerEmail'],
            EinNumber=data['EinNumber'],managerEmail=data['managerEmail'],ownerName=data['ownerName'],
            ownerPhone=data['ownerPhone'],managerName=data['managerName'],phone_number=data['phone_number'],
            storeAddress=data['storeAddress'])
            data = {'result': 'success','details':'Lender information saved successfully'}
            return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        
        

'''Lender's request to staff member'''
'''class LenderRequest(View):
    def get(self, request):
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
            data = json.loads(request.body)
            print "data in lenders request",data'''
            

'''Lead approval by staffmember'''
class ApproveLead(View):
    def get(self, request):
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            print "data",data
            customers = data['customers']
            flag = data['flag']
            length = len(customers)
            print 'length',length,data
            if flag=='staff':
                for i in range(0,length):
                    first_name = customers[i]['first_name']
                    last_name = customers[i]['last_name']
                    Email = customers[i]['Email']
                    phone_cell = customers[i]['Phone']
                    user_id = customers[i]['id']
                    source = flag
                    offered = '0'
                    lead = Lead.objects.filter(Email=Email)
                    exists = len(lead)
                    if exists == 0:
                        lead=Lead.objects.create(offered=offered,user_id=user_id,first_name=first_name,last_name=last_name,Email=Email,phone_cell=phone_cell,source=source)
                        lead.save()
                    else:
                        Lead.objects.filter(Email=Email).update(
                        offered=offered,
                        user_id=user_id,
                        first_name=first_name,
                        last_name=last_name,
                        Email=Email,
                        phone_cell=phone_cell,
                        source=source)
                    User.objects.filter(email=Email).update(
                        lead_conversion=1
                    )
                data = {'result': 'success'}
                return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                pass
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
            
            

'''Lead info saved by Lead'''
class SaveLeadInfo(View):
    def get(self, request):
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            print "data",data
            zipCode = data['Zip']
            current_user = request.user
            current_userid  = current_user.id
            #Get Long,Lat from lenders zipcode
            geolocator = GoogleV3()
            location = geolocator.geocode(zipCode)
            if location:
                longitude = location.longitude
                latitude = location.latitude
            else:
                data = {'result': 'error','detail':'Enter valid zip code'}
                return HttpResponse(json.dumps(data), content_type='application/json')
            fields=['email_alternate','middle_init','street_addr2','phone_cell','phone_work_ext']
            for field in fields:
                print "field",field
                if field not in data or data[field]==u'':
                    data[field] = None
            current_user = request.user
            currenttime=datetime.datetime.now()
            userId=current_user.id
            user_exists = Lead.objects.filter(user_id=userId)
            if user_exists:
                Lead.objects.filter(user_id=userId).update(
                first_name=data['first_name'],last_name=data['last_name'],
                street_addr1=data['street_addr1'],street_addr2=data['street_addr2'],Zip=data['Zip'],
                phone_home=data['phone_home'],updated_on = currenttime,Email=data['email'])
                #update user model
                User.objects.filter(id=userId).update(
                first_name=data['first_name'],last_name=data['last_name'],zip_code=data['Zip'],
                phone_number=data['phone_home'],email=data['email'],latitude=latitude,
                longitude=longitude)
                data = {'result': 'success','detail':'Successfully Updated'}
                return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                Lead.objects.create(
                first_name=data['first_name'],last_name=data['last_name'],
                street_addr1=data['street_addr1'],street_addr2=data['street_addr2'],Zip=data['Zip'],
                phone_home=data['phone_home'],updated_on = currenttime,Email=data['email'],user_id=current_user.id,created_on = currenttime
                )
                User.objects.filter(id=userId).update(
                first_name=data['first_name'],last_name=data['last_name'],zip_code=data['Zip'],
                phone_number=data['phone_home'],email=data['email'],latitude=latitude,
                longitude=longitude,lead_conversion=1)
                data = {'result': 'success','detail':'Successfully Saved'}
                return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
    
'''Lead advance information  saved by Lead'''
class AdvanceLeadInfo(View):
    def get(self, request):
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            print "WARNING! You are executing a method that has been edited by Bill. api.py AdvanceLeadInfo."
            ##TODO:v2.0:What is happening here?
            #fields=['email_alternate','middle_init','street_addr2','phone_cell','phone_work_ext']
            #for field in fields:
            #    print "field",field
            #    if field not in data or data[field]==u'':
            #        data[field] = None
            current_user = request.user
            current_time=datetime.datetime.now()
            userId=current_user.id
            lead = Lead.objects.filter(user_id = current_user.id)
            if lead:
                lead.update(updated_on = current_time, **data)
                #TODO:v2.0:If we ever use this, we need to check that the above works.
                #TODO:v2.0:The process of listing all variables is very bad.
                data = {'result': 'success','detail':'Successfully Updated'}
                return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                Lead.objects.create(updated_on = current_time,**data)
                User.objects.filter(id=userId).update(
                        lead_conversion=1
                    )
                data = {'result': 'success','detail':'Successfully Saved'}
                return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')            

'''lender selection by customer'''
class MyLocation(View):
    '''
    View to populate goodnews.html page with location specific data
    Can be used to get data for any single location
    '''
    def goodnews_info(token):
        goodnews_info = lead_lender_location_token.objects.get(token=token)
        return goodnews_info

    def get(self, request):
        # DONE: Create GoodNewsToken data model with location_id,lead_id,token
        # DONE: Update field list in 1089 to reflect locations model
        # DONE: Adjust goodnews.html template to reflect new data model
        # DONE: Add logo image to model
        # TODO: Add default logo image when we add a lender? Maybe we need this?
        # TODO: Capture the redirect token and serve the appropriate information
        try:
            #TODO: I don't know how to grab the token. My error messages aren't showing up.
            current_user = request.user
            ''' get authentication token and location id '''            
            token = urlparse.parse_qs(urlparse.urlparse(request.META.get('HTTP_REFERER', '')).query)['token'][0]
            goodnews_info = lead_lender_location_token.objects.get(token=token)
            
            ''' Create reference parameters '''
            lender, lead, location = goodnews_info.lender_id, goodnews_info.lead_id, goodnews_info.location_id
            
            if token and location and lender and lead:
                location = Location.objects.get(id = int(location))
                
            # If we have a logged in user, get the users most recent lead location
            elif current_user:
                location_id     = Lead.objects.filter(user_id=current_user.id).order_by('-id')[0]
                location        = Location.objects.get(id=int(location))
                
            # Else give location a falsy value    
            else:
                location = False

            # Prepare the location data
            if location:
                data = {}
                data['location_name']   = location.location_name
                data['name']            = location.name
                data['email']           = location.email
                data['phone']           = location.phone
                data['city']            = location.city
                data['state']           = location.state
                data['zipcode']         = location.zipcode
                data['latitude']        = location.latitude
                data['longitude']       = location.longitude
                data['address']         = location.address
            else:
                data = {'result': 'error','detail':'Dear customer, No Lender found in your are, we will keep you posted as soon as we get one.'}
            return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')

class MyLender(View):
    def get(self, request):
        try:
            current_user = request.user
            userId=current_user.id
            customer_longitude,customer_latitude=current_user.longitude,current_user.latitude
            customer_location = (current_user.longitude,current_user.latitude)
            print "customer_longitude",customer_longitude,customer_latitude
            print "customer_location",customer_location
            #call test_notification function of notify class, to send notifications.
            #n=notify()
            #n.test_notification(request)
            user = Lead.objects.filter(user_id = userId)
            if user:
                for i in user:
                    lender_id = i.offered_to
                    print "lender_id",lender_id
                    if lender_id:
                        lender = User.objects.filter(id = lender_id)
                        for user in lender:
                            keys = ['ownerName','first_name','last_name','Email','Phone','id','storeName','storeAddress','latitude','longitude']
                            dictList = []
                            Lenderinfo = [user.ownerName,user.first_name,user.last_name,user.email,user.phone_number,user.id,
                                user.storeName,user.storeAddress,user.latitude,user.longitude]
                            dictList.append(dict(zip(keys, Lenderinfo)))
                        return HttpResponse(json.dumps(dictList), content_type='application/json')
                    else:
                        '''new code'''
                        group='Lenders'
                        lenders = User.objects.filter(group=group,is_active=True)
                        keys = ['ownerName','distance','first_name','last_name','Email','Phone','id','storeName','storeAddress','latitude','longitude']
                        dictList = []
                        for lender in lenders:
                            lender_location =(lender.longitude,lender.latitude)
                            cust_loc = customer_location
                            lend_loc = lender_location
                            distance = great_circle(cust_loc, lend_loc).miles
                            if distance<5:
                                lendersinfo = [lender.ownerName,distance,lender.first_name,lender.last_name,
                                lender.email,lender.phone_number,lender.id,lender.storeName,lender.storeAddress,lender.latitude,lender.longitude]
                                dictList.append(dict(zip(keys, lendersinfo)))
                            else:
                                pass
                        '''ends here'''
                        print "length",len(dictList)
                        if len(dictList)>0:
                            return HttpResponse(json.dumps(dictList), content_type='application/json')
                        else:
                            data = {'result': 'error','detail':'No Lender found in five miles'}
                            return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                data = {'result': 'error','detail':'No Lender has been choosen Yet'}
                return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        
    
    def post(self, request):
            current_user = request.user
            customer_email = current_user.email
            userId=current_user.id
            is_lead=current_user.lead_conversion
            data = json.loads(request.body)
            lender_id = data['lenderid']
            lender = User.objects.filter(id=lender_id)
            for l in lender:
                lender_email=l.email
                lendername=l.username
            if is_lead:
                print "is_lead"
                Lead.objects.filter(user_id=userId).update(
                        offered_to=lender_id
                )
                #sending mail for account registration confirmation
                plaintext = get_template('registration/activation_email.txt')
                htmly = get_template('email-templates/lender-selected.htm')
                d = Context({ 'username': lendername,'email':lender_email,'customer_email':customer_email})
                subject, from_email, to = 'Lender Selection Confirmation', 'info@locallenders.net', lender_email
                text_content = plaintext.render(d)
                html_content = htmly.render(d)
                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
                data = {'result': 'success','detail':'Lender Choosen Successfully'}
                return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                print "no lead"
                data = {'result': 'error','detail':'Please fill your profile information on your account page to choose a lender'}
                return HttpResponse(json.dumps(data), content_type='application/json')

'''Remove Lender from customer choice'''
class RemoveLender(View):
    def get(self, request):
        # <view logic>
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            current_user=request.user
            user_id=current_user.id
            val = None
            Lead.objects.filter(user_id=user_id).update(
                offered=0,offered_to=val)
            data = {'result': 'success','details':'Lender removed successfully'}
            return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')

'''Get customers information for customer profile page '''
class GetCustomerInfo(View):
    def get(self, request):
        try:
            current_user = request.user
            userId=current_user.id
            userName=current_user.username
            email=current_user.email
            phoneNumber=current_user.phone_number
            first_name = current_user.first_name
            last_name = current_user.last_name
            zipCode=current_user.zip_code
            #print "data",userId,userName,phoneNumber,email
            keys = ['Name', 'Email','Phone','id','first_name','last_name','zipCode']
            dictList = []
            Customerinfo = [userName,email,phoneNumber,userId,first_name,last_name,zipCode]
            dictList.append(dict(zip(keys, Customerinfo)))
            return HttpResponse(json.dumps(dictList), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
    

'''check if customer already exists or not'''
class CustomerExistence(View):
    def get(self, request):
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            print "data",data
            email = data['userEmail']
            if email:
                user = User.objects.filter(email=email).count()
                print "user",user
                if user:
                    data = {'result': 'error','detail':'This email is already registered with us.'}
                    return HttpResponse(json.dumps(data), content_type='application/json')
                else:
                    data = {'result': 'success','detail':'No user found'}
                    return HttpResponse(json.dumps(data), content_type='application/json') 
            else:
                data = {'result': 'error','detail':'Enter a valid email'}
                return HttpResponse(json.dumps(data), content_type='application/json')
                
        except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')

'''SignIn function for lenders as well customers'''
class Login(View):
    def get(self, request):
        # <view logic>
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            username,password=data['username'],data['password']
            user = User.objects.get(username=username)
            #check if user is not none and is active
            if user is not None:
                if user.is_active:
                    #check for user provided password 
                    userpassword = user.password
                    if password == userpassword:
                        #if the user is a customer
                        if user.group == 'Customers':
                            data = {'result': 'success','detail':'Welcome,Customer...You are logged in'}
                            return HttpResponse(json.dumps(data), content_type='application/json')
                        #if user is a lender
                        elif user.group == 'Lenders':
                            data = {'result': 'success','detail':'Welcome,Lender...You are logged in'}
                            return HttpResponse(json.dumps(data), content_type='application/json')
                    else:
                        data = {'result': 'error','detail':'Credentials did not  matched'}
                        return HttpResponse(json.dumps(data), content_type='application/json')
                else:
                    data = {'result': 'error','detail':'Your account is not active yet'}
                    return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')


''' Lender SignUp Class '''
class LenderSignUp(View):
    def get(self, request):
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        ''' Exception handler starts here '''
        try:
            data = json.loads(request.body)
            #get all admin email
            adminuser = User.objects.filter(is_staff=1)
            emailid=[]
            for admin in adminuser:
                emailid.append(admin.email)
                
            ccemail = emailid
            zipCode = data['zip_code']
            
            #Get Long,Lat from lenders zipcode
            geolocator  = GoogleV3()
            location    = geolocator.geocode(zipCode)
            string      = data['email']
            index       = string.find('@')
            part1       = string[0:index]
            username    = part1
            
            if location:
                longitude, latitude, email, username=location.longitude, location.latitude, data['email'], part1
                user=User.objects.filter(email=email)
                
                ''' check if user already exist with applied email '''
                if user:
                    data = {'result': 'error','details':'Email already registered, kindly use different email address or login into your account.'}
                    return HttpResponse(json.dumps(data), content_type='application/json')
                else:
                    assert data['email']                    
                    user=User.objects.create(username=username,longitude=longitude,latitude=latitude,is_active=0,**data)
                    user.save()
                    print user
                    #Save Lender's information to database
                    if user is not None:
                        ''' Call email notification function '''
                        status, support = new_lender_sign_up(data, ccemail)
                        if status:                        
                            #Return response
                            data = {'result': 'success', 'support': support}
                            return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                data = {'result': 'error','details':"Invalid Zipcode, kindly check your zipcode and try again."}
                return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            data = {'result':'error','details': str(e)}
            return HttpResponse(json.dumps(data), content_type='application/json')        

'''Forgot Password class'''
class ForgotPassword(View):
    def get(self, request):
        # <view logic>
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            #get user email from request
            email = data['email']
            #check for user existence
            user = User.objects.get(email=email)
            if user is not None:
                password = user.password

                #send password to user's email address if exists

                send_mail('Password Reset Mail', 'Hello,This is a mail in response to your password forgotten request.Your Password is: %s' %password, 'LocalLendersStaff@example.com',
                [email], fail_silently=False)
                data = {'result': 'success',"detail":'Your password has been sent to your email address'}
                return HttpResponse(json.dumps(data), content_type='application/json')



            #if user is none
            else:
                data = {'result': 'error',"detail":'No User Found'}
                return HttpResponse(json.dumps(data), content_type='application/json')
        #if exception occurs
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')



'''Customer SignUp Class'''
class CustomerSignUp(View):
    def get(self, request):
        # <view logic>
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            print "data",data
            email,phoneNumber,zipCode,group = data['email'],data['phone_number'],data['zip_code'],data['group']
            zipCode = data['zip_code']
            #Get admin emails
            adminuser = User.objects.filter(is_staff=1)
            emailid=[]
            for admin in adminuser:
                emailid.append(admin.email)
            ccemail=emailid
            #Generate username from email
            string = data['email']
            index = string.find('@')
            part1 = string[0:index]
            part2 = string[index+1:len(string)]
            username = part1
            #Get Long,Lat from lenders zipcode
            geolocator = GoogleV3()
            location = geolocator.geocode(zipCode)
            if location:
                longitude = location.longitude
                latitude = location.latitude
                try:
                    #save customer information to database
                    user=User.objects.create(username=username,email=email,group=group, zip_code=zipCode, phone_number=phoneNumber,longitude=longitude,latitude=latitude)
                    user.save()
                    #Generate ticket
                    title="New customer added"
                    des1="Hi \n\n"
                    des2=" A new customer has registered on our app\n"
                    des3="\nEmail:"+email
                    des4="\nPhone:"+phoneNumber
                    des5="\n\nThanks"
                    ticketsytem={'cc_email': ccemail, 'des':des1+des2+des3+des4+des5, 'sub':title , 'email': email}
                    statusapi=ticket_create(ticketsytem);
                    #sending mail for account registration confirmation
                    plaintext = get_template('registration/activation_email.txt')
                    htmly     = get_template('email-templates/account-reactivated.htm')
                    d = Context({ 'username': username,'phoneNumber':phoneNumber,'email':email})
                    subject, from_email, to = 'account registration email', 'info@locallenders.net', email
                    text_content = plaintext.render(d)
                    html_content = htmly.render(d)
                    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                    msg.attach_alternative(html_content, "text/html")
                    msg.send()
                    data = {'result': 'success',"details":' Thanks! We will let you know as soon as we have a lender in your area.'}
                    return HttpResponse(json.dumps(data), content_type='application/json')
                except Exception,e:
                    res = str(e)
                    data = {'result':'error','details': "email already exists"}
                    return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                data = {'result': 'error','details':"Please enter a valid zip code"}
                return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')



'''Get All Lenders for staff dashboard'''
class GetLenders(View):
    def get(self, request):
        try:
            current_user = request.user
            customer_longitude,customer_latitude=current_user.longitude,current_user.latitude
            customer_location = (current_user.longitude,current_user.latitude)
            group = 'Lenders'
            lenders = User.objects.filter(group=group)
            keys = ['Name', 'Email', 'Store','Phone','Address','distance','id','is_active','latitude','longitude','leadstatuscode']
            dictList = []
            for lender in lenders:
                lender_location =(lender.longitude,lender.latitude)
                cust_loc = customer_location
                lend_loc = lender_location
                leadstatus=Leadsstatuscode.objects.filter(user_id=lender.id)
                if leadstatus:
                    for status in leadstatus:
                        leadstatus= status.status_code   
                else:
                    leadstatus=None
                distance = great_circle(cust_loc, lend_loc).miles
                lendersinfo = [lender.ownerName,lender.email,lender.storeName,lender.phone_number,lender.storeAddress,distance,lender.id,lender.is_active,lender.latitude,lender.longitude,leadstatus]
                dictList.append(dict(zip(keys, lendersinfo)))
            return HttpResponse(json.dumps(dictList), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        
'''Get all customers for staff dashboard '''
class GetCustomers(View):
    def get(self, request):
        try:
            current_user = request.user
            group = 'Customers'
            Customers = User.objects.filter(group=group,is_active=True)
            keys = ['Name', 'Email','Phone','id','days','seconds','lastlogindays','lastloginseconds','lastloginminutes',
            'lastloginhours','first_name','last_name','lead_conversion']
            dictList = []
            for customer in Customers:
                currentdate = timezone.now()
                #Get last login details
                lastlogin=customer.last_login
                logindifference=currentdate-lastlogin
                lastlogindays = logindifference.days
                lastloginseconds=int(logindifference.total_seconds())
                lastloginminutes=int(lastloginseconds/60)
                lastloginhours=int(lastloginseconds/3600)
                #Get joined date details
                datejoined=customer.date_joined
                difference = currentdate-datejoined
                daysdifference = difference.days
                secondsdifference=int(difference.total_seconds())
                customerinfo = [customer.first_name,customer.email,customer.phone_number,customer.id,daysdifference,secondsdifference,
                lastlogindays,lastloginseconds,lastloginminutes,lastloginhours,customer.first_name,customer.last_name,
                customer.lead_conversion]
                dictList.append(dict(zip(keys, customerinfo)))
            return HttpResponse(json.dumps(dictList), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        

'''Get Active Lenders for Customer dashboard'''
class GetActiveLenders(View):
    def get(self, request):
        try:
            current_user = request.user
            customer_longitude,customer_latitude=current_user.longitude,current_user.latitude
            customer_location = (current_user.longitude,current_user.latitude)
            group = 'Lenders'
            lenders = User.objects.filter(group=group,is_active=True)
            keys = ['Name', 'Email', 'Store','Phone','Address','distance','id','is_active','is_choosen']
            dictList = []
            data = Lead.objects.filter(user_id=request.user.id)
            if data:
                for d in data:
                    lender_id = d.offered_to
            else:
                lender_id = ''
            for lender in lenders:
                if lender_id == lender.id:
                    choosen = 1
                else:
                    choosen = 0
                lender_location =(lender.longitude,lender.latitude)
                cust_loc = customer_location
                lend_loc = lender_location
                distance = great_circle(cust_loc, lend_loc).miles
                lendersinfo = [lender.ownerName,lender.email,lender.storeName,lender.phone_number,lender.storeAddress,distance,lender.id,lender.is_active,choosen]
                dictList.append(dict(zip(keys, lendersinfo)))
            return HttpResponse(json.dumps(dictList), content_type='application/json')
            #else:
                #data = {'result': 'error','details':"Please enter you full details on your profile page"}
                #return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'result':'error','details': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
            
class StaffLogin(View):
    def get(self, request):
        # <view logic>
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            email, username, password = data['staffEmail'], data['staffUsername'], data['staffPassword']
            user = authenticate(username=username, password=password)
            if user is not None:
                # the password verified for the user
                if user.is_active and user.is_staff:
                    first_name = user.first_name
                    last_name = user.last_name
                    utctime = int(time.time())
                    data = '{0} {1}{2}{3}'.format(
                        first_name, last_name, email, utctime)
                    generated_hash = hmac.new(
                        settings.FRESHDESK_SECRET_KEY.encode(), data.encode(), hashlib.md5).hexdigest()
                    url = '{0}login/sso?name={1}&email={2}&timestamp={3}&hash={4}'.format(settings.FRESHDESK_URL,
                        urlquote('{0} {1}'.format(first_name, last_name)), urlquote(email), utctime, generated_hash)
                    res = HttpResponse(iri_to_uri(url))
                    return res
                else:
                    print("The password is valid, but the account has been disabled!")
            else:
                # the authentication system was unable to verify the username and password
                print("The username and password were incorrect.")
            #return StreamingHttpResponse('staff login result')
            data = {'error': 'user is none'}
            return HttpResponse(json.dumps(data), content_type='application/json')
        except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        

'''Approve a Lender'''        
class ApproveLender(View):
    def get(self, request):
        # <view logic>
        return StreamingHttpResponse('staff login result')
    
    def post(self, request):
        try:
            data = json.loads(request.body)
            print "data",data
            lender_id = data['lenderId']
            email = data['lenderEmail']
            lender = User.objects.filter(id=lender_id)
            if lender:
                for lenders in lender:
                    if lenders.is_active==False:
                        lenders.is_active = 1
                        lenders.password = hashlib.md5(email).hexdigest()
                        lenders.save()
                        username=lenders.username
                        #sending mail for account registration confirmation
                        plaintext = get_template('registration/activation_email.txt')
                        htmly     = get_template('email-templates/lenderaccount-activated.htm')
                        d = Context({'email':email,'username':username,'password':hashlib.md5(email).hexdigest(),'login_url':settings.SITE_URL})
                        subject, from_email, to = 'account activation email', 'info@LocalLenders.net', email
                        text_content = plaintext.render(d)
                        html_content = htmly.render(d)
                        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                        msg.attach_alternative(html_content, "text/html")
                        msg.send()
                        
                    else:
                        lenders.is_active = 0
                        lenders.save()
                data = {'result': 'success','Details':'Lender state changed successfully'}
                return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                data = {'error': 'no lender found'}
                return HttpResponse(json.dumps(data), content_type='application/json')
            
        except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
'''Lender Account Staus'''       
class LenderAccontstatus(View):
    def post(self, request):
        data = json.loads(request.body)
        lender_id = data['lenderId']
        lender_email = data['lenderEmail']
        statuscode=json.dumps(data['statuscode']).replace('"','')
        statusname=['Onboarding','Inprogress','Active','Paused','maxLeads','PastDue']
        registrationuser = Leadsstatuscode.objects.filter(user_id = lender_id)
        if registrationuser:
            for lender in registrationuser:
                liststatuscode=lender.status_code
                
            i=0
            for statue in liststatuscode:
                if statue==statuscode[i]:
                    if statue=='1':
                        if i==1:
                            leadstatus="Onboarding in progress"
                        elif i==4:
                            leadstatus="Active"
                        elif i==7:
                             leadstatus="Campaigns Paused"
                        elif i==10:
                             leadstatus="Max Leads Accepted"
                        elif i==13:
                              leadstatus="PastDue"
                        break
                   
                   #print statuscode[i]
                else:
                    if i==1:
                        leadstatus="Onboarding in progress"
                    elif i==4:
                        leadstatus="Active"
                    elif i==7:
                         leadstatus="Campaigns Paused"
                    elif i==10:
                         leadstatus="Max Leads Accepted"
                    elif i==13:
                          leadstatus="PastDue"
            
                i=i+1
                
            print leadstatus    
                
            if leadstatus!="Active":
                    message='Your account has been '+leadstatus
                    send_mail('Acount Status ', message, 'admin@gmail.com',[lender_email], fail_silently=False)
            else:
                lender = User.objects.filter(id=lender_id)
                if lender:
                    for lenders in lender:
                        if lenders.is_active==False:
                            lenders.is_active = 1
                            lenders.save()
                            #sending mail to lender for the approval of account
                            send_mail('Account Approval confirmation', 'Hello,This is an account approval email confirmation.Your account has been registered with LocalLenders.net', 'LocalLendersStaff@example.com',
                            [lender_email], fail_silently=False)
                            
                        else:
                            lenders.is_active = 0
                            lenders.save()
                
                
            Leadsstatuscode.objects.filter(user_id=lender_id).update(
                status_code=statuscode
                )
        else :
            if statuscode[1]=='1':
                    leadstatus="Onboarding in progress"
            elif statuscode[2]=='2':
                    leadstatus="Active"
            elif statuscode[3]=='3':
                     leadstatus="Campaigns Paused"
            elif statuscode[4]=='4':
                    leadstatus="Max Leads Accepted"
            elif statuscode[5]=='5':
                    leadstatus="PastDue"
            
                 
            if leadstatus!="Active":
                    print leadstatus
                    message='Your account has been '+leadstatus
                    send_mail('Acount Status ', message, 'admin@gmail.com',[lender_email], fail_silently=False)
            else:
                lender = User.objects.filter(id=lender_id)
                if lender:
                    for lenders in lender:
                        if lenders.is_active==False:
                            lenders.is_active = 1
                            lenders.save()
                            #sending mail to lender for the approval of account
                            send_mail('Account Approval confirmation', 'Hello,This is an account approval email confirmation.Your account has been registered with LocalLenders.net', 'LocalLendersStaff@example.com',
                            [lender_email], fail_silently=False)
                            
                        else:
                            lenders.is_active = 0
                            lenders.save()
                
                
            Leadsstatuscode.objects.create(
                user_id=lender_id,status_code=statuscode
                )
        return HttpResponse('Lead status '+leadstatus, content_type='application/json')
        pass
  
class SignOut(RedirectView):
    permanent = False
    query_string = True
 
    def get_redirect_url(self):
        print "here sachin"
        logout(self.request)
        return reverse('home')

class SignOut(RedirectView):
    permanent = False
    query_string = True
 
    def get_redirect_url(self):
        print "here sachin"
        logout(self.request)
        return reverse('home')


class UserList(generics.ListAPIView):
    model = User
    serializer_class = UserSerializer



class UserDetail(generics.RetrieveAPIView):
    model = User
    serializer_class = UserSerializer
    lookup_field = 'username'


class PostMixin(object):
    model = Post
    serializer_class = PostSerializer
    permission_classes = [
        PostAuthorCanEditPermission
    ]
    
    def pre_save(self, obj):
        """Force author to the current user on save"""
        obj.author = self.request.user
        return super(PostMixin, self).pre_save(obj)



class PostList(View):
    def get(self, request):
        # <view logic>
        return HttpResponse('result')
    
    def post(self, request):
        # <view logic>
        return HttpResponse('result')



class PostDetail(PostMixin, generics.RetrieveUpdateDestroyAPIView):
    pass


class UserPostList(generics.ListAPIView):
    model = Post
    serializer_class = PostSerializer
    
    def get_queryset(self):
        queryset = super(UserPostList, self).get_queryset()
        return queryset.filter(author__username=self.kwargs.get('username'))


class PhotoList(generics.ListCreateAPIView):
    model = Photo
    serializer_class = PhotoSerializer
    permission_classes = [
        permissions.AllowAny
    ]


class PhotoDetail(generics.RetrieveUpdateDestroyAPIView):
    model = Photo
    serializer_class = PhotoSerializer
    permission_classes = [
        permissions.AllowAny
    ]


class PostPhotoList(generics.ListAPIView):
    model = Photo
    serializer_class = PhotoSerializer
    
    def get_queryset(self):
        queryset = super(PostPhotoList, self).get_queryset()
        return queryset.filter(post__pk=self.kwargs.get('pk'))

class AssignLead(View):
    ''' View callable by application when a lead is assigned or reassigned
    The field is updated and a lender who previously had the lead is credited
    needs: user_id, lead_id, new_location_id
    '''
    def post(self, request):
        try:
            data = json.loads(request.body)
            print "inside accept Agreement",data
            current_user=request.user
            if not current_user.group == 'Staff':
                # TODO: How do I add a field to an object
                data = {'error':'Current User is not a staff member and cannot reassign leads.'}
                return HttpResponse(json.dumps(data), content_type='application/json')
            else:
                old_location_id = Lead.objects.filter(id=lead_id).assigned_to
                new_lead_price = Location.objects.filter(id=new_location_id).lead_price
                new_location_id = request.new_location_id
                
                if old_location_id:
                    Lender.filter(location_id == old_location_id),update(account_balance=F('account_balance') + old_sold_price) 
                
                Lender.filter(location_id == new_location_id),update(account_balance=F('account_balance') - new_sold_price) 
                Lead.filter(id == lead_id).update(assigned_to = new_location_id) 

            data = {'result':'success','details': "Assigned to location updated."}
            return HttpResponse(json.dumps(data), content_type='application/json')

        except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')

