import monitio
from .models import *
from django.contrib import messages
from django.http import HttpResponse, StreamingHttpResponse
from django.views.generic.base import View

'''Messages notifications class'''

class notify(View):
    #account activation notification
    def account_activate(self,request):
        current_user=request.user
        username = 'admin'
        user = User.objects.filter(username=username)
        for u in user:
            username=u.username
        monitio.add_message(request, messages.INFO, 'Your account has been activated', subject='Account activation')
        
    #Lender choose by customer
    def choose_lender(self,request,currentuser,lenderId):
        print "here inside choose lender"
        current_user=currentuser
        lenderId = lenderId
        user = User.objects.get(id=lenderId)
        monitio.add_message(request, messages.INFO, 'You have been choosen as a lender',user=user,subject='Lender Choice',expires=None,close_timeout=None)
        
    #Lender approval by staff
    def approve_lender(self,request):
        current_user=request.user
        username = 'admin'
        user = User.objects.filter(username=username)
        for u in user:
            username=u.username
        monitio.add_message(request, messages.INFO, 'Approved by staff', subject='Approval notification')
        
    #Lead conversion by staff
    def lead_convert(self,request):
        current_user=request.user
        username = 'admin'
        user = User.objects.filter(username=username)
        for u in user:
            username=u.username
        monitio.add_message(request, messages.INFO, 'You have been converted to lead', subject='Lead Conversion')  