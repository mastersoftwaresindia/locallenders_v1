import json
from django.contrib.auth.models import User
from django.core.mail import send_mail

from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from freshdesk_api.tickets import *
from django.conf import settings

''' New lender sign up @jv '''
def new_lender_sign_up(data, staff_emails):
    ''' Exception handler starts '''
    username_tail = data['email'].split('@')
    title   ="Locallenders.net | New Lender's Account Registered"
    desc    ="Dear Staff Team | New lender registration Notification. \n\n"
    desc    +="Added New Lender's Account with following information. \n\n"
    desc    +="Product at Store: "+str(data['product_at_store'])
    desc    +=". \nEmail:"+str(data['email'])
    desc    +=". \nPhone:"+str(data['phone_number'])
    
    ticketsytem={'cc_email':staff_emails, 'des':desc, 'sub':title , 'email':str(data['email'])}
    
    ''' Add freshdesk ticket in order to inform all staff members about new lender registration '''
    statusapi = ticket_create(json.dumps(ticketsytem))

    ''' sending mail for account registration confirmation '''
    plaintext   = get_template('registration/activation_email.txt')
    htmly       = get_template('email-templates/account-registered.htm')
    d           = Context({'username': username_tail[0],'email':data['email']})
    subject, from_email, to = 'Locallenders.net | New Lender Account Registered', 'info@locallenders.net', str(data['email'])
    text_content = plaintext.render(d)
    html_content = htmly.render(d)
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    return msg.send(), "<p>Thanks for the support. We will keep you posted on any updates. Our support team got this information, we will be with you in short while.</p> <a class='btn-u hm_link' href='/' id='home_link'>Return To Home</a>"

''' verification email to newly added customer '''
def send_verification_email_to_customer(new_user, good_news_link):
    ''' Sends verification email via djrill'''
    try:
        print new_user
        subject = 'Welcome to LocalLenders.net!\n\nPlease confirm your email address by clicking on the link below.\n\n'
        from_email = 'info@locallenders.net'
        to = new_user.email
        ''' sending mail for account registration confirmation '''
        plaintext   = get_template('registration/activation_email.txt')
        htmly       = get_template('email-templates/customer_account_verification_email.htm')
        d           = Context({'username':new_user.username,'email':new_user.email,'password':new_user.password, 'login_url':settings.SITE_URL+'/accounts/login', 'good_news':good_news_link})
        subject, from_email, to = 'Locallenders.net | New Lender Account Registered', 'info@locallenders.net', str(new_user.email)
        text_content = plaintext.render(d)
        html_content = htmly.render(d)
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        if msg:
            return email_sent, "Verification email sucessfully sent."
        else:
            return email_sent, "Error in sending verification email."
    except Exception, e:
        return False, str(e)