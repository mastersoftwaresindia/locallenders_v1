# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'User'
        db.create_table(u'api_user', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('is_superuser', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('username', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, blank=True)),
            ('is_staff', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('date_joined', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('phone_number', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True, blank=True)),
            ('zip_code', self.gf('django.db.models.fields.PositiveIntegerField')(default=None, null=True, blank=True)),
            ('group', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True, blank=True)),
            ('entityname', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True, blank=True)),
            ('state', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True, blank=True)),
            ('ownerName', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True, blank=True)),
            ('ownerEmail', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True, blank=True)),
            ('ownerPhone', self.gf('django.db.models.fields.PositiveIntegerField')(default=None, null=True, blank=True)),
            ('product_at_store', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True, blank=True)),
            ('storeName', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True, blank=True)),
            ('storeAddress', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True, blank=True)),
            ('EinNumber', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True, blank=True)),
            ('managerName', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True, blank=True)),
            ('managerEmail', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True, blank=True)),
            ('managerPhone', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True, blank=True)),
            ('longitude', self.gf('django.db.models.fields.FloatField')(default=None, null=True, blank=True)),
            ('latitude', self.gf('django.db.models.fields.FloatField')(default=None, null=True, blank=True)),
            ('loanLimit', self.gf('django.db.models.fields.FloatField')(default=None, null=True, blank=True)),
            ('city', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True, blank=True)),
            ('service', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True, blank=True)),
            ('lead_conversion', self.gf('django.db.models.fields.PositiveIntegerField')(default=None, null=True, blank=True)),
            ('agreement', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('min_balance_required_to_receive_leads', self.gf('django.db.models.fields.FloatField')(default=20)),
            ('account_balance', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=8, decimal_places=2)),
            ('test', self.gf('django.db.models.fields.CharField')(default=None, max_length=10, null=True, blank=True)),
        ))
        db.send_create_signal(u'api', ['User'])

        # Adding M2M table for field groups on 'User'
        m2m_table_name = db.shorten_name(u'api_user_groups')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('user', models.ForeignKey(orm[u'api.user'], null=False)),
            ('group', models.ForeignKey(orm[u'auth.group'], null=False))
        ))
        db.create_unique(m2m_table_name, ['user_id', 'group_id'])

        # Adding M2M table for field user_permissions on 'User'
        m2m_table_name = db.shorten_name(u'api_user_user_permissions')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('user', models.ForeignKey(orm[u'api.user'], null=False)),
            ('permission', models.ForeignKey(orm[u'auth.permission'], null=False))
        ))
        db.create_unique(m2m_table_name, ['user_id', 'permission_id'])

        # Adding M2M table for field followers on 'User'
        m2m_table_name = db.shorten_name(u'api_user_followers')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('from_user', models.ForeignKey(orm[u'api.user'], null=False)),
            ('to_user', models.ForeignKey(orm[u'api.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['from_user_id', 'to_user_id'])

        # Adding model 'UserProfile'
        db.create_table(u'api_userprofile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['api.User'])),
        ))
        db.send_create_signal(u'api', ['UserProfile'])

        # Adding model 'Post'
        db.create_table(u'api_post', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('author', self.gf('django.db.models.fields.related.ForeignKey')(related_name='posts', to=orm['api.User'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('body', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'api', ['Post'])

        # Adding model 'Photo'
        db.create_table(u'api_photo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('post', self.gf('django.db.models.fields.related.ForeignKey')(related_name='photos', to=orm['api.Post'])),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
        ))
        db.send_create_signal(u'api', ['Photo'])

        # Adding model 'Lead'
        db.create_table(u'api_lead', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(default=None, max_length=50, null=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(default=None, max_length=50, null=True)),
            ('middle_name', self.gf('django.db.models.fields.CharField')(default=None, max_length=1, null=True, blank=True)),
            ('street_addr1', self.gf('django.db.models.fields.CharField')(default=None, max_length=50, null=True)),
            ('street_addr2', self.gf('django.db.models.fields.CharField')(default=None, max_length=50, null=True, blank=True)),
            ('city', self.gf('django.db.models.fields.CharField')(default=None, max_length=100, null=True)),
            ('state', self.gf('django.db.models.fields.CharField')(default=None, max_length=2, null=True)),
            ('Zip', self.gf('django.db.models.fields.PositiveIntegerField')(default=None, null=True)),
            ('ssn', self.gf('django.db.models.fields.PositiveIntegerField')(default=None, null=True)),
            ('home_phone', self.gf('django.db.models.fields.CharField')(default=None, max_length=35, null=True)),
            ('cell_phone', self.gf('django.db.models.fields.CharField')(default=None, max_length=35, null=True, blank=True)),
            ('work_phone', self.gf('django.db.models.fields.CharField')(default=None, max_length=35, null=True)),
            ('work_phone_ext', self.gf('django.db.models.fields.CharField')(default=None, max_length=35, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=50)),
            ('dob', self.gf('django.db.models.fields.CharField')(default=None, max_length=35, null=True)),
            ('pay_frequency', self.gf('django.db.models.fields.CharField')(default=None, max_length=12, null=True)),
            ('pay_day1', self.gf('django.db.models.fields.CharField')(default=None, max_length=20, null=True)),
            ('pay_day2', self.gf('django.db.models.fields.CharField')(default=None, max_length=20, null=True)),
            ('bank_aba', self.gf('django.db.models.fields.PositiveIntegerField')(default=None, null=True)),
            ('bank_account', self.gf('django.db.models.fields.PositiveIntegerField')(default=None, null=True)),
            ('bank_name', self.gf('django.db.models.fields.CharField')(default=None, max_length=30, null=True)),
            ('monthly_income', self.gf('django.db.models.fields.CharField')(default=None, max_length=30, null=True)),
            ('own_home', self.gf('django.db.models.fields.PositiveIntegerField')(default=None, null=True)),
            ('drivers_license', self.gf('django.db.models.fields.CharField')(default=None, max_length=25, null=True)),
            ('drivers_license_state', self.gf('django.db.models.fields.CharField')(default=None, max_length=2, null=True)),
            ('client_url_root', self.gf('django.db.models.fields.URLField')(default=None, max_length=100, null=True)),
            ('client_ip_address', self.gf('django.db.models.fields.IPAddressField')(default=None, max_length=15, null=True)),
            ('email_alternate', self.gf('django.db.models.fields.EmailField')(default=None, max_length=100, null=True, blank=True)),
            ('income_type', self.gf('django.db.models.fields.CharField')(default=None, max_length=30, null=True)),
            ('is_military', self.gf('django.db.models.fields.PositiveIntegerField')(default=None, null=True)),
            ('bank_account_type', self.gf('django.db.models.fields.CharField')(default=None, max_length=10, null=True)),
            ('amount_requested', self.gf('django.db.models.fields.PositiveIntegerField')(default=None, null=True)),
            ('months_at_address', self.gf('django.db.models.fields.PositiveIntegerField')(default=None, null=True)),
            ('months_at_bank', self.gf('django.db.models.fields.PositiveIntegerField')(default=None, null=True)),
            ('payroll_type', self.gf('django.db.models.fields.CharField')(default=None, max_length=10, null=True)),
            ('language', self.gf('django.db.models.fields.CharField')(default=None, max_length=10, null=True)),
            ('mail_address', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True)),
            ('mail_city', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True)),
            ('mail_state', self.gf('django.db.models.fields.CharField')(default=None, max_length=10, null=True)),
            ('mail_zip', self.gf('django.db.models.fields.CharField')(default=None, max_length=10, null=True)),
            ('best_time_to_call', self.gf('django.db.models.fields.CharField')(default=None, max_length=10, null=True)),
            ('fax', self.gf('django.db.models.fields.CharField')(default=None, max_length=20, null=True)),
            ('occupation', self.gf('django.db.models.fields.CharField')(default=None, max_length=50, null=True)),
            ('Shift', self.gf('django.db.models.fields.CharField')(default=None, max_length=10, null=True)),
            ('months_at_employer', self.gf('django.db.models.fields.CharField')(default=None, max_length=10, null=True)),
            ('employer', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True)),
            ('emp_address', self.gf('django.db.models.fields.CharField')(default=None, max_length=50, null=True)),
            ('emp_city', self.gf('django.db.models.fields.CharField')(default=None, max_length=50, null=True)),
            ('emp_state', self.gf('django.db.models.fields.CharField')(default=None, max_length=10, null=True)),
            ('emp_zip', self.gf('django.db.models.fields.CharField')(default=None, max_length=10, null=True)),
            ('emp_Supervisor', self.gf('django.db.models.fields.CharField')(default=None, max_length=10, null=True)),
            ('user_id', self.gf('django.db.models.fields.PositiveIntegerField')(default=None, null=True, blank=True)),
            ('offered_to', self.gf('django.db.models.fields.PositiveIntegerField')(default=None, null=True, blank=True)),
            ('offered', self.gf('django.db.models.fields.CharField')(default=None, max_length=1, null=True, blank=True)),
            ('created_on', self.gf('django.db.models.fields.DateTimeField')(default=None, null=True)),
            ('updated_on', self.gf('django.db.models.fields.DateTimeField')(default=None, null=True, blank=True)),
            ('ticket_id', self.gf('django.db.models.fields.PositiveIntegerField')(default=None, null=True)),
            ('cost', self.gf('django.db.models.fields.DecimalField')(default=0.0, max_digits=7, decimal_places=2)),
            ('is_accepted', self.gf('django.db.models.fields.NullBooleanField')(default=None, null=True, blank=True)),
            ('decision_reason', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True)),
            ('ref_id', self.gf('django.db.models.fields.CharField')(default=None, max_length=25, null=True, blank=True)),
            ('comments', self.gf('django.db.models.fields.CharField')(default=None, max_length=255, null=True, blank=True)),
            ('lead_provider', self.gf('django.db.models.fields.CharField')(default=None, max_length=50, null=True, blank=True)),
            ('campaign_id', self.gf('django.db.models.fields.CharField')(default=None, max_length=50, null=True, blank=True)),
            ('campaign_name', self.gf('django.db.models.fields.CharField')(default=None, max_length=50, null=True, blank=True)),
            ('source_id', self.gf('django.db.models.fields.CharField')(default=None, max_length=50, null=True, blank=True)),
        ))
        db.send_create_signal(u'api', ['Lead'])

        # Adding model 'StatusCodes'
        db.create_table(u'api_statuscodes', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100, db_column='name')),
            ('value', self.gf('django.db.models.fields.PositiveIntegerField')(db_column='value')),
        ))
        db.send_create_signal(u'api', ['StatusCodes'])

        # Adding model 'LeadStatus'
        db.create_table(u'api_leadstatus', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('lead_id', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, db_column='lead_id')),
            ('status_id', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, db_column='status_id')),
            ('added_on', self.gf('django.db.models.fields.DateField')(null=True)),
            ('updated_on', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'api', ['LeadStatus'])

        # Adding model 'Leadsstatuscode'
        db.create_table(u'api_leadsstatuscode', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user_id', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, db_column='user_id')),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True)),
            ('status_code', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, db_column='status_code')),
        ))
        db.send_create_signal(u'api', ['Leadsstatuscode'])

        # Adding model 'Appointments'
        db.create_table(u'api_appointments', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('customer_id', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='customer_id')),
            ('lender_id', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='lender_id')),
            ('scheduled_by', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, db_column='scheduled_by')),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=1000, null=True, db_column='description')),
            ('scheduled_at', self.gf('django.db.models.fields.DateField')(null=True)),
            ('added_on', self.gf('django.db.models.fields.DateField')(null=True)),
            ('updated_on', self.gf('django.db.models.fields.DateField')(null=True)),
            ('status_code_id', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='status_code_id')),
            ('location', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, db_column='location')),
            ('note', self.gf('django.db.models.fields.CharField')(max_length=1000, null=True, db_column='note')),
        ))
        db.send_create_signal(u'api', ['Appointments'])

        # Adding model 'Statustypes'
        db.create_table(u'api_statustypes', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('relation_to', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, db_column='relation_to')),
            ('status_code', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='status_code')),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50, null=True)),
        ))
        db.send_create_signal(u'api', ['Statustypes'])

        # Adding model 'LeadDecisioning'
        db.create_table(u'api_leaddecisioning', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ip_address', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, db_column='ip_address')),
            ('social_security', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, db_column='social_security_num')),
            ('Decision_status', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, db_column='Decision_status')),
            ('decisioning_data', self.gf('django.db.models.fields.TextField')(max_length=200, null=True, db_column='decisioning_data')),
            ('added_on', self.gf('django.db.models.fields.DateField')(null=True)),
        ))
        db.send_create_signal(u'api', ['LeadDecisioning'])

        # Adding model 'Lenderleadsrequest'
        db.create_table(u'api_lenderleadsrequest', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('lender_id', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='lender_id')),
            ('leads_requested', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='leads_requested')),
            ('request_status_code', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, db_column='request_status_code')),
            ('added_on', self.gf('django.db.models.fields.DateField')(null=True)),
            ('updated_on', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'api', ['Lenderleadsrequest'])

        # Adding model 'Lead_Request'
        db.create_table(u'api_lead_request', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('daily_set', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='daily_set')),
            ('hourly_set', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='hourly_set')),
            ('weekly_set', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='weekly_set')),
            ('monthly_set', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='monthly_set')),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, db_column='status')),
            ('lead_limit_id', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='lead_limit_id')),
            ('added_on', self.gf('django.db.models.fields.DateField')(null=True)),
            ('updated_on', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('limit_diff', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='limit_diff')),
        ))
        db.send_create_signal(u'api', ['Lead_Request'])

        # Adding model 'Lead_Limit'
        db.create_table(u'api_lead_limit', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('lender_account_id', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='lender_account_id')),
            ('daily_set', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='daily_set')),
            ('hourly_set', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='hourly_set')),
            ('weekly_set', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='weekly_set')),
            ('monthly_set', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='monthly_set')),
            ('lead_limit_status', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, db_column='lead_limit_status')),
            ('added_on', self.gf('django.db.models.fields.DateField')(null=True)),
            ('updated_on', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('limit_diff', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='limit_diff')),
        ))
        db.send_create_signal(u'api', ['Lead_Limit'])

        # Adding model 'Lead_Assignment'
        db.create_table(u'api_lead_assignment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('staff_id', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='staff_id')),
            ('lender_id', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='lender_id')),
            ('lead_id', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='lead_id')),
            ('stripe_customer_account_id', self.gf('django.db.models.fields.IntegerField')(null=True, db_column='stripe_customer_account_id')),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, db_column='status')),
            ('added_on', self.gf('django.db.models.fields.DateField')(null=True)),
            ('accepting', self.gf('django.db.models.fields.BooleanField')()),
        ))
        db.send_create_signal(u'api', ['Lead_Assignment'])

        # Adding model 'lender_onetime_payment'
        db.create_table(u'api_lender_onetime_payment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('lender', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='lender_id', null=True, to=orm['api.User'])),
            ('stripe_id', self.gf('django.db.models.fields.TextField')(max_length=200, null=True, db_column='stripe_id', blank=True)),
            ('amount', self.gf('django.db.models.fields.DecimalField')(db_column='amount', decimal_places=2, max_digits=7)),
            ('paid_status', self.gf('django.db.models.fields.BooleanField')()),
            ('customer_card', self.gf('django.db.models.fields.TextField')(max_length=200, null=True, db_column='customer_card', blank=True)),
            ('added_on', self.gf('django.db.models.fields.DateField')(null=True)),
            ('data', self.gf('django.db.models.fields.TextField')(max_length=200, null=True, db_column='data', blank=True)),
        ))
        db.send_create_signal(u'api', ['lender_onetime_payment'])


    def backwards(self, orm):
        # Deleting model 'User'
        db.delete_table(u'api_user')

        # Removing M2M table for field groups on 'User'
        db.delete_table(db.shorten_name(u'api_user_groups'))

        # Removing M2M table for field user_permissions on 'User'
        db.delete_table(db.shorten_name(u'api_user_user_permissions'))

        # Removing M2M table for field followers on 'User'
        db.delete_table(db.shorten_name(u'api_user_followers'))

        # Deleting model 'UserProfile'
        db.delete_table(u'api_userprofile')

        # Deleting model 'Post'
        db.delete_table(u'api_post')

        # Deleting model 'Photo'
        db.delete_table(u'api_photo')

        # Deleting model 'Lead'
        db.delete_table(u'api_lead')

        # Deleting model 'StatusCodes'
        db.delete_table(u'api_statuscodes')

        # Deleting model 'LeadStatus'
        db.delete_table(u'api_leadstatus')

        # Deleting model 'Leadsstatuscode'
        db.delete_table(u'api_leadsstatuscode')

        # Deleting model 'Appointments'
        db.delete_table(u'api_appointments')

        # Deleting model 'Statustypes'
        db.delete_table(u'api_statustypes')

        # Deleting model 'LeadDecisioning'
        db.delete_table(u'api_leaddecisioning')

        # Deleting model 'Lenderleadsrequest'
        db.delete_table(u'api_lenderleadsrequest')

        # Deleting model 'Lead_Request'
        db.delete_table(u'api_lead_request')

        # Deleting model 'Lead_Limit'
        db.delete_table(u'api_lead_limit')

        # Deleting model 'Lead_Assignment'
        db.delete_table(u'api_lead_assignment')

        # Deleting model 'lender_onetime_payment'
        db.delete_table(u'api_lender_onetime_payment')


    models = {
        u'api.appointments': {
            'Meta': {'object_name': 'Appointments'},
            'added_on': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'customer_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'customer_id'"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'db_column': "'description'"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lender_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'lender_id'"}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'db_column': "'location'"}),
            'note': ('django.db.models.fields.CharField', [], {'max_length': '1000', 'null': 'True', 'db_column': "'note'"}),
            'scheduled_at': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'scheduled_by': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'scheduled_by'"}),
            'status_code_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'status_code_id'"}),
            'updated_on': ('django.db.models.fields.DateField', [], {'null': 'True'})
        },
        u'api.lead': {
            'Meta': {'object_name': 'Lead'},
            'Shift': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '10', 'null': 'True'}),
            'Zip': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True'}),
            'amount_requested': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True'}),
            'bank_aba': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True'}),
            'bank_account': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True'}),
            'bank_account_type': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '10', 'null': 'True'}),
            'bank_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '30', 'null': 'True'}),
            'best_time_to_call': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '10', 'null': 'True'}),
            'campaign_id': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'campaign_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'cell_phone': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '35', 'null': 'True', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True'}),
            'client_ip_address': ('django.db.models.fields.IPAddressField', [], {'default': 'None', 'max_length': '15', 'null': 'True'}),
            'client_url_root': ('django.db.models.fields.URLField', [], {'default': 'None', 'max_length': '100', 'null': 'True'}),
            'comments': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'cost': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '7', 'decimal_places': '2'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True'}),
            'decision_reason': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True'}),
            'dob': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '35', 'null': 'True'}),
            'drivers_license': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '25', 'null': 'True'}),
            'drivers_license_state': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '2', 'null': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '50'}),
            'email_alternate': ('django.db.models.fields.EmailField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'emp_Supervisor': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '10', 'null': 'True'}),
            'emp_address': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '50', 'null': 'True'}),
            'emp_city': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '50', 'null': 'True'}),
            'emp_state': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '10', 'null': 'True'}),
            'emp_zip': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '10', 'null': 'True'}),
            'employer': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True'}),
            'fax': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '20', 'null': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '50', 'null': 'True'}),
            'home_phone': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '35', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'income_type': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '30', 'null': 'True'}),
            'is_accepted': ('django.db.models.fields.NullBooleanField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'is_military': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '10', 'null': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '50', 'null': 'True'}),
            'lead_provider': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'mail_address': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True'}),
            'mail_city': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True'}),
            'mail_state': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '10', 'null': 'True'}),
            'mail_zip': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '10', 'null': 'True'}),
            'middle_name': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'monthly_income': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '30', 'null': 'True'}),
            'months_at_address': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True'}),
            'months_at_bank': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True'}),
            'months_at_employer': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '10', 'null': 'True'}),
            'occupation': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '50', 'null': 'True'}),
            'offered': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'offered_to': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'own_home': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True'}),
            'pay_day1': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '20', 'null': 'True'}),
            'pay_day2': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '20', 'null': 'True'}),
            'pay_frequency': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '12', 'null': 'True'}),
            'payroll_type': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '10', 'null': 'True'}),
            'ref_id': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '25', 'null': 'True', 'blank': 'True'}),
            'source_id': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'ssn': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '2', 'null': 'True'}),
            'street_addr1': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '50', 'null': 'True'}),
            'street_addr2': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'ticket_id': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True'}),
            'updated_on': ('django.db.models.fields.DateTimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'user_id': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'work_phone': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '35', 'null': 'True'}),
            'work_phone_ext': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '35', 'null': 'True', 'blank': 'True'})
        },
        u'api.lead_assignment': {
            'Meta': {'object_name': 'Lead_Assignment'},
            'accepting': ('django.db.models.fields.BooleanField', [], {}),
            'added_on': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lead_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'lead_id'"}),
            'lender_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'lender_id'"}),
            'staff_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'staff_id'"}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'status'"}),
            'stripe_customer_account_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'stripe_customer_account_id'"})
        },
        u'api.lead_limit': {
            'Meta': {'object_name': 'Lead_Limit'},
            'added_on': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'daily_set': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'daily_set'"}),
            'hourly_set': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'hourly_set'"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lead_limit_status': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'lead_limit_status'"}),
            'lender_account_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'lender_account_id'"}),
            'limit_diff': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'limit_diff'"}),
            'monthly_set': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'monthly_set'"}),
            'updated_on': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'weekly_set': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'weekly_set'"})
        },
        u'api.lead_request': {
            'Meta': {'object_name': 'Lead_Request'},
            'added_on': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'daily_set': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'daily_set'"}),
            'hourly_set': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'hourly_set'"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lead_limit_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'lead_limit_id'"}),
            'limit_diff': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'limit_diff'"}),
            'monthly_set': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'monthly_set'"}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'status'"}),
            'updated_on': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'weekly_set': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'weekly_set'"})
        },
        u'api.leaddecisioning': {
            'Decision_status': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'Decision_status'"}),
            'Meta': {'object_name': 'LeadDecisioning'},
            'added_on': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'decisioning_data': ('django.db.models.fields.TextField', [], {'max_length': '200', 'null': 'True', 'db_column': "'decisioning_data'"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip_address': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'db_column': "'ip_address'"}),
            'social_security': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'social_security_num'"})
        },
        u'api.leadsstatuscode': {
            'Meta': {'object_name': 'Leadsstatuscode'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'status_code': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'db_column': "'status_code'"}),
            'user_id': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'db_column': "'user_id'"})
        },
        u'api.leadstatus': {
            'Meta': {'object_name': 'LeadStatus'},
            'added_on': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lead_id': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'db_column': "'lead_id'"}),
            'status_id': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'db_column': "'status_id'"}),
            'updated_on': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        u'api.lender_onetime_payment': {
            'Meta': {'object_name': 'lender_onetime_payment'},
            'added_on': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'amount': ('django.db.models.fields.DecimalField', [], {'db_column': "'amount'", 'decimal_places': '2', 'max_digits': '7'}),
            'customer_card': ('django.db.models.fields.TextField', [], {'max_length': '200', 'null': 'True', 'db_column': "'customer_card'", 'blank': 'True'}),
            'data': ('django.db.models.fields.TextField', [], {'max_length': '200', 'null': 'True', 'db_column': "'data'", 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lender': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'lender_id'", 'null': 'True', 'to': u"orm['api.User']"}),
            'paid_status': ('django.db.models.fields.BooleanField', [], {}),
            'stripe_id': ('django.db.models.fields.TextField', [], {'max_length': '200', 'null': 'True', 'db_column': "'stripe_id'", 'blank': 'True'})
        },
        u'api.lenderleadsrequest': {
            'Meta': {'object_name': 'Lenderleadsrequest'},
            'added_on': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'leads_requested': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'leads_requested'"}),
            'lender_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'lender_id'"}),
            'request_status_code': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'request_status_code'"}),
            'updated_on': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'})
        },
        u'api.photo': {
            'Meta': {'object_name': 'Photo'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'post': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'photos'", 'to': u"orm['api.Post']"})
        },
        u'api.post': {
            'Meta': {'object_name': 'Post'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'posts'", 'to': u"orm['api.User']"}),
            'body': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'api.statuscodes': {
            'Meta': {'object_name': 'StatusCodes'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'db_column': "'name'"}),
            'value': ('django.db.models.fields.PositiveIntegerField', [], {'db_column': "'value'"})
        },
        u'api.statustypes': {
            'Meta': {'object_name': 'Statustypes'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'relation_to': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'db_column': "'relation_to'"}),
            'status_code': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'db_column': "'status_code'"})
        },
        u'api.user': {
            'EinNumber': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'User'},
            'account_balance': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '8', 'decimal_places': '2'}),
            'agreement': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'city': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'entityname': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'followers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'followees'", 'symmetrical': 'False', 'to': u"orm['api.User']"}),
            'group': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'latitude': ('django.db.models.fields.FloatField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'lead_conversion': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'loanLimit': ('django.db.models.fields.FloatField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.FloatField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'managerEmail': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'managerName': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'managerPhone': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'min_balance_required_to_receive_leads': ('django.db.models.fields.FloatField', [], {'default': '20'}),
            'ownerEmail': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'ownerName': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'ownerPhone': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'product_at_store': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'service': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'storeAddress': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'storeName': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'test': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'}),
            'zip_code': ('django.db.models.fields.PositiveIntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'})
        },
        u'api.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['api.User']"})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['api']