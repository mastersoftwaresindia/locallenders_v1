from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView


from app.views import *
from django.contrib.auth.views import login,logout
from django.contrib.auth.decorators import user_passes_test

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
login_forbidden =  user_passes_test(lambda u: u.is_anonymous(), '/dashboard')

# Enable the optional djrill admin interface
from djrill import DjrillAdminSite
admin.site = DjrillAdminSite()
admin.autodiscover()

class SimpleStaticView(TemplateView):
    def get_template_names(self):
        return [self.kwargs.get('template_name') + ".html"]
    
    def get(self, request, *args, **kwargs):

        from django.contrib.auth import authenticate, login

        #from django.contrib.auth import authenticate, login

        #if request.user.is_anonymous():
            # Auto-login the User for Demonstration Purposes
            #user = authenticate()
            #login(request, user)
        return super(SimpleStaticView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return super(SimpleStaticView, self).get(request, *args, **kwargs)




urlpatterns = patterns('',
    url(r'^api/', include('app.api.urls')),
    url(r'^dashboard/api/', include('app.api.urls')),
    url(r'^customer-dashboard/api/', include('app.api.urls')),
    url(r'^goodnews/api/', include('app.api.urls')),
    url(r'^lender-dashboard/api/', include('app.api.urls')),
    url(r'^lender-dashboard/document/api/', include('app.api.urls')),
    url(r'^customerdashboard/myaccount/api/', include('app.api.urls')),
    url(r'^customerdashboard/myaccount/completeprofile/api/', include('app.api.urls')),
    url(r'^customerdashboard/mylender/api/', include('app.api.urls')),
    url(r'^lender-dashboard/table/api/', include('app.api.urls')),
    url(r'^lender-dashboard/leads/api/', include('app.api.urls')),
    url(r'^staffdashboard/api/', include('app.api.urls')),
    url(r'^staffdashboard/appointment/id/(\d{1,4})/api/',include('app.api.urls')),
    url(r'^staffdashboard/appointment/id/(\d{1,4})/api/',include('app.api.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^(?P<template_name>\w+)$', SimpleStaticView.as_view(), name='app'),
    url(r'^$', TemplateView.as_view(template_name='base.html'), name="homepage"),
    url(r'^lender/register/$', login_forbidden(TemplateView.as_view(template_name='lenders/SignUp.html'))),
    url(r'^accounts/password/reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$','django.contrib.auth.views.password_reset_confirm',name='password_reset_confirm'),
    url(r'^customerdashboard/inbox/$', TemplateView.as_view(template_name='customers/inbox.html'), name="messages"),
    
    url(r'^lenderSignUp/', TemplateView.as_view(template_name='lenderSignUp.html')),
    url(r'^SignIn/', TemplateView.as_view(template_name='SignIn.html')),
    url(r'^accounts/login/$', login_forbidden(login), name="user_login"),

    url(r'^forgotPassword/', TemplateView.as_view(template_name='forgotpassword.html')),
    url(r'^customerdashboard/myaccount/completeprofile/$', TemplateView.as_view(template_name='customers/advanceprofile.html')),
    url(r'^accounts/register/api/',include('app.api.urls')),
    #dashboard url's

    url(r'^lender-dashboard/table/', TemplateView.as_view(template_name='lenders/table.html')),
    url(r'^dashboard/', login_required(TemplateView.as_view(template_name='dashboard.html'))),
    url(r'^customer-dashboard/',TemplateView.as_view(template_name='customers/dashboard.html')) ,
    url(r'^lender-dashboard/$', TemplateView.as_view(template_name='lenders/profile.html')),
    url(r'^lender-dashboard/document/', TemplateView.as_view(template_name='lenders/agreement.html')),
    url(r'^lender-dashboard/get-started$', TemplateView.as_view(template_name='lenders/dashboard.html')),
    url(r'^staffdashboard/(?P<template_name>\w+)$', SimpleStaticView.as_view(), name='staff'),    
    url(r'^customerdashboard/mylender/$', TemplateView.as_view(template_name='customers/myLender.twig')),
    url(r'^customerdashboard/myaccount/$', TemplateView.as_view(template_name='customers/profile.html')),
    url(r'^lender-dashboard/leads/', TemplateView.as_view(template_name='lenders/leads.html')),
    url(r'^staffdashboard/appointments/', TemplateView.as_view(template_name='staff/appointment.html')),
    url(r'^lender-dashboard/inbox/$', TemplateView.as_view(template_name='lenders/inbox.html'), name="lender-messages"),
    # freshdesk remote api login sso config
    url(r'^login/sso/', include('freshdesk.urls')),
    url(r'^pages/', include('django.contrib.flatpages.urls')),
    
    # django-registration URLs:    
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'', include('django.contrib.auth.urls')),    
    url(r'^lender-dashboard/payments/', include('djstripe.urls', namespace="djstripe")),
    url(r'^freshdesk/', include('freshdesk_api.urls')),
    
    url(r'^avatar/', include('avatar.urls')),
    url(r'helpdesk/', include('helpdesk.urls')),
    url(r'filemanager/', include('filemanager.urls', namespace='filemanager')),
    url(r'^messages/', include('monitio.urls', namespace='monitio')),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^mapview/', TemplateView.as_view(template_name='mapview.html')),
    url(r'^staffdashboard/appointmentslist/', TemplateView.as_view(template_name='staff/appointmentslist.html')),
    url(r'^staffdashboard/appointment/id/(\d{1,4})/$', TemplateView.as_view(template_name='staff/appointment.html')),
    url(r'^staffdashboard/updatelender/(\d{1,4})/api/',include('app.api.urls')),
    url(r'^lender-dashboard/documents/', TemplateView.as_view(template_name='SIGN_URL.html')),
    url(r'^staffdashboard/inbox/', TemplateView.as_view(template_name='staff/inbox.html')),
    url(r'^staffdashboard/table/', TemplateView.as_view(template_name='staff/table.html')),
    url(r'^staffdashboard/profile/', TemplateView.as_view(template_name='staff/profile.html')),
    url(r'^staffdashboard/updatelender/(\d{1,4})/$', TemplateView.as_view(template_name='staff/updatelender.html')),
    #new urls for customer signup flow
    url(r'^application/$', TemplateView.as_view(template_name='index.html')),
    url(r'^application/api/', include('app.api.urls')),
    #url(r'^goodnews/', login_required(TemplateView.as_view(template_name='customers/goodnews.html'))),
    url(r'^goodnews/', TemplateView.as_view(template_name='customers/goodnews.html')),
    
    # Url definition for lender's store @jeevan
    url(r'^lender-dashboard/stores/', login_required(TemplateView.as_view(template_name='lenders/stores.html')), name="locations"),
    url(r'^lender-dashboard/stores/(?P<template_name>\w+)$', SimpleStaticView.as_view(), name='app/lender_locations'),
    
    # manager account portal
    url(r'^manager/', login_required(TemplateView.as_view(template_name='manager/dashboard.html'))),
)

if settings.DEBUG:
    from django.views.static import serve
    urlpatterns += patterns('',
        url(r'^(?P<path>favicon\..*)$', serve, {'document_root': settings.STATIC_ROOT}),
        url(r'^%s(?P<path>.*)$' % settings.MEDIA_URL[1:], serve, {'document_root': settings.MEDIA_ROOT}),
        url(r'^%s(?P<path>.*)$' % settings.STATIC_URL[1:], 'django.contrib.staticfiles.views.serve', dict(insecure=True)),
    )