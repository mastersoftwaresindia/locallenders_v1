# Django settings for app project.
from django.conf import global_settings

DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Jeevan', 'mss.jeevanverma@gmail.com'),
    ('William', 'williamjwest@gmail.com'),
    ('Sachin', 'tend.sachin44@gmail.com'),
    ('Biri Singh', 'mss.birisingh@gmail.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'db_locallenders_5',                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': '',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['app.com']

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/Los_Angeles'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/app.com/media/"
import os.path
MEDIA_ROOT = os.path.normpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'media'))

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://app.com/media/", "http://media.app.com/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/app.com/static/"
import os.path
STATIC_ROOT = os.path.normpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'static'))

# URL prefix for static files.
# Example: "http://app.com/static/", "http://static.app.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    os.path.normpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'assets')),
    #os.path.normpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'static')),
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'm7^i&pc&^*nx*3y9ui%c68c(lcor-^3_-ma+qjk!5lz7c!wrts'

SITE_NAME="http://www.mastersoftwaretechnologies.com:9602"

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
     #"payments.middleware.ActiveSubscriptionMiddleware",
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'app.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'app.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.normpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'api', 'templates')),
    os.path.normpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'api', 'templates', 'staff')),
    '/var/www/locallenders:/var/www/locallenders/locallender/django-monitio/monitio/templates'
)

REST_FRAMEWORK = {
    # TODO: Setup token authentication or restrict to only LeadFlash's IP address, if that is secure.
    #'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.IsAdminUser',),
    'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.AllowAny',),
    'PAGINATE_BY': 10
}
TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.request',
    'djstripe.context_processors.djstripe_settings',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.static',
)

FRESHDESK_URL = 'https://locallenders.freshdesk.com/'

FRESHDESK_SECRET_KEY = '9cc2f1fc10f66e2cedec37a90df4e2a7'

HELPDESK_DEFAULT_SETTINGS = {
        'use_email_as_submitter': True,
        'email_on_ticket_assign': True,
        'email_on_ticket_change': True,
        'login_view_ticketlist': True,
        'email_on_ticket_apichange': True,
        'tickets_per_page': 25
        }


INSTALLED_APPS = (
    'django.contrib.auth',
    'dash',
    'dash.contrib.layouts.android',
    'dash.contrib.layouts.bootstrap2',
    'dash.contrib.layouts.windows8',
    'dash.contrib.plugins.dummy',
    'dash.contrib.plugins.image',
    'dash.contrib.plugins.memo',
    'dash.contrib.plugins.news',
    'dash.contrib.plugins.rss_feed',
    'dash.contrib.plugins.url',
    'dash.contrib.plugins.video',
    'dash.contrib.plugins.weather',
    'freshdesk',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_extensions',
    'south',
    'rest_framework',
    'app.api',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'django.contrib.flatpages',
    'tinymce',
    'flatpages_tinymce',
    'django_wysiwyg',
    'registration',
    "django_forms_bootstrap",
    "djstripe",
    'avatar',
    'django.contrib.admin',  # Required for helpdesk admin/maintenance  # Required for helpdesk text display
    'markdown_deux',
    'helpdesk',
    'bootstrapform',
    'filemanager',
    'django_sse',
    'corsheaders',
    'monitio',
    'easy_maps',
    'djangular',
    'djrill',
    'django_crontab',
    'locations',
)

CRONJOBS = [
        ('*/1 * * * *', 'app.api.api.AddAgents'),
        ('*/2 * * * *', 'app.api.api.InsertInvoiceClients'),
        ('* * */7 * *', 'app.api.api.GenerateWeeklyReports'),
    ]

TINYMCE_DEFAULT_CONFIG = {
    # custom plugins
    'plugins': "table,spellchecker,paste,searchreplace",
    # editor theme
    'theme': "advanced",
    # custom CSS file for styling editor area
    'content_css': MEDIA_URL + "css/custom_tinymce.css",
    # use absolute urls when inserting links/images
    'relative_urls': False,
}

ACCOUNT_ACTIVATION_DAYS = 7
DJANGO_WYSIWYG_FLAVOR = "tinymce"    # or "tinymce_advanced"

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
STRIPE_PUBLIC_KEY = os.environ.get("STRIPE_PUBLIC_KEY", "pk_test_3j0y9oG0BYaFWw8DbkpRiXQt")
STRIPE_SECRET_KEY = os.environ.get("STRIPE_SECRET_KEY", "sk_test_d1pjj1Eim1yflWqFSW2avsLb")

DJSTRIPE_PLANS = {
    "Weekly": {
        "stripe_plan_id": "pro-weekly",
        "name": "weekly Pro ($300/week)",
        "description": "The weekly subscription plan, offering 75 leads / week at $4/lead",
        "price": 30000,  # $20.00
        "currency": "usd",
        "interval": "week"
    },
    "monthly": {
        "stripe_plan_id": "pro-monthly",
        "name": "Web App Pro ($25/month)",
        "description": "The monthly subscription plan to WebApp",
        "price": 25000,  # $250.00
        "currency": "usd",
        "interval": "month"
    },
    "yearly": {
        "stripe_plan_id": "pro-yearly",
        "name": "Web App Pro ($199/year)",
        "description": "The annual subscription plan to WebApp",
        "price": 199000,  # $1990.00
        "currency": "usd",
        "interval": "year"
    }
}

USER_ROLES = (
    'customer',
    'lender',
    'manager',
)

LOGIN_REDIRECT_URL = '/dashboard/'
LOGIN_URL = '/accounts/login/'
LOGIN_ERROR_URL = '/accounts/login/'
LOGOUT_URL = '/accounts/logout/'
AUTH_USER_MODEL = 'api.User'

#### !!This is for demonstration only!! ####
AUTHENTICATION_BACKENDS = ['app.api.auth.AlwaysRootBackend']


SITE_URL='http://mastersoftwaretechnologies.com:9602'


# setting up email backend
'''EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'mss.sachinsharma@gmail.com'
EMAIL_HOST_PASSWORD = 'bph_b}$5P)dEdRv'
EMAIL_USE_TLS = True'''

# setting up email backend
EMAIL_BACKEND = 'djrill.mail.backends.djrill.DjrillBackend'
MANDRILL_API_KEY = 'pOSwJcEmhJ1rrwF8AbM4uA'
EMAIL_HOST = 'smtp.mandrillapp.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'mss.sachinsharma@gmail.com'
EMAIL_HOST_PASSWORD = 'msssachin10'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = "mss.sachinsharma@gmail.com"


MESSAGE_STORAGE = 'monitio.storage.PersistentMessageStorage'
MONITIO_EXCLUDE_READ = True
REDIS_SSEQUEUE_CONNECTION_SETTINGS = {
    'location': '127.0.0.1:6379',
    'db': 0,
}

CORS_ORIGIN_WHITELIST = (
    '127.0.0.1',
    '127.0.0.1:8000',
)
