define(['auth/module'], function(module){
    "use strict";

    return module.registerDirective('loginInfo', function(User){

        return {
            restrict: 'A',
            templateUrl: 'static/public/app/auth/directives/login-info.tpl.html',
            link: function(scope, element){
                User.initialized.then(function(){
                    scope.user = User
                });
            }
        }
    })
});
