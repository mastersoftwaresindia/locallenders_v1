var require = {
    waitSeconds: 0,
    paths: {

        'jquery': [
            '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min',
            'static/public/plugin/jquery/dist/jquery.min'
        ],
        'jquery-ui': '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min',

        'bootstrap': '//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min',

        'angular': '//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min',
        'angular-cookies': '//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular-cookies.min',
        'angular-resource': '//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular-resource.min',
        'angular-sanitize': '//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular-sanitize.min',
        'angular-animate': '//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular-animate.min',


        'domReady': 'static/public/plugin/requirejs-domready/domReady',

        'angular-ui-router': 'static/public/plugin/angular-ui-router/release/angular-ui-router.min',

        'angular-google-maps': 'static/public/plugin/angular-google-maps/dist/angular-google-maps.min',

        'angular-bootstrap': 'static/public/plugin/angular-bootstrap/ui-bootstrap-tpls.min',

        'angular-couch-potato': 'static/public/plugin/angular-couch-potato/dist/angular-couch-potato',

        'angular-easyfb': 'static/public/plugin/angular-easyfb/angular-easyfb.min',
        'angular-google-plus': 'static/public/plugin/angular-google-plus/dist/angular-google-plus.min',

        'pace':'static/public/plugin/pace/pace.min',

        'fastclick': 'static/public/plugin/fastclick/lib/fastclick',

        'jquery-color': 'static/public/plugin/jquery-color/jquery.color',

        'select2': 'static/public/plugin/select2/select2.min',

        'summernote': 'static/public/plugin/summernote/dist/summernote.min',

        'he': 'static/public/plugin/he/he',
        'to-markdown': 'static/public/plugin/to-markdown/src/to-markdown',
        'markdown': 'static/public/plugin/markdown/lib/markdown',
        'bootstrap-markdown': 'static/public/plugin/bootstrap-markdown/js/bootstrap-markdown',

        'ckeditor': 'static/public/plugin/ckeditor/ckeditor',

        'moment': 'static/public/plugin/moment/min/moment-with-locales.min',
        'moment-timezone': 'static/public/plugin/moment-timezone/moment-timezone',

        'sparkline': 'static/public/plugin/relayfoods-jquery.sparkline/dist/jquery.sparkline.min',
        'easy-pie': 'static/public/plugin/jquery.easy-pie-chart/dist/jquery.easypiechart.min',

        'flot': 'static/public/plugin/flot/jquery.flot.cust.min',
        'flot-resize': 'static/public/plugin/flot/jquery.flot.resize.min',
        'flot-fillbetween': 'static/public/plugin/flot/jquery.flot.fillbetween.min',
        'flot-orderBar': 'static/public/plugin/flot/jquery.flot.orderBar.min',
        'flot-pie': 'static/public/plugin/flot/jquery.flot.pie.min',
        'flot-time': 'static/public/plugin/flot/jquery.flot.time.min',
        'flot-tooltip': 'static/public/plugin/flot/jquery.flot.tooltip.min',

        'raphael': 'static/public/plugin/morris/raphael.min',
        'morris': 'static/public/plugin/morris/morris.min',

        'dygraphs': 'static/public/plugin/dygraphs/dygraph-combined.min',
        'dygraphs-demo': 'static/public/plugin/dygraphs/demo-data.min',

        'chartjs': 'static/public/plugin/chartjs/chart.min',

        'datatables': 'static/public/plugin/datatables/media/js/jquery.dataTables.min',
        'datatables-bootstrap': 'static/public/plugin/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap',
        'datatables-tools': 'static/public/plugin/datatables-tabletools/js/dataTables.tableTools',
        'datatables-colvis': 'static/public/plugin/datatables-colvis/js/dataTables.colVis',
        'datatables-responsive': 'static/public/plugin/datatables-responsive/files/1.10/js/datatables.responsive',


        'jqgrid':'static/public/plugin/jqgrid/js/minified/jquery.jqGrid.min',
        'jqgrid-locale-en':'static/public/plugin/jqgrid/js/i18n/grid.locale-en',


        'jquery-maskedinput': 'static/public/plugin/jquery-maskedinput/dist/jquery.maskedinput.min',
        'jquery-validation': 'static/public/plugin/jquery-validation/dist/jquery.validate.min',
        'jquery-form': 'static/public/plugin/jquery-form/jquery.form',

        'bootstrap-validator': 'static/public/plugin/bootstrapvalidator/dist/js/bootstrapValidator.min',

        'bootstrap-timepicker': 'static/public/plugin/bootstrap3-fontawesome-timepicker/js/bootstrap-timepicker.min',
        'clockpicker': 'static/public/plugin/clockpicker/dist/bootstrap-clockpicker.min',
        'nouislider': 'static/public/plugin/nouislider/distribute/jquery.nouislider.min',
        'ionslider': 'static/public/plugin/ion.rangeSlider/js/ion.rangeSlider.min',
        'bootstrap-duallistbox': 'static/public/plugin/bootstrap-duallistbox/dist/jquery.bootstrap-duallistbox.min',
        'bootstrap-colorpicker': 'static/public/plugin/bootstrap-colorpicker/js/bootstrap-colorpicker',
        'jquery-knob': 'static/public/plugin/jquery-knob/dist/jquery.knob.min',
        'bootstrap-slider': 'static/public/plugin/seiyria-bootstrap-slider/dist/bootstrap-slider.min',
        'bootstrap-tagsinput': 'static/public/plugin/bootstrap-tagsinput/dist/bootstrap-tagsinput.min',
        'x-editable': 'static/public/plugin/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min',
        // 'angular-x-editable': 'static/public/plugin/angular-xeditable/dist/js/xeditable.min',

        'fuelux-wizard': 'static/public/plugin/fuelux/js/wizard',

        'dropzone': 'static/public/plugin/dropzone/downloads/dropzone.min',

        'jcrop': 'static/public/plugin/jcrop/js/jquery.Jcrop.min',


        'bootstrap-progressbar': 'static/public/plugin/bootstrap-progressbar/bootstrap-progressbar.min',
        'jquery-nestable': 'static/public/plugin/jquery-nestable/jquery.nestable',

        'superbox': 'static/public/plugin/superbox/superbox.min',


        'jquery-jvectormap': 'static/public/plugin/vectormap/jquery-jvectormap-1.2.2.min',
        'jquery-jvectormap-world-mill-en': 'static/public/plugin/vectormap/jquery-jvectormap-world-mill-en',


        'lodash': 'static/public/plugin/lodash/dist/lodash.min',


        'magnific-popup': 'static/public/plugin/magnific-popup/dist/jquery.magnific-popup',

        'fullcalendar': '../smartadmin-plugin/fullcalendar/jquery.fullcalendar.min',
        'smartwidgets': '../smartadmin-plugin/smartwidgets/jarvis.widget.min',
        'notification': '../smartadmin-plugin/notification/SmartNotification.min',

        // app js file includes
        'appConfig': '../app.config',
        'modules-includes': 'includes'

    },
    shim: {
        'angular': {'exports': 'angular', deps: ['jquery']},

        'angular-animate': { deps: ['angular'] },
        'angular-resource': { deps: ['angular'] },
        'angular-cookies': { deps: ['angular'] },
        'angular-sanitize': { deps: ['angular'] },
        'angular-bootstrap': { deps: ['angular'] },
        'angular-ui-router': { deps: ['angular'] },
        'angular-google-maps': { deps: ['angular'] },

        'angular-couch-potato': { deps: ['angular'] },

        'socket.io': { deps: ['angular'] },

        'anim-in-out': { deps: ['angular-animate'] },
        'angular-easyfb': { deps: ['angular'] },
        'angular-google-plus': { deps: ['angular'] },

        'select2': { deps: ['jquery']},
        'summernote': { deps: ['jquery']},

        'to-markdown': {deps: ['he']},

        'bootstrap-markdown': { deps: ['jquery', 'markdown', 'to-markdown']},

        'ckeditor': { deps: ['jquery']},

        'moment': { exports: 'moment'},
        'moment-timezone': { deps: ['moment']},
        'moment-timezone-data': { deps: ['moment']},
        'moment-helper': { deps: ['moment-timezone-data']},
        'bootstrap-daterangepicker': { deps: ['jquery', 'moment']},

        'flot': { deps: ['jquery']},
        'flot-resize': { deps: ['flot']},
        'flot-fillbetween': { deps: ['flot']},
        'flot-orderBar': { deps: ['flot']},
        'flot-pie': { deps: ['flot']},
        'flot-time': { deps: ['flot']},
        'flot-tooltip': { deps: ['flot']},

        'morris': {deps: ['raphael']},

        'datatables':{deps: ['jquery']},
        'datatables-colvis':{deps: ['datatables']},
        'datatables-tools':{deps: ['datatables']},
        'datatables-bootstrap':{deps: ['datatables','datatables-tools','datatables-colvis']},
        'datatables-responsive': {deps: ['datatables']},

        'jqgrid' : {deps: ['jquery']},
        'jqgrid-locale-en' : {deps: ['jqgrid']},

        'jquery-maskedinput':{deps: ['jquery']},
        'jquery-validation':{deps: ['jquery']},
        'jquery-form':{deps: ['jquery']},
        'jquery-color':{deps: ['jquery']},

        'jcrop':{deps: ['jquery-color']},

        'bootstrap-validator':{deps: ['jquery']},

        'bootstrap-timepicker':{deps: ['jquery']},
        'clockpicker':{deps: ['jquery']},
        'nouislider':{deps: ['jquery']},
        'ionslider':{deps: ['jquery']},
        'bootstrap-duallistbox':{deps: ['jquery']},
        'bootstrap-colorpicker':{deps: ['jquery']},
        'jquery-knob':{deps: ['jquery']},
        'bootstrap-slider':{deps: ['jquery']},
        'bootstrap-tagsinput':{deps: ['jquery']},
        'x-editable':{deps: ['jquery']},

        'fuelux-wizard':{deps: ['jquery']},
        'bootstrap':{deps: ['jquery']},

        'magnific-popup': { deps: ['jquery']},
        'modules-includes': { deps: ['angular']},
        'sparkline': { deps: ['jquery']},
        'easy-pie': { deps: ['jquery']},
        'jquery-jvectormap': { deps: ['jquery']},
        'jquery-jvectormap-world-mill-en': { deps: ['jquery']},

        'dropzone': { deps: ['jquery']},

        'bootstrap-progressbar': { deps: ['bootstrap']},


        'jquery-ui': { deps: ['jquery']},
        'jquery-nestable': { deps: ['jquery']},

        'superbox': { deps: ['jquery']},

        'notification': { deps: ['jquery']},

        'smartwidgets': { deps: ['jquery-ui']}

    },
    priority: [
        'jquery',
        'bootstrap',
        'angular'
    ]
};