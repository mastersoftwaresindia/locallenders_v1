define(['app'], function(module){
    "use strict";

    module.registerDirective('languageSelector', function(Language){
        return {
            restrict: "EA",
            replace: true,
            templateUrl: "static/public/app/components/language/language-selector.tpl.html",
            scope: true,
        }
    })
});
