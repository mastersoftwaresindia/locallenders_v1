define([
    // account
    'static/public/auth/module',
    'static/public/auth/models/User',

    // layout

    'static/public/layout/module',
    'static/public/layout/actions/minifyMenu',
    'static/public/layout/actions/toggleMenu',
    'static/public/layout/actions/fullScreen',
    'static/public/layout/actions/resetWidgets',
    'static/public/layout/actions/resetWidgets',
    'static/public/layout/actions/searchMobile',
    'static/public/layout/directives/demo/demoStates',
    'static/public/layout/directives/smartInclude',
    'static/public/layout/directives/smartDeviceDetect',
    'static/public/layout/directives/smartFastClick',
    'static/public/layout/directives/smartLayout',
    'static/public/layout/directives/smartSpeech',
    'static/public/layout/directives/smartRouterAnimationWrap',
    'static/public/layout/directives/smartFitAppView',
    'static/public/layout/directives/radioToggle',
    'static/public/layout/directives/dismisser',
    'static/public/layout/directives/smartMenu',
    'static/public/layout/directives/bigBreadcrumbs',
    'static/public/layout/directives/stateBreadcrumbs',
    'static/public/layout/directives/smartPageTitle',
    'static/public/layout/directives/hrefVoid',
    'static/public/layout/service/SmartCss',
    'static/public/modules/widgets/directives/widgetGrid',
    'static/public/modules/widgets/directives/jarvisWidget',


    // dashboard
    'static/public/dashboard/module',


    //components
    'static/public/components/language/Language',
    'static/public/components/language/languageSelector',
    'static/public/components/language/language-controller',

    'static/public/components/projects/Project',
    'static/public/components/projects/recentProjects',

    'static/public/components/activities/activities-controller',
    'static/public/components/activities/activities-dropdown-toggle-directive',
    'static/public/components/activities/activities-service',

    'static/public/components/shortcut/shortcut-directive',

    'static/public/components/calendar/module',
    'static/public/components/calendar/models/CalendarEvent',
    'static/public/components/calendar/directives/fullCalendar',
    'static/public/components/calendar/directives/dragableEvent',
    'static/public/components/calendar/controllers/CalendarCtrl',

    'static/public/components/inbox/module',
    'static/public/components/inbox/models/InboxConfig',
    'static/public/components/inbox/models/InboxMessage',

    'static/public/components/todo/TodoCtrl',
    'static/public/components/todo/models/Todo',
    'static/public/components/todo/directives/todoList',

    // chat
    'static/public/components/chat/module',

    // graphs
    'static/public/modules/graphs/module',


    // tables
    'static/public/modules/tables/module',

    // forms
    'static/public/modules/forms/module',

    // ui
    'static/public/modules/ui/module',

    // widgets
    'static/public/modules/widgets/module',

    // widgets
    'static/public/modules/maps/module',

    // appViews
    'static/public/modules/app-views/module',

    // misc
    'static/public/modules/misc/module',

    // smartAdmin
    'static/public/modules/smart-admin/module'

], function () {
    'use strict';
});
