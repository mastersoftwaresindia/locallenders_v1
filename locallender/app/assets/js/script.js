/***
 * Define global application variable to call
 * through out app for angular controllers
 * */
var app;


/***
 * Define global function call automatically on load
 * */
(function() {  

  app = angular.module('app.app.static', [])
  
  .config(function($routeProvider,$locationProvider){    
    $locationProvider.html5Mode(true);
  });
  
  app.controller('AutoRequestController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      function addAgents() {        
        /*$http({ 
              method: 'POST', 
              url: 'api/addAgents/', 
              data: {"test":"test"},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data);
            }).error(function(data, status, headers, config) {
              console.log("error");
            });*/
        
        /*$http({ 
              method: 'POST', 
              url: 'api/insertInvoiceClients/', 
              data: {"test":"test"},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data);
            }).error(function(data, status, headers, config) {
              console.log("error");
            });*/
        
        /*window.setTimeout(function() {
           $http({ 
              method: 'GET', 
              url: 'api/getActivestatus/', 
            }).success(function(data, status, headers, config) {
               console.log(data,config);
                if (data=='empty') {
                   
                      $http({ 
                      method: 'GET', 
                      url: url,
                      }).success(function(data) {
                            console.log('data');
                             }).error(function(data, status, headers, config) {
                      console.log("error");
                        });
                }
            }).error(function(data, status, headers, config) {
              console.log("error");
              });
	},180000);*/
	setTimeout(addAgents,80000);
      }
      addAgents();
    }
  ]); 
  
  app.controller('DocumentsController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      $http({ 
              method: 'GET', 
              url: '/api/getSignUrl/', 
              }).success(function(data, status, headers, config) {
                console.log("data",data.sign_url);
                $scope.sign_url=data.sign_url;
                $scope.client_id=data.client_id;
                HelloSign.init(data.client_id);
                  HelloSign.open({
                  url: data.sign_url,
                  skipDomainVerification:true,
                  allowCancel: true,
                  messageListener: function(eventData) {

                  }    
    });
              }).error(function(data, status, headers, config) {
                console.log("error");
              });
    }
    ]);
  
  app.controller('AppController', [
    '$scope', '$http','$location','$route','$window', function($scope, $http,$location,$route,$window) {
      var cookie = document.cookie;
      var cookievalue = read_cookie('email');
      $('#useremail').val(cookievalue);
      $scope.submit = function(){
        
        var csrf = $('input[type=hidden]').val();
        var email = this.email;
        var valid = validateEmail(email);
        document.cookie= "email =" + email +("; path=/");
        if (valid == true) {
            var user = {
                email:email,
                Token:csrf,
            }
            $http({ 
              method: 'POST', 
              url: '/api/customerexists', 
              data: {userEmail:email},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("response",data);
                if (data.result == 'success') {
                    $('.oauthStatus').html("Please wait....").promise().done(function(){
                      $('.oauthStatus').html("Redirect to application page");
                      $window.location.href="/accounts/register/";
                    });
                }else{
                  $('.oauthStatus').html(data.detail + ".Please Click <a href='/accounts/login/' target='_self'>here</a> to login");
                }
            }).error(function(data, status, headers, config) {
              $('#authStaffStatus').html("Please wait....").promise().done(function(){
                $('#authStaffStatus').html(data);
              });
            });
        }else{
          $('.oauthStatus').html("Error");
        }
      };
    }
      
  ]);
  
  app.controller('CustomerSignUpController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      $("#customerPhoneNumber,#customerZipCode").on('keyup',function(e) {
              this.value = (this.value.replace(/[^0-9.\,]/g, ''));
       });
       $("#customerFirstName,#customerLastName").on('keyup',function(e) {
              this.value = (this.value.replace(/[^a-zA-Z_ \,]/g, ''));
       });
	var url = window.location.href;
        if(url.indexOf("ssn") > -1) {
        parts=url.split('=');
        console.log("parts",parts)
        ssn=parts[1];
        $http({ 
              method: 'POST', 
              url: '/api/getCustomerEmail/', 
              data: {ssn:ssn},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log(data);
		if(data.result=='success'){
                	email=data.info;
			$("#id_email").val(email);
			$("#id_email").prop('disabled', true);
                	console.log("email",email)
		}else{
		$("#id_email").prop('disabled', false);
		console.log("sorry check your ssn");		
		}	
            }).error(function(data, status, headers, config) {
            console.log("error")
            });
      }
       
      $scope.submit = function() {
        var useremail = $('#useremail').val();
        document.cookie= "email =" + useremail +("; path=");
        var group = "Customers"
        if (this.customer_email && this.customerPhoneNumber && this.customerZipCode ) {
        $("#loader_app").show();
        $http({ 
              method: 'POST', 
              url: 'api/customersignup/', 
              data: {email:this.customer_email,phone_number:this.customerPhoneNumber,zip_code:this.customerZipCode,
              group:group},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log(data);
                if (data.result == "success") {
                  $("#customersignup-form").hide();
                  $("#loader_app").hide();
		  $("#info_text").hide();
                  //var form = document.getElementById("customersignup-form");
                  //form.reset();
                   $('#authStaffStatus').show();
                  //$('#authStaffStatus').html(data.details);
                }else{
                  $("#loader_app").hide();
		  $('#signupstatus').show();
                  $('#signupstatus').html(data.details);
		  $('#signupstatus').delay(5000).fadeOut();
                }
            }).error(function(data, status, headers, config) {
            console.log("error")
            });
        }
      }
    }
  ]);
  
// New lender sign up controller @locallenders.net starts @jv
app.controller('LenderSignUpController', ['$scope', '$http','$window', function($scope, $http,$window) {
    $("#phoneNumber,#lenderZipcode").on('keyup',function(e) {
      this.value = (this.value.replace(/[^0-9.\,]/g, ''));
    });
    
    $("#loanLimit").on('keyup',function(e) {
      this.value = (this.value.replace(/^(0*)/,""));
    });  
      
    $scope.submit = function() {
      $("#loader_app").show();
      $http({ 
	  method: 'POST', 
	  url: '/api/lendersignup', 
	  data: {email:this.email,phone_number:this.phone_number,product_at_store:this.product_at_store,group:"Lenders",zip_code:this.zip_code},
	  headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
	}).success(function(data, status, headers, config) {
	    if (data.result == 'success') {
	      $('#resultStatus').html(data.support).promise().done(function(){
		$("#lendersignup-form").hide();
		$("#loader_app").hide();
		$(this).removeClass('hide');
	      });	      
	    }else{
	      $('.error_part').html(data.details).promise().done(function(){
		$(this).removeClass('hide');
		$("#loader_app").hide();
	      });
	    }
	}).error(function(data, status, headers, config) {
	    console.log("here in error",data);
	});
    }
  }
]);
// New lender sign up controller @locallenders.net ends @jv
  
  app.controller('SignInController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      $scope.submit = function() {
        var username = this.username;
        var password = this.password;
        $http({ 
              method: 'POST', 
              url: '/api/login', 
              data: {username:username,password:password},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data);
                if (!data.error) {
                  $('#LoginStatus').html(data.detail);
                  $window.location.href="/dashboard";
                  
                }else{
                  $('#LoginStatus').html('Please provide valid credentials');
                }
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      }
    }
  ]);
  
    app.controller('bootstrap_modal_controller', [
    '$scope', '$http','$window', function($scope, $http,$window) {
        $(document).on("click", "#appointment", function (e) {
          $(function() {
            var dateToday = new Date();
            $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd', minDate: 0});
    
          });
          
          $('.lender_email').html($('#hid_email1').val());
          $('.customer_email').html($('#hid_email2').val());
          var email_1 = $('#hid_email1').val();
          var email_2 = $('#hid_email2').val();
          $("textarea.message").val(email_2+' has requested to set an appointment with '+email_1);
        });
      
      $scope.setAppointment=function(){
        lender_email = $('#hid_email1').val();
        customer_email = $('#hid_email2').val();
        var date = $("#datepicker").val();
        message= customer_email+' has requested to set an appointment with '+lender_email + ' on dated ' + date
        console.log("data",lender_email,customer_email,date,message);
        $http({ 
              method: 'POST', 
              url: '/api/appointmentRequest/', 
              data: {"lender_email":lender_email,"customer_email":customer_email,"date":date,"message":message},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data.details);
                if (data.result=='success') {
                  $('#appoint_status').show();
                  $('#appoint_status').html(data.details);
                  setTimeout(
                    function() 
                    {
                      $("#apoint_close").click();
                    }, 3000);
                }else{
                  $('#appoint_status').show();
                  $('#appoint_status').html(data.details);
                  setTimeout(
                    function() 
                    {
                      $("#apoint_close").click();
                    }, 3000);
                }
                
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      }
      
      
      }
  ]);
  
  
  
  
  //Controller to get the choosen lender by a customer
  app.controller('Selected-LenderController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      
      $scope.submit=function(){
        status=this.status;
        $http({ 
              method: 'POST', 
              url: '/api/removeLender/', 
              data: {status:status},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data.result);
                if (data.result == 'success') {
                  $window.location.href = '/customer-dashboard/'
                }else{
                  console.log("error");
                }
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      }
      
 
      
      
      $("#clickme").on("click",function(){
        var se=$("#choose");
        se.show();
        se[0].size=2;
      });

      $("#choose").on("click",function(){
        var se=$(this);
        se.hide();
        var sel = $("#lender_select");
        sel.show();
      });
    
    
      $http({ 
              method: 'GET', 
              url: '/api/mylender', 
            }).success(function(data, status, headers, config) {
                console.log("data",data);
                if (data.result == 'error') {
                  $('#lenderStatus').html(data.detail);
                }else{
                  $scope.Lender = data; 
                }
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      
      
    }
  ]);


  app.controller('GoodNewsController', [
    '$scope', '$http','$window', function($scope, $http,$window,time) {
        $http({ 
              method: 'GET', 
              url: '/api/mylocation', 
            }).success(function(data, status, headers, config) {
               console.log("data",data);
               if (data.result == 'error') {
		  $scope.lender = ''
		  $scope.lenderlength = 0;
               }else{
		  $scope.lender=data;
		  $scope.lenderlength = 1;
               }
              }).error(function(data, status, headers, config) {
		  console.log("error");
              });
    }
  ]);
  
  
  app.controller('ForgotPasswordController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      $scope.submit = function() {
        var email = this.email;
        $http({ 
              method: 'POST', 
              url: 'api/forgotpassword', 
              data: {email:email},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data.result);
                if (data.result == 'success') {
                  $('#passwordStatus').html(data.detail);
                }else{
                  $('#passwordStatus').html('Please provide valid credentials');
                }
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
        
      }
    }
  ]);
  //app.controller('StripeController', [
  //  '$scope', '$http','$window', function($scope, $http,$window) {
  //    $scope.stripePayment=function(){
  //      
  //      console.log(this.amount );
  //    }
  //  return false;
  //  }
  //]);
}).call(this);



////new added code for dashboard to get lenders list////

(function() {
var app;

  app = angular.module('app.app.dashboard', ['ui.bootstrap'])
  .config(function($routeProvider,$locationProvider){
    
      $locationProvider.html5Mode(true);
  });

app.controller('StaffController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      $scope.submit = function(person, resultVarName) {
        $http({ 
              method: 'POST', 
              url: 'api/staff', 
              data: {staffEmail:this.staffEmail,staffPassword:this.staffPassword,staffUsername:this.staffUsername},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("response",data);
                $('#authStaffStatus').html("No Staff Member found with these credentials").promise().done(function(){
                if (!data.error) {
                    $('#authStaffStatus').html(data);
                    $window.location.href = data;
                }else{
                  console.log("You are not a staff member");
                }
                
              });
            }).error(function(data, status, headers, config) {
              $('#authStaffStatus').html("Please wait....").promise().done(function(){
                $('#authStaffStatus').html(data);
              });
            });
      };
    }
  ]);

app.controller('AgreementController', [    
    '$scope', '$http','$window', function($scope, $http,$window,time) {
      $http({
              method: 'GET', 
              url: 'api/getAgreementInfo/', 
            }).success(function(data, status, headers, config) {
                console.log("data",data);
                var agreement=data.agreement_status
                if (agreement==false) {
                  $('#agreement_check').prop('checked', false)
                }else{
                  $('#agreement_check').prop('checked', true)
                }
                
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      
      
        $scope.acceptAgreement=function(){
          if($("#agreement_check").prop('checked') == true){
              $http({ 
                method:'POST', 
                url: 'api/acceptAgreement/', 
                data: {'value':'test'},
                headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
                }).success(function(data, status, headers, config) {
                    console.log("response",data);
                }).error(function(data, status, headers, config) {
                  console.log("response",data);
                });
          }else{
            console.log("you need to check the checkbox in order to proceed further")
          }
          
        }
      
      
    }
    ]);


  app.controller('CustomerDashboardController', [    
    '$scope', '$http','$window', function($scope, $http,$window,time) {
      $("#lenderStatus").hide();
      $http({ 
              method: 'GET', 
              url: 'api/getActiveLenders/', 
            }).success(function(data, status, headers, config) {
               //console.log("data",data);
                  $scope.lendersdata=data;
                  $scope.filteredlendersdata = [];
                  $scope.currentPage = 1;
                  $scope.numPerPage = 4;
                  $scope.maxSize = 3;
                  $scope.numPages = function () {
                    return Math.ceil($scope.lendersdata.length / $scope.numPerPage);
                  };
                
                  $scope.$watch('currentPage + numPerPage', function() {
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                    ,end = begin + $scope.numPerPage;
                    $scope.filteredlendersdata = $scope.lendersdata.slice(begin, end);
                  });
              }).error(function(data, status, headers, config) {
              console.log("error");
              });
      
      
      window.setTimeout(function() {
         $scope.todos = [];
            angular.forEach($scope.lendersdata, function(count){
             // console.log();
            $scope.todos.push(count.is_choosen );
            });
            var leads=$scope.todos; 
            console.log(leads);
            console.log();
            if ($.inArray(1, leads)==-1)
                {
               $('#overlay_process').modal({"keyboard": false})
                }
                
        }, 1000);
      
 
      
      
  //    lender status check::
  //    $scope.countdown=function (element, minutes, seconds) {
  //   set time for the particular countdown
  //  var time = minutes*60 + seconds;
  //  var interval = setInterval(function() {
  //    var dt = new Date();
  //    var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
  //    console.log(time);
  //    }, 1000);
  //}
//var s=$scope.countdown('clock', 3, 0);
//         console.log(s);

           
         
                      
    
      $scope.chooseLender=function(lender_id){
        var lenderid = lender_id;
       $http({ 
              method: 'POST', 
              url: 'api/mylender', 
              data: {lenderid:lenderid},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data.result);
                if (data.result == 'success') {
                  $("#lenderStatus").show();
                  $('#lenderStatus').html(data.detail);
                  $('#lenderStatus').delay(3000).fadeOut('slow');
                  $window.location.href = '/customerdashboard/mylender/'
                  
                }else{
                  $("#lenderStatus").show();
                  $('#lenderStatus').html(data.detail);
                  $('#lenderStatus').delay(3000).fadeOut('slow');
                }
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
        
      
      }
    }
    
    ]);

    
  app.controller('AppointmentsController', [
    '$scope', '$http','$window', function($scope, $http,$window,time) {
      
      $http({
              method: 'GET', 
              url: 'api/getAppointments/', 
            }).success(function(data, status, headers, config) {
                console.log("data",data);
                $scope.appointments = data.info;
                /*if (data.result == "success") { 
                  $scope.leadsdata=data.info;
                  $scope.filteredleads = [];
                  $scope.currentPage = 1;
                  $scope.numPerPage = 4;
                  $scope.maxSize = 3;
                  $scope.numPages = function () {
                    return Math.ceil($scope.leadsdata.length / $scope.numPerPage);
                  };
                
                  $scope.$watch('currentPage + numPerPage', function() {
                      var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                      ,end = begin + $scope.numPerPage;
                      $scope.filteredleads = $scope.leadsdata.slice(begin, end);
                  });
                }else{
                  //$('#leadStatus').html(data.detail); 
                }*/
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
    }
    ]);
    
  app.controller('SingleAppointmentController', [
    '$scope', '$http','$window', function($scope, $http,$window,time) {
      var url = document.URL;
      var parts = url.split('/');
      var id = parts[6];
      $http({ 
              method: 'POST', 
              url: 'api/getSingleAppointment/', 
              data: {appointment_id:id},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data);
                $scope.customer=data.Customer_data;
                $scope.lender=data.Lender_data;
                $scope.appointment = data.Appointment_data;
                $scope.stat  =data.status;
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
    }
  ]);
    
  app.controller('staff_modal_controller', [
    '$scope', '$http','$window', function($scope, $http,$window) {
        $(document).on("click", "#schedule", function (e) {
          $(function() {
            $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
          });
          
          $('.lender_email_field').html($('#hid_lender_email').val());
          $('.customer_email_field').html($('#hid_customer_email').val());
          $('.requested_on').html($('#hid_requested_on').val());
          $('.added_on').html($('#hid_added_on').val());
          $('.appoint_location ').html($('#location').val());
          var lender_email= $('#hid_lender_email').val();
          var customer_email = $('#hid_customer_email').val();
        });
        
        $scope.approveAppointment=function(){
          var lender_email= $('#hid_lender_email').val();
          var customer_email = $('#hid_customer_email').val();
          loc=$('#location').val();
          appointment_id=$('#hid_id').val();
          requested_on=$('#hid_requested_on').val();
          var date = $("#datepicker").val();
          var note = $("#note").val();
          if (date) {
            requested_on = date;
          }
          else{
            requested_on = requested_on;
          }
          console.log(loc,appointment_id,requested_on,note);
          $http({ 
              method: 'POST', 
              url: 'api/approveAppointment/', 
              data: {'lender_email':lender_email,'customer_email':customer_email,'appointment_id':appointment_id,'loc':loc,'requested_on':requested_on,'note':note},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data);
                if (data.result=='success') {
                  window.location.reload();
                }else{
                  console.log(data.details);
                }
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
        }
      
      }
  ]);  
    
    
  app.controller('LeadsListingController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      $http({
              method: 'GET', 
              url: 'api/getMyLeads/', 
            }).success(function(data, status, headers, config) {
                console.log("data",data);
		//var limit = data.limits_difference
                if( data.limits_difference ) {
		  $scope.limit_difference=data.limits_difference
                }else{
                  $scope.limit_difference='null'
                }
                
//		$scope.jqgrid_data=data.info;
//                if (typeof data_array=='undefined') {
//		    jqgrid_data=[];
//                }else{
//		    jqgrid_data=jqgrid_data;
//                }
//                var len=data.info.length;
//                for(var i=0;i<len;i++){
//                  var jqgrid_data1 = [{
//					id : data.info[i].first_name,
//					date : data.info[i].Email,
//					name : data.info[i].phone_home,
//					note : data.info[i].appointment_time,
//                                        amount : data.info[i].Zip,
//                                        tax : 'test',
//                                        total : "test",
//                                      }]
//                  jqgrid_data.push({'id':data.info[i].first_name,'date':data.info[i].Email,'name':data.info[i].phone_home,
//                                  'note':data.info[i].appointment_time,'amount':data.info[i].Zip,'tax':'test','total':'loan'})
//                
//                }
//                //console.log("jqgrid_data",jqgrid_data);
//                ////new code///////
//                jQuery("#jqgrid").jqGrid({
//					data : jqgrid_data,
//					datatype : "local",
//					height : 'auto',
//					colNames : ['Actions','Name', 'Email', 'Phone', 'Zip', 'Center', 'Status', 'Time'],
//					colModel : [{
//						name : 'act',
//						index : 'act',
//						sortable : false
//					}, {
//						name : 'id',
//						index : 'id'
//					}, {
//						name : 'date',
//						index : 'date',
//						editable : true
//					}, {
//						name : 'name',
//						index : 'name',
//						editable : true
//					}, {
//						name : 'amount',
//						index : 'amount',
//						align : "right",
//						editable : true
//					}, {
//						name : 'tax',
//						index : 'tax',
//						align : "right",
//						editable : true
//					}, {
//						name : 'total',
//						index : 'total',
//						align : "right",
//						editable : true
//					}, {
//						name : 'note',
//						index : 'note',
//						sortable : false,
//						editable : true
//					}],
//					rowNum : 10,
//					rowList : [10, 20, 30],
//					pager : '#pjqgrid',
//					sortname : 'id',
//					toolbarfilter : true,
//					viewrecords : true,
//					sortorder : "asc",
//					gridComplete : function() {
//						var ids = jQuery("#jqgrid").jqGrid('getDataIDs');
//						for (var i = 0; i < ids.length; i++) {
//							var cl = ids[i];
//							be = "<button class='btn btn-xs btn-default' data-original-title='Edit Row' onclick=\"jQuery('#jqgrid').editRow('" + cl + "');\"><i class='fa fa-pencil'></i></button>";
//							se = "<button class='btn btn-xs btn-default' data-original-title='Save Row' onclick=\"jQuery('#jqgrid').saveRow('" + cl + "');\"><i class='fa fa-save'></i></button>";
//							ca = "<button class='btn btn-xs btn-default' data-original-title='Cancel' onclick=\"jQuery('#jqgrid').restoreRow('" + cl + "');\"><i class='fa fa-times'></i></button>";
//							//ce = "<button class='btn btn-xs btn-default' onclick=\"jQuery('#jqgrid').restoreRow('"+cl+"');\"><i class='fa fa-times'></i></button>";
//							//jQuery("#jqgrid").jqGrid('setRowData',ids[i],{act:be+se+ce});
//							jQuery("#jqgrid").jqGrid('setRowData', ids[i], {
//								act : be + se + ca
//							});
//						}
//					},
//					editurl : "dummy.html",
//					caption : "SmartAdmin jQgrid Skin",
//					multiselect : true,
//					autowidth : true,
//
//				});
//                jQuery("#jqgrid").jqGrid('navGrid', "#pjqgrid", {
//					edit : false,
//					add : false,
//					del : true
//				});
//				jQuery("#jqgrid").jqGrid('inlineNav', "#pjqgrid");
//				/* Add tooltips */
//				$('.navtable .ui-pg-button').tooltip({
//					container : 'body'
//				});
//
//				jQuery("#m1").click(function() {
//					var s;
//					s = jQuery("#jqgrid").jqGrid('getGridParam', 'selarrrow');
//					alert(s);
//				});
//				jQuery("#m1s").click(function() {
//					jQuery("#jqgrid").jqGrid('setSelection', "13");
//				});
//
//				// remove classes
//				$(".ui-jqgrid").removeClass("ui-widget ui-widget-content");
//				$(".ui-jqgrid-view").children().removeClass("ui-widget-header ui-state-default");
//				$(".ui-jqgrid-labels, .ui-search-toolbar").children().removeClass("ui-state-default ui-th-column ui-th-ltr");
//				$(".ui-jqgrid-pager").removeClass("ui-state-default");
//				$(".ui-jqgrid").removeClass("ui-widget-content");
//
//				// add classes
//				$(".ui-jqgrid-htable").addClass("table table-bordered table-hover");
//				$(".ui-jqgrid-btable").addClass("table table-bordered table-striped");
//
//				$(".ui-pg-div").removeClass().addClass("btn btn-sm btn-primary");
//				$(".ui-icon.ui-icon-plus").removeClass().addClass("fa fa-plus");
//				$(".ui-icon.ui-icon-pencil").removeClass().addClass("fa fa-pencil");
//				$(".ui-icon.ui-icon-trash").removeClass().addClass("fa fa-trash-o");
//				$(".ui-icon.ui-icon-search").removeClass().addClass("fa fa-search");
//				$(".ui-icon.ui-icon-refresh").removeClass().addClass("fa fa-refresh");
//				$(".ui-icon.ui-icon-disk").removeClass().addClass("fa fa-save").parent(".btn-primary").removeClass("btn-primary").addClass("btn-success");
//				$(".ui-icon.ui-icon-cancel").removeClass().addClass("fa fa-times").parent(".btn-primary").removeClass("btn-primary").addClass("btn-danger");
//
//				$(".ui-icon.ui-icon-seek-prev").wrap("<div class='btn btn-sm btn-default'></div>");
//				$(".ui-icon.ui-icon-seek-prev").removeClass().addClass("fa fa-backward");
//
//				$(".ui-icon.ui-icon-seek-first").wrap("<div class='btn btn-sm btn-default'></div>");
//				$(".ui-icon.ui-icon-seek-first").removeClass().addClass("fa fa-fast-backward");
//
//				$(".ui-icon.ui-icon-seek-next").wrap("<div class='btn btn-sm btn-default'></div>");
//				$(".ui-icon.ui-icon-seek-next").removeClass().addClass("fa fa-forward");
//
//				$(".ui-icon.ui-icon-seek-end").wrap("<div class='btn btn-sm btn-default'></div>");
//				$(".ui-icon.ui-icon-seek-end").removeClass().addClass("fa fa-fast-forward");
//
//
//			$(window).on('resize.jqGrid', function() {
//				$("#jqgrid").jqGrid('setGridWidth', $("#content").width());
//			})
                
                
                
                $('#leadStatus').html(data.detail);
		$scope.deposite=data.deposite;
                if (data.result =="success") {
		    ///ends/////
		    $scope.hourly_set_val	=data.limits[0].hourly_set;
		    $scope.daily_set_val	=data.limits[0].daily_set;
		    $scope.weekly_set_val	=data.limits[0].weekly_set;
		    $scope.monthly_set_val	=data.limits[0].monthly_set;
		    
		    $scope.leadsdata=data.info;
		    $scope.filteredleads = [];
		    $scope.currentPage = 1;
		    $scope.numPerPage = 4;
		    $scope.maxSize = 3;
		    $scope.numPages = function () {
		      return Math.ceil($scope.leadsdata.length / $scope.numPerPage);
		    };
		  
		    $scope.$watch('currentPage + numPerPage', function() {
		      var begin = (($scope.currentPage - 1) * $scope.numPerPage)
		      ,end = begin + $scope.numPerPage;
		      $scope.filteredleads = $scope.leadsdata.slice(begin, end);
		    });
                }else{
                  $('#leadStatus').html(data.detail); 
                }
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      
      /*$("#ex1Slider").click(function(){
        var progressvalue=  $("#ex1").attr('data')
        var progress = progressvalue.split(":");
        var count = progress[1].replace(/'/g, " ");;
        $http({ 
              method: 'POST', 
              url: 'api/leadsRequest/', 
              data: {count:count},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data);
                $("#requeststatus").show();
                $('#requeststatus').html(data.details);
                $('#requeststatus').delay(3000).fadeOut('slow');
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
        
      });*/
        
	//Leads request by lender
      $scope.submit=function(){
        console.log(this.hourly_set,this.daily_set,this.weekly_set,this.monthly_set)
	if (this.hourly_set>15){
		if(this.hourly_set<this.hourly_set_val){
        		this.daily_set  = (24*this.hourly_set)+10;
        		this.weekly_set  = (24*7*this.hourly_set)+10;
        		this.monthly_set  = (24*7*30*this.hourly_set)+10;
		}else{
		alert("You can not exceed the limit set by staff memeber");
		return false;
			}
	}else{
		alert("Please enter value greater than 15");
		return false;	
		
	}
	limit_diff=this.hourly_set_val-this.hourly_set;
        $http({ 
              method: 'POST', 
              url: 'api/leadsRequest/', 
              data: {'hourly_set':this.hourly_set,'daily_set':this.daily_set,'weekly_set':this.weekly_set,
              'monthly_set':this.monthly_set,'limit_diff':limit_diff},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data);
                $("#requeststatus").show();
                $('#requeststatus').html(data.details);
                $('#requeststatus').delay(3000).fadeOut('slow');
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      }
      //Date Filters
      $scope.eventDateFilter = function(column) {
        if(column === 'all') {
            $http({
              method: 'GET', 
              url: 'api/getMyLeads/', 
            }).success(function(data, status, headers, config) {
                console.log("data",data);
                $('#leadStatus').html(data.detail);
                if (data.result == "success") { 
                  $scope.leadsdata=data.info;
                  $scope.filteredleads = [];
                  $scope.currentPage = 1;
                  $scope.numPerPage = 4;
                  $scope.maxSize = 3;
                  $scope.numPages = function () {
                    return Math.ceil($scope.leadsdata.length / $scope.numPerPage);
                  };
                
                  $scope.$watch('currentPage + numPerPage', function() {
                      var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                      ,end = begin + $scope.numPerPage;
                      $scope.filteredleads = $scope.leadsdata.slice(begin, end);
                  });
                }else{
                  $('#leadStatus').html(data.detail); 
                }
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
        } else if (column === 'lastweek') {
            var firstDay = new Date();
            var previousweek= new Date(firstDay.getTime() - 7 * 24 * 60 * 60 * 1000);
            var prevDate = previousweek.getDate()
            var prevMonth = previousweek.getMonth()
            var digits1=prevDate.toString().length
            if (digits1 == 1) {
              lastweekday = "0" +previousweek.getDate();
            }else{
              lastweekday = previousweek.getDate();
            }
            var digit2 = prevMonth.toString().length
            if (digit2 == 1) {
              lastweekmonth = "0" +previousweek.getMonth();
            }else{
              lastweekmonth = previousweek.getMonth();
            }
            var date = previousweek.getFullYear()+ "-" +lastweekmonth+ "-" +lastweekday;
            var filtered = $scope.leadsdata.filter(Lastweek);
            console.log("filtered",filtered);
            $scope.filteredleads = filtered;
        } else if (column === 'lasttwoweeks') {
            var firstDay = new Date();
            var previousweek= new Date(firstDay.getTime() - 14 * 24 * 60 * 60 * 1000);
            var prevDate = previousweek.getDate()
            var prevMonth = previousweek.getMonth()
            var digits1=prevDate.toString().length
            if (digits1 == 1) {
              lastweekday = "0" +previousweek.getDate();
            }else{
              lastweekday = previousweek.getDate();
            }
            var digit2 = prevMonth.toString().length
            if (digit2 == 1) {
              lastweekmonth = "0" +previousweek.getMonth();
            }else{
              lastweekmonth = previousweek.getMonth();
            }
            var date = previousweek.getFullYear()+ "-" +lastweekmonth+ "-" +lastweekday;
            console.log("date",date)
            var filtered = $scope.leadsdata.filter(Lastweek);
            console.log("filtered",filtered);
            $scope.filteredleads = filtered;         
        } else if (column === 'lastmonth') {
            var firstDay = new Date();
            var previousweek= new Date(firstDay.getTime() - 30 * 24 * 60 * 60 * 1000);
            var prevDate = previousweek.getDate()
            var prevMonth = previousweek.getMonth()
            var digits1=prevDate.toString().length
            if (digits1 == 1) {
              lastweekday = "0" +previousweek.getDate();
            }else{
              lastweekday = previousweek.getDate();
            }
            var digit2 = prevMonth.toString().length
            if (digit2 == 1) {
              lastweekmonth = "0" +previousweek.getMonth();
            }else{
              lastweekmonth = previousweek.getMonth();
            }
            var date = previousweek.getFullYear()+ "-" +lastweekmonth+ "-" +lastweekday;
            console.log("date",date)
            var filtered = $scope.leadsdata.filter(Lastweek);
            console.log("filtered",filtered);
            $scope.filteredleads = filtered;     
        } else if (column === 'twomonthsago') {
            var firstDay = new Date();
            var previousweek= new Date(firstDay.getTime() - 60 * 24 * 60 * 60 * 1000);
            var prevDate = previousweek.getDate()
            var prevMonth = previousweek.getMonth()
            var digits1=prevDate.toString().length
            if (digits1 == 1) {
              lastweekday = "0" +previousweek.getDate();
            }else{
              lastweekday = previousweek.getDate();
            }
            var digit2 = prevMonth.toString().length
            if (digit2 == 1) {
              lastweekmonth = "0" +previousweek.getMonth();
            }else{
              lastweekmonth = previousweek.getMonth();
            }
            var date = previousweek.getFullYear()+ "-" +lastweekmonth+ "-" +lastweekday;
            console.log("date",date)
            var filtered = $scope.leadsdata.filter(Lastweek);
            console.log("filtered",filtered);
            $scope.filteredleads = filtered; 
        }else {
            $scope.dateRange = "";
        }
        
        
        function Lastweek(element) {
              return element.created_on > date;
            }
      };
      //ends here//
      $scope.add = function(){
        var f = document.getElementById('file').files[0]
        console.log(f)
      }
      
      $scope.leadStatus=function(status,id){
        var lead_status = status
        var lead_id = id
        $http({ 
              method: 'POST', 
              url: 'api/leadStatus/', 
              data: {lead_status :lead_status,lead_id : lead_id},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data.result);
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      }
    
    }
  ]);
  
  app.controller('CustomerProfileController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      $("#customerPhone,#phone_cell,#zip_code,#phone_work,#phonework_ext,#routing_number,#account_number,#monthly_income,#months_employed,#security_num,#requested_amount,#months_add,#months_bank").on('keyup',function(e) {
              this.value = (this.value.replace(/[^0-9.\,]/g, ''));
      });
      $("#first_name,#last_name,#city,#middle_init,#employer_name").on('keyup',function(e) {
              this.value = (this.value.replace(/[^a-zA-Z_ \,]/g, ''));
       });
      $http({
              method: 'GET', 
              url: 'api/getCustomerInfo', 
            }).success(function(data, status, headers, config) {
                console.log("data",data[0]);
		Date.prototype.monthName = function() {
                  return this.toUTCString().split(' ')[2]
                };
                d = new Date();
                var month = d.monthName();
                var year = d.getFullYear();
                var day = d.getDate();
                var date = month+' '+day+','+year
                $scope.todayDate = date;
		$scope.Customer=data[0];
                $scope.customerName = data[0].Name;
                $scope.customerPhone = data[0].Phone;
                $scope.customerEmail = data[0].Email;
                $scope.first_name = data[0].first_name;
                $scope.last_name = data[0].last_name;
                $scope.zip_code=data[0].zipCode
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      
      $scope.submit=function(){
        $http({ 
              method: 'POST', 
              url: 'api/saveLeadInfo', 
              data: {first_name:this.first_name,last_name:this.last_name,
              street_addr1:this.customerAddress1,street_addr2:this.customerAddress2,
              Zip:this.zip_code,phone_home:this.customerPhone,email:this.customerEmail},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("here in success",data);
                if (data.result == 'success') {
                  //$("#leadInformation-form").hide();
                  $('#leadStatus').html("Thanks for Saving your information.You can choose a lender now");
                  //var form = document.getElementById("lendersignup-form");
                  //form.reset();
                }else{
                  $('#leadStatus').html(data.details);
                }
            }).error(function(data, status, headers, config) {
              console.log("here in error",data);
            });
      }
    }
  ]);
  
  app.controller('AdvanceCustomerProfileController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      $("#customerPhone,#phone_cell,#zip_code,#phone_work,#phonework_ext,#routing_number,#account_number,#monthly_income,#months_employed,#security_num,#requested_amount,#months_add,#months_bank").on('keyup',function(e) {
              this.value = (this.value.replace(/[^0-9.\,]/g, ''));
      });
      $("#first_name,#last_name,#city,#middle_init,#employer_name").on('keyup',function(e) {
              this.value = (this.value.replace(/[^a-zA-Z_ \,]/g, ''));
       });
      
      $scope.submit=function(){
        $http({ 
              method: 'POST', 
              url: 'api/saveAdvanceLeadInfo', 
              data: {first_name:this.first_name,last_name:this.last_name,middle_init:this.middle_init,
              street_addr1:this.customerAddress1,street_addr2:this.customerAddress2,city:this.city,state:this.state,
              Zip:this.zip_code,social_security:this.security_num,phone_home:this.customerPhone,
              phone_cell:this.phone_cell,phone_work:this.phone_work,phone_work_ext:this.phonework_ext,
              Email:this.customerEmail,birth_date:this.birth_date,employer_name:this.employer_name,
              pay_frequency:this.pay_frequency,direct_deposite:this.direct_deposite,pay_day1:this.payday1,
              pay_day2:this.payday2,bank_aba:this.routing_number,bank_account:this.account_number,
              bank_name:this.bank_name,income_monthly:this.monthly_income,own_home:this.own_home,
              drivers_license:this.license_number,drivers_license_st:this.license_state,
              client_url_root:this.url_root,client_ip_address:this.ip_address,
              email_alternate:this.alternate_email,months_employed:this.months_employed,income_type:this.income_type,
              is_military:this.is_military,bank_account_type:this.account_type,requested_amount:this.requested_amount,
              months_at_address:this.months_add,months_at_bank:this.months_bank},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("here in success",data);
                if (data.result == 'success') {
                  //$("#leadInformation-form").hide();
                  $('#leadStatus').html("Thanks for Saving your information.");
                  //var form = document.getElementById("lendersignup-form");
                  //form.reset();
                }else{
                  $('#leadStatus').html(data.details);
                }
            }).error(function(data, status, headers, config) {
              console.log("here in error",data);
            });
      }
    }
  ]);

  app.controller('LeadDecisioningController',[
      '$scope', '$http','$window', function($scope, $http,$window) {
       $scope.submit = function() {
       path=this.decision_path;
       $http({ 
              url:'decision/',
              method: 'POST',
              data:{'path':this.decision_path},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data,headers) {
                console.log(data);
            }).error(function(data,error) {
              console.log("error");
            });
       
       }
      }
      ]);
  
  
  
  
  app.controller('LenderProfileController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      $("#saveStatus").hide(); 
      $("#owner_phone,#manager_phone,#loan_limit").on('keyup',function(e) {
              this.value = (this.value.replace(/[^0-9.\,]/g, ''));
      });
	$("#city,#storename").on('keyup',function(e) {
              this.value = (this.value.replace(/[^a-zA-Z_ \,]/g, ''));
       });
      $("#owner_name,#manager_name").on('keyup',function(e) {
              this.value = (this.value.replace(/[^a-zA-Z_ \,]/g, ''));
       });

      
      $http({
              method: 'GET', 
              url: 'api/getLenderInfo/', 
            }).success(function(data, status, headers, config) {
                console.log("data",data[0]);
		Date.prototype.monthName = function() {
                  return this.toUTCString().split(' ')[2]
                };
                d = new Date();
                var month = d.monthName();
                var year = d.getFullYear();
                var day = d.getDate();
                var date = month+' '+day+','+year
                $scope.todayDate = date;
                $scope.LenderInfo = data[0];
                $scope.owner_name=data[0].ownerName;
                $scope.owner_phone=data[0].ownerPhone;
                $scope.owner_email=data[0].ownerEmail;
                $scope.ein_number=data[0].EinNumber;
                $scope.manager_name=data[0].managerName;
                $scope.manager_phone=data[0].managerPhone;
                $scope.manager_email=data[0].managerEmail;
                $scope.loan_limit=data[0].loanLimit;
                $scope.store_phone = data[0].phone_number;
                $scope.store_email = data[0].email;
                $scope.store_address=data[0].storeAddress;
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      
      
      $scope.submit=function(){
        console.log(this.owner_name,this.owner_phone,this.owner_email,this.ein_number,this.manager_name,
                    this.manager_email,this.manager_phone,this.loan_limit,this.store_phone,this.store_address)
        $http({ 
              method: 'POST', 
              url: 'api/saveLenderInfo/', 
              //data: {ownerName:this.owner_name,ownerEmail:this.owner_email,ownerPhone:this.owner_phone,
             // EinNumber:this.ein_number,managerName:this.manager_name,managerEmail:this.manager_email,
              //managerPhone:this.manager_phone,loanLimit:this.loan_limit,storeAddress:this.store_address,
              //phone_number:this.store_phone},
	      data: {ownerName:this.owner_name,ownerEmail:this.owner_email,ownerPhone:this.owner_phone,
              EinNumber:this.ein_number,managerName:this.manager_name,managerEmail:this.manager_email,state:this.state,
	      city:this.city,storeName:this.storeName,entityname:this.entityname,managerPhone:this.manager_phone,
	      loanLimit:this.loan_limit,storeAddress:this.store_address,
              phone_number:this.store_phone},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data.details);
                $('#saveStatus').show();
                $('#saveStatus').html(data.details);
                $('#saveStatus').delay(3000).fadeOut('slow');
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      }
      
      /*$scope.makeRequest=function(){
        console.log("req",this.request)
         $http({ 
              method: 'POST', 
              url: 'api/lenderRequest/', 
              data: {'req':this.request},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data.result);
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      }*/
    }
  ]);

app.controller('EditableLenderController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      var url = document.URL;
      var parts = url.split('/');
      var id = parts[5];
     $http({ 
              method: 'POST', 
              url: 'api/getlenderData/',
              data: {'lender_id':id},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log(data)
                $scope.lender=data[0]
                $scope.store_name=data[0].storeName;
                $scope.lender_email=data[0].email;
                $scope.store_phone=data[0].phone_number;
                $scope.store_address=data[0].storeAddress;
                $scope.zip_code=data[0].zip_code;
                $scope.manager_name=data[0].managerName;
                $scope.manager_phone=data[0].managerPhone;
                $scope.manager_email=data[0].managerEmail;
                $scope.owner_phone=data[0].ownerPhone;
                $scope.owner_email=data[0].ownerEmail;
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      
      //Lender Info edited by staff
      $scope.editLenderInfo=function(){
        var url = document.URL;
        var parts = url.split('/');
        var id = parts[5];
        console.log(this.store_name,this.lender_email,this.store_phone,this.store_address,this.zip_code,
                    this.manager_name,this.manager_phone,this.manager_email,this.owner_phone,this.owner_email)
        if(this.store_name&&this.lender_email&&this.store_phone&&this.store_address&&this.zip_code&&
          this.manager_name&&this.manager_phone&&this.manager_email&&this.owner_phone&&this.owner_email){
          $http({ 
              method: 'POST', 
              url: 'api/saveLenderChanges/',
              data: {'store_name':this.store_name,'lender_email':this.lender_email,'store_phone':this.store_phone,
              'store_address':this.store_address,'zip_code':this.zip_code,'manager_name':this.manager_name,
              'manager_phone':this.manager_phone,'manager_email':this.manager_email,
              'owner_phone':this.owner_phone,'owner_email':this.owner_email,'lender_id':id},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log(data)
                if (data.result=="success") {
                  $('#editstatus').html(data.details);
                  $('#editstatus').delay(3000).fadeOut('slow');
                }else{
                  $('#editstatus').html(data.details);
                  $('#editstatus').delay(3000).fadeOut('slow');
                }
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
          
        }else{
           $('#editstatus').html('Please fill all the fields');
          $('#editstatus').delay(3000).fadeOut('slow');
        }
      }
    
    
    }
  ]);
  
  app.controller('CustomerListController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      customers_array=[]
      $http({ 
              method: 'GET', 
              url: 'api/getCustomers', 
            }).success(function(data, status, headers, config) {
                console.log("data",data);
                $scope.customersdata=data;
                $scope.filteredcustomers = [];
                $scope.currentPage = 1;
                $scope.numPerPage = 5;
                $scope.maxSize = 3;
                $scope.numPages = function () {
                  return Math.ceil($scope.customersdata.length / $scope.numPerPage);
                };
                
                $scope.$watch('currentPage + numPerPage', function() {
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                    ,end = begin + $scope.numPerPage;
                    $scope.filteredcustomers = $scope.customersdata.slice(begin, end);
                });
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
    
      $scope.approveLead = function(customer){
        customers_array.push(customer)
        //console.log("customer",customers_array)
      
      }
      
      $scope.convertToLead = function(){
          console.log("customer",customers_array)
          if (customers_array.length == 0) {
            $('#syncStatus').html('Please Choose customers to convert to lead');
          }else{
            customers_data=customers_array;
            $http({ 
              method: 'POST', 
              url: 'api/approveLead/', 
              data: {customers:customers_data,'flag':'staff'},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data.result);
                if (data.result == 'success') {
                  console.log(data.Details);
                  $('#syncStatus').html('Successfully Converted to lead');
                }else{
                  $('#syncStatus').html(data.details);
                }
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
          }
      }
    }
  ]);
  
  
  app.controller('MapViewController', [
        '$scope', '$http','$window', function($scope, $http,$window) {
          var getparams = (function(a) {
            if (a == "") return {};
              var b = {};
              for (var i = 0; i < a.length; ++i){
                var p=a[i].split('=', 2);
                if (p.length == 1)
                  b[p[0]] = "";
                else
                  b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
              }
              return b;
          })(window.location.search.substr(1).split('&'));
            var longitude = getparams['longi']
            var latitude = getparams['lati']
            console.log(longitude,latitude);
            $scope.longitude = longitude;
            $scope.latitude = latitude;
            $scope.address = latitude+' '+longitude;
            
        }
  ]);
  
  app.controller('StaffDashboardController', [
    '$scope', '$http','$window', function($scope, $http,$window) {
      $("#map").hide();
       
        $http({ 
              method: 'GET', 
              url: 'api/helpdeskticket/', 
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log("data",data);
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      $http({ 
              method: 'GET', 
              url: 'api/getLenders', 
            }).success(function(data, status, headers, config) {
                //console.log("data",data);
                $scope.lendersdata=data;
                $scope.filteredlenders = [];
                $scope.currentPage = 1;
                $scope.numPerPage = 4;
                $scope.maxSize = 3;
                $scope.numPages = function () {
                  return Math.ceil($scope.lendersdata.length / $scope.numPerPage);
                };
                
                $scope.$watch('currentPage + numPerPage', function() {
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                    ,end = begin + $scope.numPerPage;
                    $scope.filteredlenders = $scope.lendersdata.slice(begin, end);
                });
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
     
     
     //leads limit set by staff for a lender
     	$scope.submit=function(e){
	if (this.hourly_set>15){
        	this.daily_set  = (24*this.hourly_set)+10;
        	this.weekly_set  = (24*7*this.hourly_set)+10;
        	this.monthly_set  = (24*7*30*this.hourly_set)+10;
	}else{
		alert("Please enter value greater than 15");
		return false;	
	
	}
        lender_id=$("#lender_id_new"+e).val();
        $http({ 
              method: 'POST', 
              url: 'api/setLeadsLimit/', 
              data: 						         {lender_id:lender_id,hourly_set:this.hourly_set,daily_set:this.daily_set,weekly_set:this.weekly_set,monthly_set:this.monthly_set},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log(data);
              }).error(function(data, status, headers, config) {
              console.log("error");
            });
        
      }
      
      $scope.accountstatus=function(userid,email){
       var lenderId=userid;
       var lenderEmail=email;
       if(this.Onboarding==null){ var Onboarding=0; }else{var Onboarding=this.Onboarding}
       if(this.Active==null){ var Active=0; }else{var Active=this.Active}
       if(this.Paused==null){ var Paused=0; }else{var Paused=this.Paused}
       if(this.maxLeads==null){ var maxLeads=0; }else{var maxLeads=this.maxLeads}
       if(this.PastDue==null){ var PastDue=0; }else{var PastDue=this.PastDue}

       var statuscode=[Onboarding,Active,Paused,maxLeads,PastDue];
       alert(statuscode);
      $('#leadstatus').modal({"keyboard": false});
      $('.progress-bar').animate({width:'+=100%'}, 2000);
          $http({ 
              method: 'POST', 
              url: 'api/leanderaccountstatus/', 
              data: {lenderId:lenderId,lenderEmail:lenderEmail,statuscode:statuscode},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log(data);
               jQuery('#closedleadsform').click();
                
            }).error(function(data, status, headers, config) {
              console.log("error");
              jQuery('#closedleadsform').click();
            });
      }
      
      
      
      $scope.approveLender = function(id,email){
      var lenderId = id;
      var lenderEmail = email;
      $http({ 
              method: 'POST', 
              url: 'api/approveLender/', 
              data: {lenderId:lenderId,lenderEmail:lenderEmail},
              headers: {'Content-Type': 'application/json','X-CSRFToken': $("input[name='csrfmiddlewaretoken']").val()}
            }).success(function(data, status, headers, config) {
                console.log(data);
                if (data.result == 'success') {
                  console.log(data.Details);
                  window.location.reload();
                }else{
                  console.log(data.error);
                  //$('#passwordStatus').html('Please provide valid credentials');
                }
            }).error(function(data, status, headers, config) {
              console.log("error");
            });
      
      }
    }
  ])
}).call(this);
///ends here/////
  

(function() {
  var app;

  app = angular.module('app.app.editor', ['app.api', 'app.app.photos']);

  app.controller('EditController', [
    '$scope', 'Post', function($scope, Post) {
      $scope.newPost = new Post();
      return $scope.save = function() {
        return $scope.newPost.$save().then(function(result) {
          return $scope.posts.push(result);
        }).then(function() {
          return $scope.newPost = new Post();
        }).then(function() {
          return $scope.errors = null;
        }, function(rejection) {
          return $scope.errors = rejection.data;
        });
      };
    }
  ]);

}).call(this);

(function() {
  var app;

  app = angular.module('app.app.manage', ['app.api', 'app.app.editor']);

  app.controller('DeleteController', [
    '$scope', 'AuthUser', function($scope, AuthUser) {
      $scope.canDelete = function(post) {
        return post.author.username === AuthUser.username;
      };
      return $scope["delete"] = function(post) {
        return post.$delete().then(function() {
          var idx;
          idx = $scope.posts.indexOf(post);
          return $scope.posts.splice(idx, 1);
        });
      };
    }
  ]);

}).call(this);

(function() {
  var app;

  app = angular.module('app.app.photos', ['app.api']);

  app.controller('AppController', [
    '$scope', 'Post', 'PostPhoto', function($scope, Post, PostPhoto) {
      $scope.photos = {};
      $scope.posts = Post.query();
      return $scope.posts.$promise.then(function(results) {
        return angular.forEach(results, function(post) {
          return $scope.photos[post.id] = PostPhoto.query({
            post_id: post.id
          });
        });
      });
    }
  ]);

}).call(this);

(function() {
  var app;

  app = angular.module('app.app.playground', ['app.api.playground']);

  app.controller('AppController', [
    '$scope', 'User', function($scope, User) {
      $scope.users = [];
      $scope.newUsername = "";
      $scope.loadUsers = function() {
        return User.query().$promise.then(function(results) {
          return $scope.users = results;
        });
      };
      $scope.addUser = function() {
        var user;
        user = new User({
          username: $scope.newUsername
        });
        $scope.newUsername = "";
        return user.$save().then($scope.loadUsers);
      };
      $scope.deleteUser = function(user) {
        return user.$delete().then($scope.loadUsers);
      };
      return $scope.loadUsers();
    }
  ]);

}).call(this);

(function() {
  var app;

  app = angular.module('app.app.resource', ['app.api']);

  app.controller('AppController', [
    '$scope', 'Post', function($scope, Post) {
      return $scope.posts = Post.query();
    }
  ]);

}).call(this);

(function() {
  var app;

  app = angular.module('app.app.bacic', []);
  var cookievalue = read_cookie('email');
  $('#useremail').val(cookievalue);
  app.controller('AppController', [
    '$scope', '$http', function($scope, $http) {
      $scope.posts = [];
      //return $http.post('/api/posts').then(function(result) {
      //  return angular.forEach(result.data, function(item) {
      //    return $scope.posts.push(item);
      //  });
      //});
    }
  ]);

}).call(this);

(function() {
  var app;

  app = angular.module('app.app.update', ['app.api']);

  app.controller('AppController', [
    '$scope', 'User', function($scope, User) {
      $scope.users = [];
      $scope.newUsername = "";
      $scope.loadUsers = function() {
        return User.query().$promise.then(function(results) {
          return $scope.users = results;
        });
      };
      $scope.addUser = function() {
        var user;
        user = new User({
          username: $scope.newUsername
        });
        $scope.newUsername = "";
        return user.$save().then($scope.loadUsers);
      };
      $scope.deleteUser = function(user) {
        return user.$delete().then($scope.loadUsers);
      };
      return $scope.loadUsers();
    }
  ]);

}).call(this);

(function() {
  var app;

  app = angular.module('app.api', ['ngResource']);

  app.factory('User', [
    '$resource', function($resource) {
      return $resource('/api/users/:username', {
        username: '@username'
      });
    }
  ]);

  app.factory('Post', [
    '$resource', function($resource) {
      return $resource('/api/posts/:id', {
        id: '@id'
      });
    }
  ]);

  app.factory('Photo', [
    '$resource', function($resource) {
      return $resource('/api/photos/:id', {
        id: '@id'
      });
    }
  ]);

  app.factory('UserPost', [
    '$resource', function($resource) {
      return $resource('/api/users/:username/posts/:id');
    }
  ]);

  app.factory('PostPhoto', [
    '$resource', function($resource) {
      return $resource('/api/posts/:post_id/photos/:id');
    }
  ]);

}).call(this);

(function() {
  var app;

  app = angular.module('app.api.playground', []);

  app.factory('User', [
    '$q', function($q) {
      var MockUser, storage, user, username, _i, _len, _ref;
      storage = {};
      MockUser = (function() {
        function MockUser(params) {
          var key, value;
          for (key in params) {
            value = params[key];
            this[key] = value;
          }
        }

        MockUser.query = function() {
          var dfr, key, val, values;
          dfr = $q.defer();
          values = (function() {
            var _results;
            _results = [];
            for (key in storage) {
              val = storage[key];
              _results.push(val);
            }
            return _results;
          })();
          dfr.resolve(values);
          values.$promise = dfr.promise;
          return values;
        };

        MockUser.save = function(params) {
          var user;
          user = new this(params);
          user.$save();
          return user;
        };

        MockUser.prototype.$save = function() {
          var dfr;
          storage[this.username] = this;
          dfr = $q.defer();
          dfr.resolve(this);
          return dfr.promise;
        };

        MockUser.prototype.$delete = function() {
          var dfr;
          delete storage[this.username];
          dfr = $q.defer();
          dfr.resolve();
          return dfr.promise;
        };

        return MockUser;

      })();
      _ref = ['bob', 'sally', 'joe', 'rachel'];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        username = _ref[_i];
        user = new MockUser({
          username: username
        });
        storage[user.username] = user;
      }
      return MockUser;
    }
  ]);

}).call(this);

/*Function to validate an email address*/
function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(email)){
        return true;
    }else{
        return false;
    }
} 

/*Function to delete cookie on url change*/
function delete_cookie( name ) {
  document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

/*Function to read a cookie value*/
function read_cookie(key)
{
    var result;
    return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? (result[1]) : null;
}

/*Function to validate a phonenumber*/
function phonenumber(inputtxt1,inputtxt2,inputtxt3)  
{  
  var phoneno = /^\d{10}$/;  
  if((inputtxt1.value.match(phoneno))&&(inputtxt2.value.match(phoneno))&&(inputtxt3.value.match(phoneno))){  
    var validphone = 'true'; 
  }else{  
    var validphone = 'false';
  }
  return validphone;
}
function ticketgenrater(des,sub,email,cc_email){
  
 // var url="http://localhost:9602/freshdesk/api/tickets/create/?data={"des":des,"sub":sub,"email":email,"cc_email":cc_email}";
  
  return url;
}

