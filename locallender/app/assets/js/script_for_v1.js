// Controller to manage lender's multiple store locations @jeevan
app.controller('lender_locations_controller', [
    '$scope', '$http','$window', function($scope, $http,$window) {
        
        $http({
                method: 'GET', 
                url: '/api/locations/v1/get/', 
            }).success(function(data, status, headers, config) {
                pageSetUp();                
		$scope.jqgrid_data=data.results;
                var len=data.results.length;
                var jqgrid_data =[]
                for(var i=0;i<len;i++){
                    var jqgrid_data1 = [data.results[i]]
                    jqgrid_data.push(data.results[i])
                }                
                jQuery("#jqgrid").jqGrid({
                    data : jqgrid_data,
                    datatype : "local",
                    height : 'auto',
                    colNames : ['Actions', 'Store ID', 'Location Name', 'Manager Name', 'Manager Email', 'Phone', 'longi', 'latti', 'address', 'zipcode', 'Max Hourly','Max Daily','Max Weekly','Max Monthly'],
                    colModel : [{
                            name : 'act',
                            index : 'act',
                            sortable : false
                    }, {
                            name : 'id',
                            index : 'id'
                    }, {
                            name : 'location_name',
                            index : 'location_name',
                            editable : true,
                            editrules : { required:true }
                    }, {
                            name : 'name',
                            index : 'name',
                            editable : true,
                            editrules : { required:true },
                    }, {
                            name : 'email',
                            index : 'email',
                            align : "right",
                            editable : true,
                            editrules : { required:true, email: true }
                    }, {
                            name : 'phone',
                            index : 'phone',
                            align : "right",
                            editable : true,
                            editrules : { required:true, number: true },
                            size: 15,
                            maxlength: 10,
                            minlength: 10,
                            width: 60,
                            //formatter: formatPhoneNumber
                    }, {
                            name : 'longitude',
                            index : 'longitude',
                            align : "right",
                            editable : false
                    }, {
                            name : 'latitude',
                            index : 'latitude',
                            sortable : false,
                            editable : false
                    }, {
                            name : 'address',
                            index : 'address',
                            sortable : false,
                            editable : true,
                            edittype : 'textarea',
                            width: 60,
                            editrules : { required:true },
                    }, {
                            name : 'zipcode',
                            index : 'zipcode',
                            sortable : true,
                            editable : true,
                            editrules : { required:true, number: true },
                            editoptions: { size:6, maxlength:6, minlength:6 }
                    }, {
                            name : 'max_hourly',
                            index : 'max_hourly',
                            sortable : true,
                            editable : true,
                            editrules : { required:true, number: true },
                            editoptions: { size:6, maxlength:6, minlength:6 }
                    }, {
                            name : 'max_daily',
                            index : 'max_daily',
                            sortable : true,
                            editable : true,
                            editrules : { required:true, number: true },
                            editoptions: { size:6, maxlength:6, minlength:6 }
                    }, {
                            name : 'max_weekly',
                            index : 'max_weekly',
                            sortable : true,
                            editable : true,
                            editrules : { required:true, number: true },
                            editoptions: { size:6, maxlength:6, minlength:6 }
                    }, {
                            name : 'max_monthly',
                            index : 'max_monthly',
                            sortable : true,
                            editable : true,
                            editrules : { required:true, number: true },
                            editoptions: { size:6, maxlength:6, minlength:6 }
                    }],
                    rowNum : 10,
                    rowList : [10, 20, 30],
                    pager : '#pjqgrid',
                    sortname : 'id',
                    toolbarfilter : true,
                    viewrecords : true,
                    sortorder : "asc",
                    gridComplete : function() {
                            var ids = jQuery("#jqgrid").jqGrid('getDataIDs');
                            for (var i = 0; i < ids.length; i++) {
                                    var cl = ids[i];
                                    be = "<button class='btn btn-xs btn-default' data-original-title='Edit Row' onclick=\"jQuery('#jqgrid').editRow('" + cl + "');\"><i class='fa fa-pencil'></i></button>";
                                    se = "<button class='btn btn-xs btn-default' data-original-title='Save Row' onclick=\"jQuery('#jqgrid').saveRow('" + cl + "');\"><i class='fa fa-save'></i></button>";
                                    ca = "<button class='btn btn-xs btn-default' data-original-title='Cancel' onclick=\"jQuery('#jqgrid').restoreRow('" + cl + "');\"><i class='fa fa-times'></i></button>";
                                    ce = "<button class='btn btn-xs btn-default' onclick=\"jQuery('#jqgrid').restoreRow('"+cl+"');\"><i class='fa fa-times'></i></button>";
                                    jQuery("#jqgrid").jqGrid('setRowData',ids[i],{act:be+se+ce});
                                    jQuery("#jqgrid").jqGrid('setRowData', ids[i], {
                                            act : be + se + ca
                                    });
                            }
                    },
                    editurl : "/api/locations/v1/add/",
                    caption : "Lender Store | Locations",
                    multiselect : true,
                    autowidth : true,
                    postData: {
                            csrfmiddlewaretoken: $.cookie('csrftoken')
                    },
		});
			
                // converts number to (xxx)xxx-xxxx or xxx-xxx-xxxx
                function formatPhoneNumber(cellvalue, options, rowObject) {
                    var re = /\D/;
                    // test for this format: (xxx)xxx-xxxx
                    var re2 = /^\({1}\d{3}\)\d{3}-\d{4}/;
                    // test for this format: xxx-xxx-xxxx
                    //var re2 = /^\d{3}-\d{3}-\d{4}/;
                    var num = cellvalue;
                    if (num === null){
                        num = "";
                    }
                    var newNum = num;
                    if (num != "" && re2.test(num) != true) {
                        if (num != "") {
                            while (re.test(num)) {
                                num = num.replace(re, "");
                            }
                        }
                        if (num.length == 10) {
                            // for format (xxx)xxx-xxxx
                            newNum = '(' + num.substring(0, 3) + ')' + num.substring(3, 6) + '-' + num.substring(6, 10);
                            // for format xxx-xxx-xxxx
                            // newNum = num.substring(0,3) + '-' + num.substring(3,6) + '-' + num.substring(6,10);
                        }
                    }
                    return newNum;
                }
			
                jQuery("#jqgrid").navGrid('#pjqgrid',{edit:true,add:true,del:true,search:true},
                {
                        closeAfterEdit:true,
                        reloadAfterSubmit:true,
                        closeOnEscape:true,
                        editData: {csrfmiddlewaretoken: $.cookie('csrftoken')}
                },{
                        closeAfterAdd:true,
                        reloadAfterSubmit:true,
                        closeOnEscape:true,
                        editData: {csrfmiddlewaretoken: $.cookie('csrftoken')}
                },{
                        closeOnEscape:true,
                        delData: {csrfmiddlewaretoken: $.cookie('csrftoken')}
                },{
                        caption: "Search",
                        Find: "Find",
                        Reset: "Reset",
                        sopt  : ['eq', 'cn'],
                        matchText: " match",
                        rulesText: " rules",
                        closeAfterSearch: true,
                        afterShowSearch: function () {
                                        $('#reset_filter1_block').show();
                        }
                  }
                );
                
                jQuery("#jqgrid").jqGrid('navGrid', "#pjqgrid", {
                        edit : false,
                        add : false,
                        del : true,
                });
                jQuery("#jqgrid").jqGrid('inlineNav', "#pjqgrid");
                /* Add tooltips */
                $('.navtable .ui-pg-button').tooltip({
                        container : 'body'
                });

                jQuery("#m1").click(function() {
                        var s;
                        s = jQuery("#jqgrid").jqGrid('getGridParam', 'selarrrow');
                        alert(s);
                });
                jQuery("#m1s").click(function() {
                        jQuery("#jqgrid").jqGrid('setSelection', "13");
                });

                // remove classes
                $(".ui-jqgrid").removeClass("ui-widget ui-widget-content");
                $(".ui-jqgrid-view").children().removeClass("ui-widget-header ui-state-default");
                $(".ui-jqgrid-labels, .ui-search-toolbar").children().removeClass("ui-state-default ui-th-column ui-th-ltr");
                $(".ui-jqgrid-pager").removeClass("ui-state-default");
                $(".ui-jqgrid").removeClass("ui-widget-content");

                // add classes
                $(".ui-jqgrid-htable").addClass("table table-bordered table-hover");
                $(".ui-jqgrid-btable").addClass("table table-bordered table-striped");

                $(".ui-pg-div").removeClass().addClass("btn btn-sm btn-primary");
                $(".ui-icon.ui-icon-plus").removeClass().addClass("fa fa-plus");
                $(".ui-icon.ui-icon-pencil").removeClass().addClass("fa fa-pencil");
                $(".ui-icon.ui-icon-trash").removeClass().addClass("fa fa-trash-o");
                $(".ui-icon.ui-icon-search").removeClass().addClass("fa fa-search");
                $(".ui-icon.ui-icon-refresh").removeClass().addClass("fa fa-refresh");
                $(".ui-icon.ui-icon-disk").removeClass().addClass("fa fa-save").parent(".btn-primary").removeClass("btn-primary").addClass("btn-success");
                $(".ui-icon.ui-icon-cancel").removeClass().addClass("fa fa-times").parent(".btn-primary").removeClass("btn-primary").addClass("btn-danger");

                $(".ui-icon.ui-icon-seek-prev").wrap("<div class='btn btn-sm btn-default'></div>");
                $(".ui-icon.ui-icon-seek-prev").removeClass().addClass("fa fa-backward");

                $(".ui-icon.ui-icon-seek-first").wrap("<div class='btn btn-sm btn-default'></div>");
                $(".ui-icon.ui-icon-seek-first").removeClass().addClass("fa fa-fast-backward");

                $(".ui-icon.ui-icon-seek-next").wrap("<div class='btn btn-sm btn-default'></div>");
                $(".ui-icon.ui-icon-seek-next").removeClass().addClass("fa fa-forward");

                $(".ui-icon.ui-icon-seek-end").wrap("<div class='btn btn-sm btn-default'></div>");
                $(".ui-icon.ui-icon-seek-end").removeClass().addClass("fa fa-fast-forward");
            }).error(function(data, status, headers, config) {
                console.log("error");
            });      
    }
]);