define([
    // account
    '/static/dashboard/auth/module',
    '/static/dashboard/auth/models/User',

    // layout

    '/static/dashboard/layout/module',
    '/static/dashboard/layout/actions/minifyMenu',
    '/static/dashboard/layout/actions/toggleMenu',
    '/static/dashboard/layout/actions/fullScreen',
    '/static/dashboard/layout/actions/resetWidgets',
    '/static/dashboard/layout/actions/resetWidgets',
    '/static/dashboard/layout/actions/searchMobile',
    '/static/dashboard/layout/directives/demo/demoStates',
    '/static/dashboard/layout/directives/smartInclude',
    '/static/dashboard/layout/directives/smartDeviceDetect',
    '/static/dashboard/layout/directives/smartFastClick',
    '/static/dashboard/layout/directives/smartLayout',
    '/static/dashboard/layout/directives/smartSpeech',
    '/static/dashboard/layout/directives/smartRouterAnimationWrap',
    '/static/dashboard/layout/directives/smartFitAppView',
    '/static/dashboard/layout/directives/radioToggle',
    '/static/dashboard/layout/directives/dismisser',
    '/static/dashboard/layout/directives/smartMenu',
    '/static/dashboard/layout/directives/bigBreadcrumbs',
    '/static/dashboard/layout/directives/stateBreadcrumbs',
    '/static/dashboard/layout/directives/smartPageTitle',
    '/static/dashboard/layout/directives/hrefVoid',
    '/static/dashboard/layout/service/SmartCss',
    '/static/dashboard/modules/widgets/directives/widgetGrid',
    '/static/dashboard/modules/widgets/directives/jarvisWidget',


    // dashboard
    '/static/dashboard/dashboard/module',


    //components
    '/static/dashboard/components/language/Language',
    '/static/dashboard/components/language/languageSelector',
    '/static/dashboard/components/language/language-controller',

    '/static/dashboard/components/projects/Project',
    '/static/dashboard/components/projects/recentProjects',

    '/static/dashboard/components/activities/activities-controller',
    '/static/dashboard/components/activities/activities-dropdown-toggle-directive',
    '/static/dashboard/components/activities/activities-service',

    '/static/dashboard/components/shortcut/shortcut-directive',

    '/static/dashboard/components/calendar/module',
    '/static/dashboard/components/calendar/models/CalendarEvent',
    '/static/dashboard/components/calendar/directives/fullCalendar',
    '/static/dashboard/components/calendar/directives/dragableEvent',
    '/static/dashboard/components/calendar/controllers/CalendarCtrl',

    '/static/dashboard/components/inbox/module',
    '/static/dashboard/components/inbox/models/InboxConfig',
    '/static/dashboard/components/inbox/models/InboxMessage',

    '/static/dashboard/components/todo/TodoCtrl',
    '/static/dashboard/components/todo/models/Todo',
    '/static/dashboard/components/todo/directives/todoList',

    // chat
    '/static/dashboard/components/chat/module',

    // graphs
    '/static/dashboard/modules/graphs/module',


    // tables
    '/static/dashboard/modules/tables/module',

    // forms
    '/static/dashboard/modules/forms/module',

    // ui
    '/static/dashboard/modules/ui/module',

    // widgets
    '/static/dashboard/modules/widgets/module',

    // widgets
    '/static/dashboard/modules/maps/module',

    // appViews
    '/static/dashboard/modules/app-views/module',

    // misc
    '/static/dashboard/modules/misc/module',

    // smartAdmin
    '/static/dashboard/modules/smart-admin/module'

], function () {
    'use strict';
});
