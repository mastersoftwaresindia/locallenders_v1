from django.template import Context,loader
from django.http import HttpResponse, StreamingHttpResponse
from django.conf import settings
from pprint import pprint
import json
import pycurl
from StringIO import StringIO
        
   
def test(debug_type, debug_msg):
    print "shintu(%d): %s" % (debug_type, debug_msg)
    

    
 # View all user data          
def viewall(content):
    try:
        storage = StringIO()
        scope = ("contacts.json")
        module = __name__.split('.')
        url = settings.FRESHDESK_URL
        url = settings.FRESHDESK_URL+module[1]+".json"
        c = pycurl.Curl()
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.HTTPHEADER, ['Content-Type: application/json'])
        c.setopt(pycurl.USERPWD, "O4kZHAFk0wNo094bD7VZ:X")
        #c.setopt(pycurl.WRITEFUNCTION, write_out) # now passing own method
        c.setopt(c.WRITEDATA, storage)
        #c.setopt(pycurl.DEBUGFUNCTION, test)
        c.setopt(c.NOPROGRESS, 0)
        c.setopt(pycurl.FOLLOWLOCATION, 1)
       # c.setopt(pycurl.POST, 1)
        c.setopt(c.VERBOSE, True)
        #c.setopt(pycurl.POSTFIELDS,postData)
        c.perform()
        content = storage.getvalue()
        return {c.getinfo(pycurl.RESPONSE_CODE), content}
    except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        
        

   
# View all user single user data only pass user id         
def view(content):
    try:
        storage = StringIO()
        scope = ("contacts.json")
        module = __name__.split('.')
        url = settings.FRESHDESK_URL
        url = settings.FRESHDESK_URL+module[1]+'/'+content['id']+".json"
        print url
        c = pycurl.Curl()
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.HTTPHEADER, ['Content-Type: application/json'])
        c.setopt(pycurl.USERPWD, "O4kZHAFk0wNo094bD7VZ:X")
        #c.setopt(pycurl.WRITEFUNCTION, write_out) # now passing own method
        c.setopt(c.WRITEDATA, storage)
        #c.setopt(pycurl.DEBUGFUNCTION, test)
        c.setopt(c.NOPROGRESS, 0)
        c.setopt(pycurl.FOLLOWLOCATION, 1)
       # c.setopt(pycurl.POST, 1)
        c.setopt(c.VERBOSE, True)
        #c.setopt(pycurl.POSTFIELDS,postData)
        c.perform()
        content = storage.getvalue()
        return {c.getinfo(pycurl.RESPONSE_CODE), content}
    except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')

# Create user acccount 
def create(content):
    try:
        storage = StringIO()
        scope = ("contacts.json")
        username =content['name']
        useremail=content['email']
        postData1 = { "user": { "name":username, "email":useremail }}
        postData=str(postData1).replace("'",'"')
        module = __name__.split('.')
        url = settings.FRESHDESK_URL+module[1]+".json"
        c = pycurl.Curl()
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.HTTPHEADER, ['Content-Type: application/json'])
        c.setopt(pycurl.USERPWD, "O4kZHAFk0wNo094bD7VZ:X")
        #c.setopt(pycurl.WRITEFUNCTION, write_out) # now passing own method
        c.setopt(c.WRITEDATA, storage)
        c.setopt(c.NOPROGRESS, 0)
        c.setopt(pycurl.POST, 1)
        c.setopt(c.VERBOSE, True)
        c.setopt(pycurl.POSTFIELDS, postData)   
        c.perform()
        content = storage.getvalue()
        return {c.getinfo(pycurl.RESPONSE_CODE), content}
    except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')

 #update user data
def update(content):
    try:
        storage = StringIO()
        scope = ("contacts.json")
        postData1 = { "user": { "name":username, "email":useremail }}
        postData=str(postData1).replace("'",'"')
        module = __name__.split('.')
        url = settings.FRESHDESK_URL+module[1]+'/'+content['id']+".json"
        c = pycurl.Curl()
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.HTTPHEADER, ['Content-Type: application/json'])
        c.setopt(pycurl.USERPWD, "O4kZHAFk0wNo094bD7VZ:X")
        #c.setopt(pycurl.WRITEFUNCTION, write_out) # now passing own method
        c.setopt(pycurl.CUSTOMREQUEST, "PUT")
        c.setopt(c.WRITEDATA, storage) 
        c.setopt(pycurl.DEBUGFUNCTION, test)
        c.setopt(c.NOPROGRESS, 0)
        c.setopt(pycurl.POST, 1)
        c.setopt(c.VERBOSE, True)
        c.setopt(pycurl.POSTFIELDS, postData)
        c.perform()
        content = storage.getvalue()
        return {c.getinfo(pycurl.RESPONSE_CODE), content}
    except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        
#delete User through  id      
def delete(content):
    try:
        storage = StringIO()
        scope = ("contacts.json")
        module = __name__.split('.')
        url = settings.FRESHDESK_URL+module[1]+'/'+content['id']+".json"
        c = pycurl.Curl()
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.HTTPHEADER, ['Content-Type: application/json'])
        c.setopt(pycurl.USERPWD, "O4kZHAFk0wNo094bD7VZ:X")
        #c.setopt(pycurl.WRITEFUNCTION, write_out) # now passing own method
        c.setopt(c.WRITEDATA, storage) 
        c.setopt(pycurl.DEBUGFUNCTION, test)
        c.setopt(pycurl.FOLLOWLOCATION, 1)
        c.setopt(pycurl.CUSTOMREQUEST, "DELETE")
        #c.setopt(c.NOPROGRESS, 0)
        #c.setopt(pycurl.POST, 1)
        c.setopt(c.VERBOSE, True)
        #c.setopt(pycurl.POSTFIELDS, postData)
        c.perform()
        content = storage.getvalue()
        return {c.getinfo(pycurl.RESPONSE_CODE), content}
    except Exception,e:
            res = str(e)
            data = {'error': res}
            return HttpResponse(json.dumps(data), content_type='application/json')
        
        